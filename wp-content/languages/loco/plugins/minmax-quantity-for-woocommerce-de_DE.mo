��          ,       <       <   L   =   �  �   U   /   Quantity of product %products% can not be less than <strong>%value%</strong> Project-Id-Version: Min and Max Quantity for WooCommerce
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-05-06 14:54+0000
PO-Revision-Date: 2020-06-02 12:18+0000
Last-Translator: 
Language-Team: Deutsch
Language: de_DE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.0; wp-5.4.1 Sie müssen bitte mindestens <strong>%value%</strong> Portionen %products% bestellen. 