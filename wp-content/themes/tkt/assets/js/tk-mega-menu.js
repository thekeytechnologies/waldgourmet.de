jQuery(function ($) {

    $('#tkMainNav').find('.elementor-nav-menu--dropdown.elementor-nav-menu__container').addClass('tk-menu-overlay')
    $('.elementor-menu-toggle').click(function(){
        $('.tk-menu-overlay').toggleClass('active');
        $('.attachment-full').toggleClass('tk-hide');
    })
    $('ul:has(li.tk-mega-menu)').addClass('has-mega-menu');
});