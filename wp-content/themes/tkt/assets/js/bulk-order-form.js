jQuery(document).ready(function ($) {
    $(".tk-bulk-order-form").submit(function (e) {
        e.preventDefault();

        const products = [];
        const inputs = $(".tk-bulk-order-form .tk-addable input[type=number]");

        const submitButton = $(".tk-bulk-order-form button[type=submit]");

        submitButton.attr("disabled", true);
        submitButton.addClass("tk-sending");

        inputs.each(function (index, input) {

            if (!$(input).is('[readonly]')) {
                const productId = $(input).data("tk-product-id");
                const variationId = $(input).data("tk-variation-id");
                products.push({
                    "productId": productId,
                    "variationId": variationId,
                    "quantity": Math.round($(input).val() / $(input).data("tk-portion-size"))
                });
            }

        });

        $.ajax({
            url: tkAjax.ajax_url,
            type: 'POST',
            data: {
                "action": "tkBulkOrder",
                "products": products
            },
            cache: false,
            success: function (data) {
                document.location.href = "/warenkorb/";
            },
            error: function (jqXHR, textStatus) {
                submitButton.removeClass("tk-sending");
                submitButton.attr("disabled", false);
                alert(textStatus);
            }
        });
        return false;
    });

    $(".tk-bulk-order-form .tk-addable input[type=number]").change(function(event) {
        const targetInput = event.target;
        const variationId = $(targetInput).data("tk-variation-id");
        const group = $(targetInput).closest('.tk-product-wrapper');

        let targetVal = tkStringToNumber($(targetInput).val());
        let targetMax = tkStringToNumber($(targetInput).attr('max'));
        let step = tkStringToNumber($(targetInput).attr('step'));

        /* adjust to nearest step to correct manual inputs */
        if (targetVal % step !== 0) {
            targetVal = tkRoundToNearest(targetVal, step);
            $(targetInput).val(targetVal);
        }

        if (variationId) {
            const parentInput = $(group).find('.tk-variable-product-parent-row input[type=number]');
            const childInputs = $(group).find('.tk-variations input[type=number]');

            /* Adjust Amounts and Max Amounts */
            const max = tkStringToNumber($(parentInput).attr('max'));
            let sum = 0;

            childInputs.each(function (index, input) {
                let val = tkStringToNumber($(input).val());
                sum += val;
            });

            /* correct sum and current field value */
            if (sum > max) {
                sum = max;
                $(targetInput).val(Number($(targetInput).attr('max')).toFixed(1));
            }

            $(parentInput).val(sum.toFixed(1));

            childInputs.each(function (index, input) {
                let val = tkStringToNumber($(input).val());
                let newMax = max - (sum - val);
                $(input).attr('max', newMax.toFixed(1));
            });
        }

        tkUpdatePrices(targetInput);
    });

    function tkStringToNumber(str) {
        str = String(str).replace(',', '.');
        return Number(str);
    }

    function tkNumberToString(number, decimalPlaces = 1) {
        number = Number(number).toFixed(decimalPlaces);
        number = number.replace('.', ',');
        return number;
    }

    function tkRoundToNearest( number, multiple, decimal = 2 ){
        return Number((Math.round(number/multiple)*multiple).toFixed(decimal));
    }

    function tkUpdatePrices(targetInput) {
        const variationId = $(targetInput).data("tk-variation-id");
        const productRow = $(targetInput).closest('.tk-product-row')
        const singlePriceWrapper = $(productRow).find('.tk-price-col .tk-price-container');
        const totalPriceWrapper = $(productRow).find('.tk-total-col .tk-price-container');

        let amount = tkStringToNumber($(targetInput).val());
        let singlePrice = tkGetPriceFromWrapper(singlePriceWrapper);
        let newPrice = singlePrice * amount;
        tkSetTotalPrice(totalPriceWrapper, newPrice);

        if(variationId) {
            const group = $(targetInput).closest('.tk-product-wrapper');
            const parentTotalPriceWrapper = $(group).find('.tk-variable-product-parent-row .tk-total-col .tk-price-container');
            const childTotalPriceWrappers = $(group).find('.tk-variations .tk-total-col .tk-price-container');

            let sum = 0;
            childTotalPriceWrappers.each(function (index, wrapper) {
                let price = tkGetPriceFromWrapper(wrapper);
                sum += price;
            });

            tkSetTotalPrice(parentTotalPriceWrapper,sum)
        }

    }

    function tkGetPriceFromWrapper(wrapper) {
        let content = $(wrapper).text();
        return tkStringToNumber(content);
    }

    function tkSetTotalPrice(wrapper, price) {
        let priceString = tkNumberToString(price,2);
        $(wrapper).text(priceString);
    }
});