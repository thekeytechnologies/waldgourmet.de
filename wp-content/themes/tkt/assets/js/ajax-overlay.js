jQuery(document).ready(function ($) {
    $(".tk-ajax-overlay-target").append(`<div class="tk-ajax-overlay">
        <div class="tk-spinner">
            <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
        </div>
    </div>`);
});