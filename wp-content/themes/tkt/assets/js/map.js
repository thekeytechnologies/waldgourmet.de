// noinspection JSUnusedGlobalSymbols
function tkInitMap() {
    const map = new google.maps.Map(document.getElementById("tk-source-map"), {
        center: {lat: 52.482443, lng: 11.425307},
        zoom: 13,
        disableDefaultUI: true
    });
}

jQuery(document).ready(function ($) {
    $(document).on('submit', '#tk-product-source-form', function (e) {
        const form = $("#tk-product-source-form");
        const submitButton = form.find("button");

        const originalButtonContent = submitButton.html();
        const tagIdInput = form.find("input[type=text]");
        const spinnerIcon = form.find("i");
        const allInputsInForm = form.find(":input");
        const googleMapElement = $("#tk-source-map");
        const notFound = $(".tk-not-found");
        spinnerIcon.show();
        allInputsInForm.attr("disabled", true);

        notFound.hide();
        submitButton.addClass("tk-is-loading");
        submitButton.html("<i class=\"fa fa-circle-o-notch fa-spin fa-3x fa-fw\"></i>");

        jQuery.post("/wp-admin/admin-ajax.php", {
            "action": "get_gps_coords",
            "tagId": tagIdInput.val()
        }, function (response) {
            response = JSON.parse(response);
            if (response && response.lat && response.lon) {
                googleMapElement.empty();
                const map = new google.maps.Map(googleMapElement.get(0), {
                    zoom: 15,
                    scrollwheel: false,
                    disableDefaultUI: true
                });

                const marker = new google.maps.Marker({
                    position: {lat: response.lat, lng: response.lon},
                    map: map
                });
                const bounds = new google.maps.LatLngBounds();
                bounds.extend(marker.getPosition());

                const infowindow = new google.maps.InfoWindow({
                    content: "<div class='tk-map-popup'>" + response.description + "</div>"
                });

                map.fitBounds(bounds);

                const listener = google.maps.event.addListener(map, "idle", function () {
                    if (map.getZoom() > 14) map.setZoom(14);
                    infowindow.open(map, marker);
                    google.maps.event.removeListener(listener);
                });
            } else {
                notFound.show();
            }
            submitButton.removeClass("tk-is-loading");
            submitButton.html(originalButtonContent);
            spinnerIcon.hide();
            allInputsInForm.attr("disabled", false);
        });
        e.preventDefault();
    })
});