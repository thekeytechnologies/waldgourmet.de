jQuery(document).ready(function ($) {
    $.ajax({
        url: tkAjax.ajax_url,
        type: 'POST',
        data: {
            action: "tkCartCount",
        },
        cache: false,
        success: function (data) {
            $(".tk-cart-count").html(data["cart-count"]);
        },
        error: function (jqXHR, textStatus) {
        }
    });

    const form = $(".single-product form.tk-add-to-cart");

    function trackAddToCart(product) {
        if (ga !== 'undefined') {
            ga('ec:addProduct', {
                'id': product.id,
                'name': product.name,
                'price': product.price,
                'quantity': product.qty
            });
            ga('ec:setAction', 'add');
            ga('send', 'event', 'UX', 'click', 'add to cart');     // Send data using an event.
        }
        if (fbq !== 'undefined') {
            fbq('track', 'AddToCart');
        }
    }

    function addToCart(data) {

        data["action"] = "tkAddToCart";
        form.attr("disabled", true);
        const button = form.find("button[type=submit]");
        button.removeClass("tk-done");
        button.attr("disabled", true);
        button.addClass("tk-sending");
        $.ajax({
            url: tkAjax.ajax_url,
            type: 'POST',
            data: data,
            cache: false,
            success: function (data) {
                form.attr("disabled", false);
                button.removeClass("tk-sending");
                button.addClass("tk-done");
                button.attr("disabled", false);
                $(".tk-cart-count").html(data["cart-count"]);
                form.find(".added_to_cart").show();
                trackAddToCart(data["product"]);
            },
            error: function (jqXHR, textStatus) {
                alert(textStatus);
            }
        });
    }

    if (form.length > 0) {
        const priceDisplay = form.find(".price:not(.price-unit)");
        const availabilityDisplay = form.find(".tk-availability");
        const addToCartButton = form.find(".tk-add-to-cart-wrapper button");
        const quantityInput = form.find("input[name=quantity]");
        const singleVariation = form.find("input[name=tk-single-variation]");
        const unitDisplay = form.find(".tk-price-unit");
        const originalPriceDisplay = priceDisplay.html();
        const originalAvailabilityDisplay = availabilityDisplay.html();
        const originalUnitDisplay = unitDisplay.html();

        function isVariableProduct() {
            const variations = form.data("product_variations");
            return variations && variations !== "null";
        }

        function isSubscription() {
            var product = $('.product');
            var variation = getSelectedVariation();
            if ((product.hasClass('product-type-variable-subscription') || product.hasClass('product-type-variable-subscription')) && variation && variation.attributes.attribute_ausfuerung != "Einzelbestellung") {
                return true;
            } else {
                return false;
            }
        }

        function getSelectedVariation() {
            const variations = form.data("product_variations");
            if (variations && variations !== "null") {
                const selectedAttributes = {};
                form.find("select").each(function () {
                    selectedAttributes[$(this).attr("name")] = $(this).find("option:selected").val();
                });

                const selectedAttributeForEqual = JSON.stringify(selectedAttributes);
                return $.grep(variations, function (variation) {
                    return JSON.stringify(variation.attributes) === selectedAttributeForEqual;
                })[0];
            }
            return false;
        }

        function changeFormBasedOnSelectedProduct(price, maxQty) {
            let quantity = Number.parseInt(quantityInput.val());
            if (Number.isNaN(quantity)) {
                quantity = 0;
            }
            const fullPrice = price * quantity;

            const isUnlimited = maxQty === -1 || maxQty === "";

            if (isUnlimited) {
                availabilityDisplay.html("Lieferbar innerhalb von 3-8 Werktagen. Warum so lange? <i class=\"far fa-question-circle\"></i>");
            } else {
                quantityInput.attr("max", maxQty);
                availabilityDisplay.html(`${maxQty} verfügbar. Lieferbar innerhalb von 3-8 Werktagen. Warum so lange? <i class="far fa-question-circle"></i>`);
            }
            window.tkReinitializeAvailabilityTooltip ? window.tkReinitializeAvailabilityTooltip() : undefined;

            if (!Number.isNaN(fullPrice) && quantity > 0 && (isUnlimited || quantity <= maxQty)) {
                const basePriceValue = Number.parseInt(unitDisplay.data("tk-base-price-value"));
                const basePriceUnit = unitDisplay.data("tk-base-price-unit");

                if (isSubscription()) {
                    unitDisplay.html(`pro Lieferung`)
                } else if (basePriceValue && basePriceValue) {
                    unitDisplay.html(`für ${basePriceValue * quantity}${basePriceUnit} - ${quantity} ${quantity === 1 ? "Portion" : "Portionen"}`)
                } else {
                    unitDisplay.html(`für ${quantity} Stück`)
                }


                addToCartButton.attr("disabled", false);
                addToCartButton.html("In den Warenkorb");

                priceDisplay.html(fullPrice.toLocaleString('de-DE', {
                    style: 'currency',
                    currency: 'EUR'
                }));
            } else {
                unitDisplay.html(originalUnitDisplay);
                addToCartButton.attr("disabled", true);
                addToCartButton.html("Bitte Menge anpassen");
                priceDisplay.html(originalPriceDisplay);
            }
        }

        function updateDisplays() {
            const selectedVariation = getSelectedVariation();

            if (isVariableProduct() && selectedVariation) {
                const maxQty = selectedVariation.max_qty;
                const price = selectedVariation.display_price;
                changeFormBasedOnSelectedProduct(price, maxQty);
            } else if (!isVariableProduct()) {
                const maxQty = form.data("simple_product_max_quantity");
                const price = form.data("simple_product_price");
                changeFormBasedOnSelectedProduct(Number.parseFloat(price), maxQty);
            } else {
                availabilityDisplay.html(originalAvailabilityDisplay);
                priceDisplay.html(originalPriceDisplay);
                addToCartButton.attr("disabled", true);
                addToCartButton.html("Bitte Variante wählen");
                unitDisplay.html(originalUnitDisplay);
            }
        }

        updateDisplays();

        $(document).on('change', '.single-product form.tk-add-to-cart select', updateDisplays);
        $(document).on('change keyup mouseup scrollwheel', '.single-product form.tk-add-to-cart input[name=quantity]', updateDisplays);

        setTimeout(function () {
            addToCartButton.off();
        }, 1000);

        $(document).on("submit", "form.tk-add-to-cart", function (e) {
            e.preventDefault();

            const selectedVariation = getSelectedVariation();
            const quantity = Number.parseInt(quantityInput.val());
            const productId = form.data("product_id");

            let data = null;
            if (isVariableProduct() && selectedVariation) {
                data = {
                    quantity: quantity,
                    productId: productId,
                    variationId: selectedVariation.variation_id
                }
            } else if (!isVariableProduct()) {
                data = {
                    quantity: quantity,
                    productId: productId,
                }
            }


            if (data) {
                addToCart(data);
            }
        });

        $(document).on("click", '.tk-add-single-variation-link', function (event) {
            event.preventDefault();
            if (singleVariation.length > 0) {
                let singleVariationId = singleVariation.val();
                let data = {
                    quantity: 1,
                    productId: singleVariationId
                }

                addToCart(data);
            }

        })
    }
});
