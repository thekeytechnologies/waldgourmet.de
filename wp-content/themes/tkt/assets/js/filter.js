jQuery(document).ready(function ($) {
    const filterTarget = $(".tk-filter-target");

    if (filterTarget.length > 0) {
        const filterPopup = $(".tk-filter-popup");

        function reloadWithFilters() {
            const form = $(".tk-filter-form");

            const inputs = form.find("input[type=checkbox]");

            const activeTaxonomies = [];
            $.each(inputs, function (index, input) {
                if ($(input).is(':checked')) {
                    const taxonomy = $(input).attr("name");
                    if (activeTaxonomies.indexOf(taxonomy) < 0) {
                        activeTaxonomies.push(taxonomy);
                    }
                }
            });

            const postType = form.find("input[name=post-type]").val();

            const data = {};
            let queryString = "";
            let index = 0;
            activeTaxonomies.forEach((taxonomy) => {

                const slugs = [];
                $.each(inputs, function (index, input) {
                    if ($(input).is(":checked") && $(input).attr("name") === taxonomy) {
                        slugs.push($(input).val());
                    }
                });

                data[taxonomy] = slugs;

                if (index === 0) {
                    queryString += "?";
                } else {
                    queryString += "&";
                }
                const queryParam = $(".tk-filter-form input[name=" + taxonomy + "]").data("tk-query-param");
                queryString += queryParam + "=" + slugs.join(",");
                index++;
            });

            const orderBy = $(".woocommerce-ordering select.orderby option:selected").val();
            if (orderBy) {
                if (queryString.length === 0) {
                    queryString += "?";
                } else {
                    queryString += "&";
                }
                queryString += "orderby=" + orderBy;
            }

            history.replaceState({}, "", location.pathname + queryString);

            $(".tk-ajax-overlay").addClass("tk-showing");
            form.find("input").attr("disabled", true);

            $.ajax({
                url: tkAjax.ajax_url,
                type: 'POST',
                data: {
                    action: "tkFilterArchive",
                    postType: postType,
                    filters: data,
                    orderby: orderBy
                },
                cache: false,
                success: function (data) {
                    $(".tk-filter-target ul.products, ul.tk-filter-target").html(
                        data["list"]
                    );

                    $(".tk-active-filters-wrapper").html(
                        data["filters"]
                    );
                    $(".pagination, .woocommerce-pagination").html("");
                    $(".tk-ajax-overlay").removeClass("tk-showing");
                    form.find("input").attr("disabled", false);
                },
                error: function (jqXHR, textStatus) {
                    alert(textStatus);
                }
            });
        }


        $(document).on("click", ".tk-filter-popup", function (e) {
            e.stopPropagation();
        });

        $(document).on("click", ":not(.tk-filter-popup, .tk-filter-popup *)", function () {
            if (filterPopup.is(":visible") && !$(this).is(".tk-filter-popup") && $(this).parents(".tk-filter-popup").length === 0) {
                filterPopup.slideUp();
            }
        });

        $(document).on("click", ".tk-filter-button", function (e) {
            if (filterPopup.is(":visible")) {
                filterPopup.slideUp();
            } else {
                filterPopup.slideDown();

            }
            e.stopPropagation();
        });

        $(document).on("change", ".tk-filter-form input[type=checkbox]", function (e) {
            e.preventDefault();
            reloadWithFilters();
        });

        setTimeout(function () {
            $(".woocommerce-ordering").off();
        }, 1000);

        $(document).on("change", ".woocommerce-ordering select.orderby", function () {
            const selectedOption = $(this).find("option:selected");
            if (selectedOption.length === 1) {
                reloadWithFilters();
                return false;
            }
        });

        $(document).on("click", ".tk-active-filters .tk-remove-all .tk-remove", function () {
            $(".tk-filters input[type=checkbox]").prop('checked', false);
            reloadWithFilters();
        });

        $(document).on("click", ".tk-active-filters .tk-active-filter .tk-remove", function () {
            const activeFilterElement = $(this).parent();
            /*const taxonomy = activeFilterElement.data("tk-taxonomy");
            const term = activeFilterElement.data("tk-term");*/
            var targetFilter = activeFilterElement.data('tk-for');
            $(".tk-filters input[type=checkbox]#" + targetFilter).prop('checked', false);
            reloadWithFilters();
        });
    }
});