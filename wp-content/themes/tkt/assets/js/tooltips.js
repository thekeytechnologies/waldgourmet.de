jQuery(document).ready(function ($) {

    $(".shipping-costs-info").tooltipster({
        contentAsHTML: true,
        content: `<ul>
<li><strong>Warenkorbwert 0-50€:</strong> 14,95€ Versandkosten</li>
<li><strong>Warenkorbwert 50-100€:</strong> 9,95€ Versandkosten</li>
<li><strong>Warenkorbwert 100-150€:</strong> 4,95€ Versandkosten</li>
<li><strong>Warenkorbwert ab 150€:</strong> kostenloser Versand</li>
</ul>`,
        trigger: 'ontouchstart' in window || navigator.maxTouchPoints ? 'click' : 'hover'
    });

    window.tkReinitializeAvailabilityTooltip = function () {
        $(".tk-product-configuration .tk-availability").tooltipster({
            contentAsHTML: true,
            content: `<p>Weil Qualität ihre Zeit braucht. <strong>Wir arbeiten nur auf Bestellung und können deshalb eine ganz besondere Frische anbieten.</strong></p><p>Momentan folgen wir dabei einem bestimmten Prozess. Für alle Bestellungen, die bis Montag früh 07:00 Uhr eingehen, suchen wir die passenden Wildtiere aus und entfernen anschließend die Felle. Danach kommt der Veterinär zur Fleischuntersuchung. <strong>Kein Tier verlässt unseren Betrieb, das nicht genauestens untersucht wurde.</strong> Am Dienstag beginnen wir mit der Zerlegung. In der Regel übergeben wir dann am Mittwoch die Pakete, so dass sie am Donnerstag bis 12:00 Uhr ankommen. Am Tag davor versenden wir aber in jedem Fall noch eine E-Mail zum Versand.</p><p>Es stimmt, bei der Lieferdauer machen wir Kompromisse. Aber nur, damit wir es bei der Qualität und der Frische nicht müssen.</p>`,
            trigger: 'ontouchstart' in window || navigator.maxTouchPoints ? 'click' : 'hover'
        })
    }
    window.tkReinitializeAvailabilityTooltip();

});


