<?php

/**
 * Add Elementor-Templates Select field to menu item
 *
 * @param int $item_id
 * @params obj $item - the menu item
 * @params array $args
 */
function tkAddElementorTemplatesSelectBoxToMenuItems($item_id, $item)
{

    if ($item->title == 'Mega Menu') {
        wp_nonce_field('custom_menu_meta_nonce', '_custom_menu_meta_nonce_name');
        $custom_menu_meta = get_post_meta($item_id, '_elementor-template', true);
        $args = array(
            'post_type' => 'elementor_library',
            'post_per_page' => -1
        );
        $elementorTemplatesQuery = new WP_Query($args);
        $elementorTemplates = $elementorTemplatesQuery->get_posts();
        ?>
        <input type="hidden" name="custom-menu-meta-nonce"
               value="<?php echo wp_create_nonce('custom-menu-meta-name'); ?>"/>
        <input type="hidden" class="nav-menu-id" value="<?php echo $item_id; ?>"/>
        <br>
        <span class="description">Elementor Template auswählen</span>
        <br/>
        <select id="elementor-template-menu-meta-for-<?php echo $item_id; ?>"
                name="elementor_template_menu_meta[<?php echo $item_id; ?>]"
                value="tisi">
            <option value=""> keins</option>
            <?php
            foreach ($elementorTemplates as $elementorTemplate) {
                ?>
                <option value="<?php echo $elementorTemplate->ID; ?> " <?php if ($custom_menu_meta == $elementorTemplate->ID) echo 'selected'; ?>> <?php echo $elementorTemplate->post_title; ?> </option>
                <?php
            }
            ?>
        </select>
        <?php
    }
}

add_action('wp_nav_menu_item_custom_fields', 'tkAddElementorTemplatesSelectBoxToMenuItems', 10, 2);


/**
 * Save the menu item meta
 *
 * @param int $menu_id
 * @param int $menu_item_db_id
 */
function tkUpdateElementorTemplatesSelectBoxInMenuItems($menu_id, $menu_item_db_id)
{

    // Verify this came from our screen and with proper authorization.
    if (!isset($_POST['_custom_menu_meta_nonce_name']) || !wp_verify_nonce($_POST['_custom_menu_meta_nonce_name'], 'custom_menu_meta_nonce')) {
        return $menu_id;
    }

    if (isset($_POST['elementor_template_menu_meta'][$menu_item_db_id])) {
        $sanitized_data = sanitize_text_field($_POST['elementor_template_menu_meta'][$menu_item_db_id]);
        update_post_meta($menu_item_db_id, '_elementor-template', $sanitized_data);
    } else {
        delete_post_meta($menu_item_db_id, '_elementor-template');
    }
}

add_action('wp_update_nav_menu_item', 'tkUpdateElementorTemplatesSelectBoxInMenuItems', 10, 2);

