<?php

function navMenuColumns_init()
{
    register_post_type('mega_menu',
        array(
            'labels' => array(
                'name' => __('Mega Menu'),
                'singular_name' => __('Mega Menu')
            ),
            'supports' => array('title'),

            // Doesn't need to be listed most places as it's not a valid content item on it's own
            'public' => false, // Base setting. More specific settings below
            'exclude_from_search' => false,
            'publicly_queryable' => false,
            'show_ui' => false,
            'show_in_menu' => false,
            'show_in_nav_menus' => true,  // The only thing this is used for
            'show_in_admin_bar' => false,
            'has_archive' => false,
        )
    );


    navMenuColumns_install10();


    add_action('admin_footer', 'navMenuColumns_adminFooter');
}

function navMenuColumns_install10()
{
    $allMegaMenuPosts = get_posts(array('post_type' => 'mega_menu', 'numberposts' => -1));
    if (!count($allMegaMenuPosts) >= 1) {
        $navMenuPost = wp_insert_post(array(
            'post_type' => 'mega_menu',
            'post_title' => 'Mega Menu',
            'post_status' => 'publish'
        ), true);
    }
}

function navMenuColumns_adminFooter()
{
    ?>
    <script>
        jQuery(document).ready(function ($) {
            // Hides most of the fields when editing a Menu Column item.
            $('#menu-to-edit').on('click', 'a.item-edit', function () {
                var $li = $(this).parents('li.menu-item');

                if ($li.find('.item-type').text() == 'Mega Menu') {
                    $li.find('p.description').hide();
                    $li.find('p.link-to-original').hide();
                    $li.find('p.field-move').show();
                }
            });
        });
    </script><?php
}

add_action('init', 'navMenuColumns_init');

