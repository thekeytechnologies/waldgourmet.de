<?php

class TkMegaMenuWalker extends Walker_Nav_Menu
{
    public function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $classes = implode(' ', $item->classes);
        $elementorTemplateID = get_post_meta($item->ID, '_elementor-template', true);
        $thumbnail = get_the_post_thumbnail_url($item->ID);
        if ($elementorTemplateID != "") {
            $output .= "<li class='tk-mega-menu'>" . do_shortcode('[elementor-template id=' . "$elementorTemplateID" . ']') . "</li>";
        } else {
            $output .= "<li class='tk-menu-item  $classes'>" . '<div class="d-inline-block tk-menu-item-img-wrapper"><img class="tk-menu-item-img" src = "' . $thumbnail . '"  > </div>' . '<a class="elementor-item" href="' . $item->url . '">' . do_shortcode($item->title) . $elementorTemplateID . '</a>';
        }
    }
}

function tkCustomWalker($args)
{
    // check if primary menu
    if ($args['menu'] == 'hauptmenue') {
        $args = array_merge($args, array(
            'walker' => new TkMegaMenuWalker(),
        ));
    }


    return $args;

}

add_filter('wp_nav_menu_args', 'tkCustomWalker');
