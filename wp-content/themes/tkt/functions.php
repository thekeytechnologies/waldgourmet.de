<?php

require_once("waldgourmet/waldgourmet.php");
require_once("woocommerce/text-filters.php");
require_once("_temporary-fix-meta-titles.php");
require_once("tk-menu/_menu.php");

function tkAddFiles()
{
    wp_dequeue_style("Lato");
    wp_dequeue_style("ubermenu-lato");

    wp_enqueue_style("tk-fontawesome", get_theme_root_uri() . '/tkt/css/fontawesome/css/all.css');
    wp_enqueue_style("tk-lato", get_theme_root_uri() . '/tkt/css/lato/css/lato.css');
    wp_enqueue_style("tk-tooltipster", get_theme_root_uri() . '/tkt/css/tooltipster/tooltipster.bundle.min.css');


    if (tkIsWeidegourmet()) {
        wp_enqueue_style('tk-child-style', get_theme_root_uri() . '/tkt/css/weide.css');
    } else {
        wp_enqueue_style('tk-child-style', get_theme_root_uri() . '/tkt/css/wald.css');
    }


    //wp_enqueue_script("sentry", get_theme_root_uri() . '/tkt/js/sentry/bundle.min.js', array("jquery"));
    wp_enqueue_script("tooltipster", get_theme_root_uri() . '/tkt/js/tooltipster/tooltipster.bundle.min.js', array("jquery"));

    wp_enqueue_script("tk-child-script", get_theme_root_uri() . '/tkt/js/tk.js', array("jquery", "tooltipster"));
    wp_localize_script('tk-child-script', 'tkAjax', array('ajax_url' => admin_url('admin-ajax.php')));
}

add_filter('elementor/frontend/print_google_fonts', function () {
    return false;
});


add_action('wp_enqueue_scripts', 'tkAddFiles', 11);

if (function_exists("tkEnableSubnavigation")) {
    tkEnableSubnavigation();
    tkInstallCF7ConversionTracking();
}

add_action('after_setup_theme', function () {
    add_theme_support('woocommerce');
    add_theme_support('post-thumbnails');
});

add_filter('wp_title', function ($title) {
    if (is_404()) {
        $title = 'Seite nicht gefunden!';
    }

    if (is_shop()) {
        $title = "test";
    }

    // You can do other filtering here, or
    // just return $title
    return $title;
});


function tkShowHooks($hookname)
{
    global $wp_filter;
    var_dump($wp_filter[$hookname]);
}

add_action("wp_head", function () {
    ?>
    <link rel="icon" href="/wp-content/uploads/waldgourmets-favicon.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="/wp-content/uploads/waldgourmets-favicon.png" type="image/x-icon"/>
    <?php
});

add_shortcode("tk-cart", function () {
    $wooCommerce = WooCommerce::instance();
    $cart = $wooCommerce ? $wooCommerce->cart : null;
    $cartCount = $cart ? $cart->get_cart_contents_count() : 0;

    return "<div class=\"tk-icon-link tk-cart-link\">
                        <div class=\"tk-cart-count\">$cartCount</div>
                        <img src=\"/wp-content/themes/tkt/assets/img/einkaufskorb.png\">
                </div>";
});

add_filter('nav_menu_item_title', function ($title, $item, $args, $depth) {
    return do_shortcode($title);
}, 10, 4);

add_filter('the_content', 'add_ids_to_header_tags');

add_filter('woocommerce_single_product_image_gallery_classes', function ($classes) {
    global $product;
    $newClasses = [];
    $newClasses[] = $product ? $product->get_shipping_class() : "";
    return array_merge($classes, $newClasses);
}, 10, 1);


//redirect Weidegourmet to Wladgourmet if current user is not Admin
function tkRedirectWeideToWald()
{
    if (!current_user_can('administrator') && !is_login_page() ) {
        $site_title = get_bloginfo('name');
        $location = 'https://www.waldgourmet.de/';
        $status = 302;
        $redirect_by = 'Admin';
        if ($site_title == 'Weidegourmet') {
            wp_redirect($location, $status, $redirect_by);
        }
    }
}

add_action('template_redirect', 'tkRedirectWeideToWald');

function is_login_page() {
    return in_array($GLOBALS['pagenow'], array('wp-login.php'));
}