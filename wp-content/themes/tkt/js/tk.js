jQuery(document).ready(function ($) {

    $(".shipping-costs-info").tooltipster({
        contentAsHTML: true,
        content: `<ul>
<li><strong>Warenkorbwert 0-50€:</strong> 14,95€ Versandkosten</li>
<li><strong>Warenkorbwert 50-100€:</strong> 9,95€ Versandkosten</li>
<li><strong>Warenkorbwert 100-150€:</strong> 4,95€ Versandkosten</li>
<li><strong>Warenkorbwert ab 150€:</strong> kostenloser Versand</li>
</ul>`,
        trigger: 'ontouchstart' in window || navigator.maxTouchPoints ? 'click' : 'hover'
    });

    window.tkReinitializeAvailabilityTooltip = function () {
        $(".tk-product-configuration .tk-availability").tooltipster({
            contentAsHTML: true,
            content: `<p>Weil Qualität ihre Zeit braucht. <strong>Wir arbeiten nur auf Bestellung und können deshalb eine ganz besondere Frische anbieten.</strong></p><p>Momentan folgen wir dabei einem bestimmten Prozess. Für alle Bestellungen, die bis Montag früh 07:00 Uhr eingehen, suchen wir die passenden Wildtiere aus und entfernen anschließend die Felle. Danach kommt der Veterinär zur Fleischuntersuchung. <strong>Kein Tier verlässt unseren Betrieb, das nicht genauestens untersucht wurde.</strong> Am Dienstag beginnen wir mit der Zerlegung. In der Regel übergeben wir dann am Mittwoch die Pakete, so dass sie am Donnerstag bis 12:00 Uhr ankommen. Am Tag davor versenden wir aber in jedem Fall noch eine E-Mail zum Versand.</p><p>Es stimmt, bei der Lieferdauer machen wir Kompromisse. Aber nur, damit wir es bei der Qualität und der Frische nicht müssen.</p>`,
            trigger: 'ontouchstart' in window || navigator.maxTouchPoints ? 'click' : 'hover'
        })
    }
    window.tkReinitializeAvailabilityTooltip();

});



jQuery(document).ready(function ($) {
    const filterTarget = $(".tk-filter-target");

    if (filterTarget.length > 0) {
        const filterPopup = $(".tk-filter-popup");

        function reloadWithFilters() {
            const form = $(".tk-filter-form");

            const inputs = form.find("input[type=checkbox]");

            const activeTaxonomies = [];
            $.each(inputs, function (index, input) {
                if ($(input).is(':checked')) {
                    const taxonomy = $(input).attr("name");
                    if (activeTaxonomies.indexOf(taxonomy) < 0) {
                        activeTaxonomies.push(taxonomy);
                    }
                }
            });

            const postType = form.find("input[name=post-type]").val();

            const data = {};
            let queryString = "";
            let index = 0;
            activeTaxonomies.forEach((taxonomy) => {

                const slugs = [];
                $.each(inputs, function (index, input) {
                    if ($(input).is(":checked") && $(input).attr("name") === taxonomy) {
                        slugs.push($(input).val());
                    }
                });

                data[taxonomy] = slugs;

                if (index === 0) {
                    queryString += "?";
                } else {
                    queryString += "&";
                }
                const queryParam = $(".tk-filter-form input[name=" + taxonomy + "]").data("tk-query-param");
                queryString += queryParam + "=" + slugs.join(",");
                index++;
            });

            const orderBy = $(".woocommerce-ordering select.orderby option:selected").val();
            if (orderBy) {
                if (queryString.length === 0) {
                    queryString += "?";
                } else {
                    queryString += "&";
                }
                queryString += "orderby=" + orderBy;
            }

            history.replaceState({}, "", location.pathname + queryString);

            $(".tk-ajax-overlay").addClass("tk-showing");
            form.find("input").attr("disabled", true);

            $.ajax({
                url: tkAjax.ajax_url,
                type: 'POST',
                data: {
                    action: "tkFilterArchive",
                    postType: postType,
                    filters: data,
                    orderby: orderBy
                },
                cache: false,
                success: function (data) {
                    $(".tk-filter-target ul.products, ul.tk-filter-target").html(
                        data["list"]
                    );

                    $(".tk-active-filters-wrapper").html(
                        data["filters"]
                    );
                    $(".pagination, .woocommerce-pagination").html("");
                    $(".tk-ajax-overlay").removeClass("tk-showing");
                    form.find("input").attr("disabled", false);
                },
                error: function (jqXHR, textStatus) {
                    alert(textStatus);
                }
            });
        }


        $(document).on("click", ".tk-filter-popup", function (e) {
            e.stopPropagation();
        });

        $(document).on("click", ":not(.tk-filter-popup, .tk-filter-popup *)", function () {
            if (filterPopup.is(":visible") && !$(this).is(".tk-filter-popup") && $(this).parents(".tk-filter-popup").length === 0) {
                filterPopup.slideUp();
            }
        });

        $(document).on("click", ".tk-filter-button", function (e) {
            if (filterPopup.is(":visible")) {
                filterPopup.slideUp();
            } else {
                filterPopup.slideDown();

            }
            e.stopPropagation();
        });

        $(document).on("change", ".tk-filter-form input[type=checkbox]", function (e) {
            e.preventDefault();
            reloadWithFilters();
        });

        setTimeout(function () {
            $(".woocommerce-ordering").off();
        }, 1000);

        $(document).on("change", ".woocommerce-ordering select.orderby", function () {
            const selectedOption = $(this).find("option:selected");
            if (selectedOption.length === 1) {
                reloadWithFilters();
                return false;
            }
        });

        $(document).on("click", ".tk-active-filters .tk-remove-all .tk-remove", function () {
            $(".tk-filters input[type=checkbox]").prop('checked', false);
            reloadWithFilters();
        });

        $(document).on("click", ".tk-active-filters .tk-active-filter .tk-remove", function () {
            const activeFilterElement = $(this).parent();
            /*const taxonomy = activeFilterElement.data("tk-taxonomy");
            const term = activeFilterElement.data("tk-term");*/
            var targetFilter = activeFilterElement.data('tk-for');
            $(".tk-filters input[type=checkbox]#" + targetFilter).prop('checked', false);
            reloadWithFilters();
        });
    }
});
jQuery(document).ready(function ($) {
    $(".tk-ajax-overlay-target").append(`<div class="tk-ajax-overlay">
        <div class="tk-spinner">
            <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
        </div>
    </div>`);
});
jQuery(document).ready(function ($) {
    $.ajax({
        url: tkAjax.ajax_url,
        type: 'POST',
        data: {
            action: "tkCartCount",
        },
        cache: false,
        success: function (data) {
            $(".tk-cart-count").html(data["cart-count"]);
        },
        error: function (jqXHR, textStatus) {
        }
    });

    const form = $(".single-product form.tk-add-to-cart");

    function trackAddToCart(product) {
        if (ga !== 'undefined') {
            ga('ec:addProduct', {
                'id': product.id,
                'name': product.name,
                'price': product.price,
                'quantity': product.qty
            });
            ga('ec:setAction', 'add');
            ga('send', 'event', 'UX', 'click', 'add to cart');     // Send data using an event.
        }
        if (fbq !== 'undefined') {
            fbq('track', 'AddToCart');
        }
    }

    function addToCart(data) {

        data["action"] = "tkAddToCart";
        form.attr("disabled", true);
        const button = form.find("button[type=submit]");
        button.removeClass("tk-done");
        button.attr("disabled", true);
        button.addClass("tk-sending");
        $.ajax({
            url: tkAjax.ajax_url,
            type: 'POST',
            data: data,
            cache: false,
            success: function (data) {
                form.attr("disabled", false);
                button.removeClass("tk-sending");
                button.addClass("tk-done");
                button.attr("disabled", false);
                $(".tk-cart-count").html(data["cart-count"]);
                form.find(".added_to_cart").show();
                trackAddToCart(data["product"]);
            },
            error: function (jqXHR, textStatus) {
                alert(textStatus);
            }
        });
    }

    if (form.length > 0) {
        const priceDisplay = form.find(".price:not(.price-unit)");
        const availabilityDisplay = form.find(".tk-availability");
        const addToCartButton = form.find(".tk-add-to-cart-wrapper button");
        const quantityInput = form.find("input[name=quantity]");
        const singleVariation = form.find("input[name=tk-single-variation]");
        const unitDisplay = form.find(".tk-price-unit");
        const originalPriceDisplay = priceDisplay.html();
        const originalAvailabilityDisplay = availabilityDisplay.html();
        const originalUnitDisplay = unitDisplay.html();

        function isVariableProduct() {
            const variations = form.data("product_variations");
            return variations && variations !== "null";
        }

        function isSubscription() {
            var product = $('.product');
            var variation = getSelectedVariation();
            if ((product.hasClass('product-type-variable-subscription') || product.hasClass('product-type-variable-subscription')) && variation && variation.attributes.attribute_ausfuerung != "Einzelbestellung") {
                return true;
            } else {
                return false;
            }
        }

        function getSelectedVariation() {
            const variations = form.data("product_variations");
            if (variations && variations !== "null") {
                const selectedAttributes = {};
                form.find("select").each(function () {
                    selectedAttributes[$(this).attr("name")] = $(this).find("option:selected").val();
                });

                const selectedAttributeForEqual = JSON.stringify(selectedAttributes);
                return $.grep(variations, function (variation) {
                    return JSON.stringify(variation.attributes) === selectedAttributeForEqual;
                })[0];
            }
            return false;
        }

        function changeFormBasedOnSelectedProduct(price, maxQty) {
            let quantity = Number.parseInt(quantityInput.val());
            if (Number.isNaN(quantity)) {
                quantity = 0;
            }
            const fullPrice = price * quantity;

            const isUnlimited = maxQty === -1 || maxQty === "";

            if (isUnlimited) {
                availabilityDisplay.html("Lieferbar innerhalb von 3-8 Werktagen. Warum so lange? <i class=\"far fa-question-circle\"></i>");
            } else {
                quantityInput.attr("max", maxQty);
                availabilityDisplay.html(`${maxQty} verfügbar. Lieferbar innerhalb von 3-8 Werktagen. Warum so lange? <i class="far fa-question-circle"></i>`);
            }
            window.tkReinitializeAvailabilityTooltip ? window.tkReinitializeAvailabilityTooltip() : undefined;

            if (!Number.isNaN(fullPrice) && quantity > 0 && (isUnlimited || quantity <= maxQty)) {
                const basePriceValue = Number.parseInt(unitDisplay.data("tk-base-price-value"));
                const basePriceUnit = unitDisplay.data("tk-base-price-unit");

                if (isSubscription()) {
                    unitDisplay.html(`pro Lieferung`)
                } else if (basePriceValue && basePriceValue) {
                    unitDisplay.html(`für ${basePriceValue * quantity}${basePriceUnit} - ${quantity} ${quantity === 1 ? "Portion" : "Portionen"}`)
                } else {
                    unitDisplay.html(`für ${quantity} Stück`)
                }


                addToCartButton.attr("disabled", false);
                addToCartButton.html("In den Warenkorb");

                priceDisplay.html(fullPrice.toLocaleString('de-DE', {
                    style: 'currency',
                    currency: 'EUR'
                }));
            } else {
                unitDisplay.html(originalUnitDisplay);
                addToCartButton.attr("disabled", true);
                addToCartButton.html("Bitte Menge anpassen");
                priceDisplay.html(originalPriceDisplay);
            }
        }

        function updateDisplays() {
            const selectedVariation = getSelectedVariation();

            if (isVariableProduct() && selectedVariation) {
                const maxQty = selectedVariation.max_qty;
                const price = selectedVariation.display_price;
                changeFormBasedOnSelectedProduct(price, maxQty);
            } else if (!isVariableProduct()) {
                const maxQty = form.data("simple_product_max_quantity");
                const price = form.data("simple_product_price");
                changeFormBasedOnSelectedProduct(Number.parseFloat(price), maxQty);
            } else {
                availabilityDisplay.html(originalAvailabilityDisplay);
                priceDisplay.html(originalPriceDisplay);
                addToCartButton.attr("disabled", true);
                addToCartButton.html("Bitte Variante wählen");
                unitDisplay.html(originalUnitDisplay);
            }
        }

        updateDisplays();

        $(document).on('change', '.single-product form.tk-add-to-cart select', updateDisplays);
        $(document).on('change keyup mouseup scrollwheel', '.single-product form.tk-add-to-cart input[name=quantity]', updateDisplays);

        setTimeout(function () {
            addToCartButton.off();
        }, 1000);

        $(document).on("submit", "form.tk-add-to-cart", function (e) {
            e.preventDefault();

            const selectedVariation = getSelectedVariation();
            const quantity = Number.parseInt(quantityInput.val());
            const productId = form.data("product_id");

            let data = null;
            if (isVariableProduct() && selectedVariation) {
                data = {
                    quantity: quantity,
                    productId: productId,
                    variationId: selectedVariation.variation_id
                }
            } else if (!isVariableProduct()) {
                data = {
                    quantity: quantity,
                    productId: productId,
                }
            }


            if (data) {
                addToCart(data);
            }
        });

        $(document).on("click", '.tk-add-single-variation-link', function (event) {
            event.preventDefault();
            if (singleVariation.length > 0) {
                let singleVariationId = singleVariation.val();
                let data = {
                    quantity: 1,
                    productId: singleVariationId
                }

                addToCart(data);
            }

        })
    }
});

jQuery(document).ready(function ($) {



});
// noinspection JSUnusedGlobalSymbols
function tkInitMap() {
    const map = new google.maps.Map(document.getElementById("tk-source-map"), {
        center: {lat: 52.482443, lng: 11.425307},
        zoom: 13,
        disableDefaultUI: true
    });
}

jQuery(document).ready(function ($) {
    $(document).on('submit', '#tk-product-source-form', function (e) {
        const form = $("#tk-product-source-form");
        const submitButton = form.find("button");

        const originalButtonContent = submitButton.html();
        const tagIdInput = form.find("input[type=text]");
        const spinnerIcon = form.find("i");
        const allInputsInForm = form.find(":input");
        const googleMapElement = $("#tk-source-map");
        const notFound = $(".tk-not-found");
        spinnerIcon.show();
        allInputsInForm.attr("disabled", true);

        notFound.hide();
        submitButton.addClass("tk-is-loading");
        submitButton.html("<i class=\"fa fa-circle-o-notch fa-spin fa-3x fa-fw\"></i>");

        jQuery.post("/wp-admin/admin-ajax.php", {
            "action": "get_gps_coords",
            "tagId": tagIdInput.val()
        }, function (response) {
            response = JSON.parse(response);
            if (response && response.lat && response.lon) {
                googleMapElement.empty();
                const map = new google.maps.Map(googleMapElement.get(0), {
                    zoom: 15,
                    scrollwheel: false,
                    disableDefaultUI: true
                });

                const marker = new google.maps.Marker({
                    position: {lat: response.lat, lng: response.lon},
                    map: map
                });
                const bounds = new google.maps.LatLngBounds();
                bounds.extend(marker.getPosition());

                const infowindow = new google.maps.InfoWindow({
                    content: "<div class='tk-map-popup'>" + response.description + "</div>"
                });

                map.fitBounds(bounds);

                const listener = google.maps.event.addListener(map, "idle", function () {
                    if (map.getZoom() > 14) map.setZoom(14);
                    infowindow.open(map, marker);
                    google.maps.event.removeListener(listener);
                });
            } else {
                notFound.show();
            }
            submitButton.removeClass("tk-is-loading");
            submitButton.html(originalButtonContent);
            spinnerIcon.hide();
            allInputsInForm.attr("disabled", false);
        });
        e.preventDefault();
    })
});
jQuery(document).ready(function ($) {
    $(".tk-bulk-order-form").submit(function (e) {
        e.preventDefault();

        const products = [];
        const inputs = $(".tk-bulk-order-form .tk-addable input[type=number]");

        const submitButton = $(".tk-bulk-order-form button[type=submit]");

        submitButton.attr("disabled", true);
        submitButton.addClass("tk-sending");

        inputs.each(function (index, input) {

            if (!$(input).is('[readonly]')) {
                const productId = $(input).data("tk-product-id");
                const variationId = $(input).data("tk-variation-id");
                products.push({
                    "productId": productId,
                    "variationId": variationId,
                    "quantity": Math.round($(input).val() / $(input).data("tk-portion-size"))
                });
            }

        });

        $.ajax({
            url: tkAjax.ajax_url,
            type: 'POST',
            data: {
                "action": "tkBulkOrder",
                "products": products
            },
            cache: false,
            success: function (data) {
                document.location.href = "/warenkorb/";
            },
            error: function (jqXHR, textStatus) {
                submitButton.removeClass("tk-sending");
                submitButton.attr("disabled", false);
                alert(textStatus);
            }
        });
        return false;
    });

    $(".tk-bulk-order-form .tk-addable input[type=number]").change(function(event) {
        const targetInput = event.target;
        const variationId = $(targetInput).data("tk-variation-id");
        const group = $(targetInput).closest('.tk-product-wrapper');

        let targetVal = tkStringToNumber($(targetInput).val());
        let targetMax = tkStringToNumber($(targetInput).attr('max'));
        let step = tkStringToNumber($(targetInput).attr('step'));

        /* adjust to nearest step to correct manual inputs */
        if (targetVal % step !== 0) {
            targetVal = tkRoundToNearest(targetVal, step);
            $(targetInput).val(targetVal);
        }

        if (variationId) {
            const parentInput = $(group).find('.tk-variable-product-parent-row input[type=number]');
            const childInputs = $(group).find('.tk-variations input[type=number]');

            /* Adjust Amounts and Max Amounts */
            const max = tkStringToNumber($(parentInput).attr('max'));
            let sum = 0;

            childInputs.each(function (index, input) {
                let val = tkStringToNumber($(input).val());
                sum += val;
            });

            /* correct sum and current field value */
            if (sum > max) {
                sum = max;
                $(targetInput).val(Number($(targetInput).attr('max')).toFixed(1));
            }

            $(parentInput).val(sum.toFixed(1));

            childInputs.each(function (index, input) {
                let val = tkStringToNumber($(input).val());
                let newMax = max - (sum - val);
                $(input).attr('max', newMax.toFixed(1));
            });
        }

        tkUpdatePrices(targetInput);
    });

    function tkStringToNumber(str) {
        str = String(str).replace(',', '.');
        return Number(str);
    }

    function tkNumberToString(number, decimalPlaces = 1) {
        number = Number(number).toFixed(decimalPlaces);
        number = number.replace('.', ',');
        return number;
    }

    function tkRoundToNearest( number, multiple, decimal = 2 ){
        return Number((Math.round(number/multiple)*multiple).toFixed(decimal));
    }

    function tkUpdatePrices(targetInput) {
        const variationId = $(targetInput).data("tk-variation-id");
        const productRow = $(targetInput).closest('.tk-product-row')
        const singlePriceWrapper = $(productRow).find('.tk-price-col .tk-price-container');
        const totalPriceWrapper = $(productRow).find('.tk-total-col .tk-price-container');

        let amount = tkStringToNumber($(targetInput).val());
        let singlePrice = tkGetPriceFromWrapper(singlePriceWrapper);
        let newPrice = singlePrice * amount;
        tkSetTotalPrice(totalPriceWrapper, newPrice);

        if(variationId) {
            const group = $(targetInput).closest('.tk-product-wrapper');
            const parentTotalPriceWrapper = $(group).find('.tk-variable-product-parent-row .tk-total-col .tk-price-container');
            const childTotalPriceWrappers = $(group).find('.tk-variations .tk-total-col .tk-price-container');

            let sum = 0;
            childTotalPriceWrappers.each(function (index, wrapper) {
                let price = tkGetPriceFromWrapper(wrapper);
                sum += price;
            });

            tkSetTotalPrice(parentTotalPriceWrapper,sum)
        }

    }

    function tkGetPriceFromWrapper(wrapper) {
        let content = $(wrapper).text();
        return tkStringToNumber(content);
    }

    function tkSetTotalPrice(wrapper, price) {
        let priceString = tkNumberToString(price,2);
        $(wrapper).text(priceString);
    }
});
jQuery(function ($) {

    $('#tkMainNav').find('.elementor-nav-menu--dropdown.elementor-nav-menu__container').addClass('tk-menu-overlay')
    $('.elementor-menu-toggle').click(function(){
        $('.tk-menu-overlay').toggleClass('active');
        $('.attachment-full').toggleClass('tk-hide');
    })
    $('ul:has(li.tk-mega-menu)').addClass('has-mega-menu');
});