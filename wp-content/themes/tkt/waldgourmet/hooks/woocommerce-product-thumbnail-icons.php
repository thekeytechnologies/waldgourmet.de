<?php

function tk_product_thumbnail_icons()
{
    global $product;

    switch (get_class($product)) {
        case "WC_Product_Variable":
        case "WC_Product_Simple":
            $productID = $product->get_id();
            break;
        default:
            $productID = $product->ID;
    }


    $infoIcon = wp_get_attachment_image(get_option('tk-wg-settings_tk-info-icon')[0], 'tk-category-icon');
    $prepIcon = wp_get_attachment_image(tkWpMeta($productID, 'tk-product-listing-preparation-icon')["ID"], 'tk-category-icon');

    $html = "";
    if ($infoIcon || $prepIcon) {
        $html .= "<div class='tk-product-box-icons'>";

        if ($infoIcon) {
            $html .= "<div class='tk-product-box-icon-wrapper tk-info-icon'>$infoIcon</div>";
        }

        if ($prepIcon) {
            $html .= "<div class='tk-product-box-icon-wrapper tk-prep-icon'>$prepIcon</div>";
        }

        $html .= "</div>";
    }

    echo $html;
}

add_action('woocommerce_before_shop_loop_item_title', 'tk_product_thumbnail_icons', 11);
