<?php /** @noinspection ALL */

function tkLieferdatumLogic($textFactory)
{
    $dayOfMonth = intval(date("d"));
    $monthOfYear = intval(date("m"));
    $timeOfDay = intval(date('H'));
    $dayOfWeek = intval(date("w"));
    $year = intval(date("Y"));

    if (($monthOfYear == 12 and $dayOfMonth > 20) or ($monthOfYear == 1 and $dayOfMonth < 11)) {
        $deliveryDate = new DateTime("14.01." . ($year + 1));
    } else {
        /** @noinspection PhpDuplicateSwitchCaseBodyInspection */
        switch ($dayOfWeek) {
            case 0: //Sunday
                $deliveryDate = new DateTime("next Thursday");
                break;
            case 1: //Monday
                if ($timeOfDay < 7) {
                    $deliveryDate = new DateTime("next Thursday");
                } else {
                    $deliveryDate = new DateTime("next Thursday");
                    $deliveryDate->modify("+7 days");
                }
                break;
            case 2: //Tuesday
            case 3: //Wednesday
                $deliveryDate = new DateTime("next Thursday");
                $deliveryDate->modify("+7 days");
                break;
            case 4: //Thursday
            case 5: //Friday
            case 6: //Saturday
                $deliveryDate = new DateTime("next Thursday");
                break;

        }
    }

    return $textFactory($deliveryDate->format('d.m.Y'));
}

function tkShowLieferdatumHinweis()
{
    echo tkLieferdatumLogic(
        function ($deliveryDate) {
            return "<div>Wenn Sie jetzt bestellen und mit <strong>PayPal oder Kreditkarte</strong> zahlen, wird Ihre Bestellung voraussichtlich am <strong>$deliveryDate</strong> zwischen 08.00 und 10.00 Uhr frisch geliefert. Ansonsten versenden wir die Bestellung 5-7 Werktage nach Eingang der Zahlung. Auf Wunsch ist auch eine Zustellung an einem späteren Tag möglich. Füllen Sie dazu einfach das Feld direkt unter diesem Text aus. <small><a href='/faq/#fragen-zum-versand' target='_blank'>Fragen zu Lieferungen?</a></small></div>";
        }
    );
}

add_action('woocommerce_before_order_notes', "tkShowLieferdatumHinweis", 10);


add_action("woocommerce_email_order_details", function (WC_Order $order) {
    if ($order->is_paid() or in_array($order->get_payment_method(), array("stripe", "paypal"))) {
        echo tkLieferdatumLogic(
            function ($deliveryDate) {
                return "<p style='margin: 0px 0px 16px;'>Ihre Bestellung wird voraussichtlich am <strong>$deliveryDate</strong> zwischen 08.00 und 10.00 Uhr frisch geliefert.</p>";
            }
        );
    } else {
        echo "<p style='margin: 0px 0px 16px;'>Ihre Bestellung wird nach Zahlungseingang innerhalb von 5-7 Werktagen geliefert. Das genaue Lieferdatum steht in der \"Zahlung erhalten\" E-Mail, die Sie von uns bekommen werden.</p>";
    }
}, 1, 1);
