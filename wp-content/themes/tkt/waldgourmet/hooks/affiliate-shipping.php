<?php
add_filter('tk_filter_cart_shipping_methods', 'tkFilterShippingMethods', 10, 2);

define("TK_NAME_OF_AFFILIATE_SHIPPING_METHOD", "Versand direkt ins Geschäft");

/**
 * @param WC_GZD_Shipping_Rate[] $shippingMethods
 * @return WC_GZD_Shipping_Rate[]
 */
function tkFilterShippingMethods(array $shippingMethods)
{
    return array_filter(array_map(function (WC_Shipping_Rate $rate) {
        if ($rate->get_label() == TK_NAME_OF_AFFILIATE_SHIPPING_METHOD) {
            $currentAffiliateUserMeta = tkGetCurrentAffiliateUserMeta();
            if ($currentAffiliateUserMeta && $currentAffiliateUserMeta["tk-is-shipping-affiliate"]) {
                $rate->set_label("Versand zu " . $currentAffiliateUserMeta["nickname"][0]);
                return $rate;
            } else {
                return null;
            }
        }
        return $rate;
    }, $shippingMethods), function ($rate) {
        return $rate != null;
    });
}


add_action('woocommerce_checkout_create_order', 'tkAddOrderMetaOnPickup', 10, 2);
function tkAddOrderMetaOnPickup(WC_Order $order, $data)
{
    $shippingMethod = $order->get_shipping_method();
    if ($shippingMethod == TK_NAME_OF_AFFILIATE_SHIPPING_METHOD) {
        $currentAffiliateUserMeta = tkGetCurrentAffiliateUserMeta();

        $order->set_shipping_company($currentAffiliateUserMeta["shipping_company"][0]);
        $order->set_shipping_address_1($currentAffiliateUserMeta["shipping_address_1"][0]);
        $order->set_shipping_address_2($currentAffiliateUserMeta["shipping_address_2"][0]);
        $order->set_shipping_city($currentAffiliateUserMeta["shipping_city"][0]);
        $order->set_shipping_postcode($currentAffiliateUserMeta["shipping_postcode"][0]);
        $order->set_shipping_country($currentAffiliateUserMeta["shipping_country"][0]);
    }

    return $order;
}


function tkGetCurrentAffiliateUserMeta()
{
    $currentAffiliate = tkGetCurrentAffiliateId();

    if ($currentAffiliate) {
        $affiliateUser = affwp_get_affiliate_user_id($currentAffiliate);
        return get_user_meta($affiliateUser);
    }
    return null;
}

function tkGetCurrentAffiliateId()
{
    if (function_exists("affiliate_wp")) {
        $affiliateId = affiliate_wp()->tracking->get_affiliate_id();
        if (!$affiliateId and $_GET[affiliate_wp()->tracking->get_referral_var()]) {
            $affiliateId = $_GET[affiliate_wp()->tracking->get_referral_var()];
        }
        if ($affiliateId) {
            return affiliate_wp()->affiliates->get($affiliateId);
        }
    }
    return null;
}


function tkGetCurrentReferralId($entryId)
{
    if ($entryId and function_exists("affiliate_wp")) {
        $referral = affiliate_wp()->referrals->get_by("reference", $entryId);

        if ($referral) {
            return $referral->referral_id;
        }
    }
    return "";
}

function tkShippingNameById($shipping_id)
{

    $rate = tkShippingRateById($shipping_id);
    return $rate ? $rate->get_label() : "";
}

/**
 * @param string $shippingId
 * @return WC_Shipping_Rate|null
 */
function tkShippingRateById($shippingId)
{
    $packages = WC()->shipping()->get_packages();

    foreach ($packages as $i => $package) {
        if (isset($package['rates']) && isset($package['rates'][$shippingId])) {
            return $package['rates'][$shippingId];
        }
    }

    return null;
}
