<?php


function tkAddMoreToCartIncentive()
{
    $woocommerce = WooCommerce::instance();

    $subt = $woocommerce->cart->subtotal;

    $chosenShippingMethod = WC()->session->get('chosen_shipping_methods')[0];
    $rate = tkShippingRateById($chosenShippingMethod);

    $isNormalShipping = $rate && $rate->get_label() == "Versand per DHL-Päckchen";

    if ($isNormalShipping) {
        if ($subt > 0) {
            if ($subt < 50) {
                $difference = number_format(50 - $subt, 2, ",", ".");
                wc_print_notice(__("Unser Fleisch wird Ihnen mit DHL in einer biologisch abbaubaren Verpackung geliefert. 
             Wenn der Bestellwert größer als 50€ ist, ist der Versand kostenlos. Fügen Sie noch Waren im Wert von ${difference}€ hinzu um davon zu profitieren.", 'woocommerce'), 'notice');
            }
        }
    } else {
        if ($subt > 0) {
            if ($subt < 50) {
                $difference = number_format(50 - $subt, 2, ",", ".");
                wc_print_notice(__("Unser Fleisch wird Ihnen frisch, gekühlt und in einer biologisch abbaubaren Verpackung geliefert. 
             Wenn der Bestellwert größer als 50€ ist, beträgt die Versandkostenpauschale 9,95€. Fügen Sie noch Waren im Wert von ${difference}€ hinzu um davon zu profitieren.", 'woocommerce'), 'notice');
            } else if ($subt < 100) {
                $difference = number_format(100 - $subt, 2, ",", ".");
                wc_print_notice(__("Unser Fleisch wird Ihnen frisch, gekühlt und in einer biologisch abbaubaren Verpackung geliefert. 
             Wenn der Bestellwert größer als 100€ ist, beträgt die Versandkostenpauschale 4,95€. Fügen Sie noch Waren im Wert von ${difference}€ hinzu um davon zu profitieren.", 'woocommerce'), 'notice');
            } else if ($subt < 150) {
                $difference = number_format(150 - $subt, 2, ",", ".");
                wc_print_notice(__("Unser Fleisch wird Ihnen frisch, gekühlt und in einer biologisch abbaubaren Verpackung geliefert. 
             Wenn der Bestellwert größer als 150€ ist, ist der Versand kostenlos. Fügen Sie noch Waren im Wert von ${difference}€ hinzu um davon zu profitieren.", 'woocommerce'), 'notice');
            }
        }
    }


}

add_action('woocommerce_before_cart_table', "tkAddMoreToCartIncentive", 10);
