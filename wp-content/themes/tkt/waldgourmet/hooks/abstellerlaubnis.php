<?php
add_action('woocommerce_after_order_notes', function ($checkout) {
    woocommerce_form_field('abstellerlaubnis', array(
        'type' => 'checkbox',
        'required' => false,
        'class' => array('form-row-wide'),
        'label' => 'Abstellerlaubnis  geben - bitte Details im Feld darüber eintragen',
        'label_class' => '',
    ), $checkout->get_value('advertising'));
});

add_action('woocommerce_checkout_update_order_meta', function ($order_id) {
    if (!empty($_POST['abstellerlaubnis'])) {
        update_post_meta($order_id, 'tk-abstellerlaubnis', 1);
    }
});


add_action('woocommerce_after_order_itemmeta', function ($item_id, $item, $product) {
    // Only for "line item" order items
    if (!$item->is_type('line_item')) return;

    // Only for backend and  for product ID 123
    if ($product->get_id() == 123 && is_admin())
        echo '<a href="http://example.com/new-view/?id=' . $item->get_order_id() . '">' . __("Click here to view this") . '</a>';
}, 20, 3);


add_action('woocommerce_admin_order_data_after_shipping_address', function ($order) {
    $abstellErlaubnis = get_post_meta($order->get_id(), 'tk-abstellerlaubnis', true);
    echo '<strong>Abstellerlaubnis:</strong> ' . ($abstellErlaubnis ? "Ja" : "Nein");
}, 10, 1);
