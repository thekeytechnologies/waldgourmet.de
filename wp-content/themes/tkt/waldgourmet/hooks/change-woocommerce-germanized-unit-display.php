<?php
add_filter("woocommerce_gzd_hide_product_units_text", function () {
    return true;
}, 10, 2);
add_filter("woocommerce_germanized_disabled_product_units_text", function ($output, WC_GZD_Product $product) {
    $wcProduct = $product->get_wc_product();

    $id = $wcProduct->get_id();
    if ($wcProduct instanceof WC_Product_Variation) {
        $id = $wcProduct->get_parent_id();
    }
    $re = '/\(.*\)/m';

    $basePriceUnitTitle = get_post_meta($id, "tk-base-price-unit-title", true);
    $basePriceUnitTitle = trim(preg_replace($re, "", $basePriceUnitTitle));

    return $product->has_unit_product() ?
        $product->get_unit_product() . " " . $product->get_unit() . " " . $basePriceUnitTitle :
        "";

}, 10, 2);
