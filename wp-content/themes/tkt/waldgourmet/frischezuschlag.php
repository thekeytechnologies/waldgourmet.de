<?php

function tkFrischezuschlag()
{
    $woocommerce = WooCommerce::instance();
    if (tkFrischeZuschlagApplies()) {
        $surcharge = 25 - $woocommerce->cart->subtotal;
        WC()->cart->add_fee("Kleinstmengenzuschlag für Warenwert kleiner 25€", $surcharge, false);
    }
}

add_action('woocommerce_cart_calculate_fees', 'tkFrischezuschlag');


function tkFrischezuschlagMessage()
{
    if (tkFrischeZuschlagApplies()) {
        wc_print_notice("Da eine ressourcenschonende Verarbeitung und eine umweltgerechte Verpackung bei
                kleinen Bestellmengen stark erschwert werden, runden wir den Warenkorbwert bei Lieferung auf 25€ auf.
                Das ermöglicht uns, keinen Mindestbestellwert festlegen zu müssen. Vielleicht finden Sie ja noch etwas
                anderes im <a href=\"/shop/\">Shop</a>?", "notice");
    }
}

function tkFrischeZuschlagApplies()
{
    $woocommerce = WooCommerce::instance();

    $subt = $woocommerce->cart->subtotal;

    $chosen_methods = $woocommerce->session->get('chosen_shipping_methods');
    $chosen_shipping = $chosen_methods[0];

    return startsWith($chosen_shipping, "flexible") && $subt < 25;
}

add_action('woocommerce_before_cart_table', 'tkFrischezuschlagMessage', 10);
