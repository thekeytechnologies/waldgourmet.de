<?php

add_filter('elementor/utils/get_the_archive_title', function ($title) {
    if (is_shop()) {
        $title = get_option("tk-wg-settings_tk-shop-title");
    } else if (is_archive() && get_post_type() == "tk-recipe") {
        $title = get_option("tk-wg-settings_tk-recipe-archive-title");
    }

    return $title;
});

function woocommerce_product_archive_description()
{
    // Don't display the description on search results page.
    if (is_search()) {
        return;
    }
    if (is_shop()) {
        echo wc_format_content(get_option("tk-wg-settings_tk-shop-text"));
        return;
    }


    if (is_post_type_archive('product') && in_array(absint(get_query_var('paged')), array(0, 1), true)) {
        $shop_page = get_post(wc_get_page_id('shop'));
        if ($shop_page) {
            $description = wc_format_content($shop_page->post_content);
            if ($description) {
                echo '<div class="page-description">' . $description . '</div>'; // WPCS: XSS ok.
            }
        }
    }
}