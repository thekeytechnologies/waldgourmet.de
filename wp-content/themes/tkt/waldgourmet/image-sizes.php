<?php

add_image_size("tk-post-box", 220, 155, array("center", "center"));

add_image_size("tk-large-post-box", 180, 280, true);

add_image_size("tk-post-box-large", 290, 240, array("center", "center"));

add_image_size("tk-shop-thumbnail", 130, 90);
add_image_size("tk-shop-archive", 400, 260, true);
add_image_size("tk-category-icon", 50, 50);
add_image_size("tk-recipe-header-image", 750, 500, array("center", "center"));


add_filter("woocommerce_gallery_thumbnail_size", function () {
    return "tk-shop-thumbnail";
}, 1);