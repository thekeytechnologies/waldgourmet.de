<?php

function tkFilterArchive()
{
    $postType = $_POST["postType"];
    $filters = $_POST["filters"];
    $orderBy = $_POST["orderby"];


    $inStock = false;
   if (!empty($filters['inStock'])) {
        if ($filters['inStock'][0] == '1') {
            $inStock = true;

            $metaquery = array(
                array(
                    'key' => '_stock_status',
                    'value' => 'instock'
                ),
                array(
                    'key' => '_backorders',
                    'value' => 'no'
                ),
            );
        }
        unset($filters['inStock']);
    }
    $taxQuery = array(
        "relation" => "AND",
    );

    $displayFilters = array();
    foreach ($filters as $taxonomy => $slugs) {
        $taxQuery[] = array(
            'taxonomy' => $taxonomy,
            'field' => 'slug',
            'terms' => $slugs,
            'operator' => 'IN'
        );

        foreach ($slugs as $slug) {
            $term = get_term_by("slug", $slug, $taxonomy);
            $filterValue = array("slug" => $term->slug, "name" => $term->name);
            if (isset($filters[$taxonomy])) {
                $displayFilters[$taxonomy][] = $filterValue;
            } else {
                $displayFilters[$taxonomy] = array($filterValue);
            }
        }
    }

    $query = array(
        "post_type" => $postType,
        "posts_per_page" => -1
    );

    if ($orderBy) {
        if ($orderBy == "price") {
            $query["orderby"] = "meta_value_num";
            $query["meta_key"] = "_regular_price";
            $query["order"] = "ASC";
        } else if ($orderBy == "price-desc") {
            $query["orderby"] = "meta_value_num";
            $query["meta_key"] = "_regular_price";
            $query["order"] = "DESC";
        } else if ($orderBy == "popularity") {
            $query["orderby"] = 'meta_value_num';
            $query['meta_key'] = 'total_sales';
            $query["order"] = "desc";
        } else {
            $query["orderby"] = $orderBy;

        }
    }

    if (!empty($filters)) {
        $query['tax_query'] = $taxQuery;
    }



    if ($metaquery) {
        $query['meta_query'] = $metaquery;
    }


    $posts = new WP_Query($query);
    $GLOBALS['wp_query'] = $posts;


    $html = "";

    if (!$posts->have_posts()) {
        $html .= "<p class='tk-no-results'>Keine passenden Ergebnisse gefunden, bitte passen Sie die Filter an.</p>";
    }

    while ($posts->have_posts()) {
        $posts->the_post();
        if ($postType == "product") {
            global $product;
            $product = WooCommerce::instance()->product_factory->get_product(get_the_ID());
            $html .= wc_get_template_html('content-product.php', array(), "woocommerce");
        } else if ($postType == "tk-recipe") {
            global $recipe;
            $recipe = get_post();
            $html .= wc_get_template_html('content-recipe.php', array(), "includes");
        }
    }

    $name = "";
    if ($postType == "product") {
        $name = "Produkt";
    } else if ($postType == "tk-recipe") {
        $name = "Rezept";
    }

    $response = array();

    $response["list"] = $html;

    global $tkTwig;

    $response["filters"] = $tkTwig->renderTemplate("active-filters.twig", array(
        "resultCount" => $posts->post_count,
        "filters" => $displayFilters,
        "name" => $name,
        "inStock" => $inStock
    ));

    wp_send_json($response);
}

add_action('wp_ajax_tkFilterArchive', 'tkFilterArchive');
add_action('wp_ajax_nopriv_tkFilterArchive', 'tkFilterArchive');