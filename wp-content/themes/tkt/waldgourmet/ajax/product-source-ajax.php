<?php
function getGpsCoords()
{
    $tagId = $_POST["tagId"];

    $post = get_page_by_title($tagId, "OBJECT", "tk-product-source");

    if ($post instanceof WP_Post) {
        $postId = $post->ID;
        $gpsCoords = get_post_meta($postId, "tk-gps-coords", true);
        $gpsCoords = explode(",", $gpsCoords);
        $lat = trim($gpsCoords[0]);
        $lon = trim($gpsCoords[1]);
        $description = get_post_meta($postId, "tk-description", true);

        echo json_encode(array("lat" => (double)$lat, "lon" => (double)$lon, "description" => $description));
    } else {
        echo json_encode(array());
    }

    wp_die();
}

add_action('wp_ajax_get_gps_coords', 'getGpsCoords');
add_action('wp_ajax_nopriv_get_gps_coords', 'getGpsCoords');