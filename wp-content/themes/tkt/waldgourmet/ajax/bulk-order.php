<?php

function tkBulkOrder()
{
    $products = $_POST["products"];

    if (isset($products)) {
        global $woocommerce;

        foreach ($products as $product) {
            $woocommerce->cart->add_to_cart($product["productId"], $product["quantity"], $product["variationId"]);
        }
    }

}

add_action('wp_ajax_tkBulkOrder', 'tkBulkOrder');
add_action('wp_ajax_nopriv_tkBulkOrder', 'tkBulkOrder');