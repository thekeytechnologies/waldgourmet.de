<?php

function tkAddToCart()
{
    $quantity = $_POST["quantity"];
    $productId = $_POST["productId"];

    if (isset($quantity) and isset($productId)) {
        $product = new WC_Product($productId);

        if (!$product) {
            wp_die(new WP_Error(404, "product id unknown"));
        }

        global $woocommerce;
        if (!$woocommerce->cart->add_to_cart($productId, $quantity, $_POST["variationId"])) {
            wp_die(new WP_Error(500, "Fehler beim hinzufügen - bitte laden Sie die Seite neu"));
        }

        $productsToAdvertise = array();
        foreach ($product->get_upsell_ids() as $crossSellId) {
            $upsell = new WC_Product($crossSellId);
            if ($upsell->is_in_stock()) {
                $productsToAdvertise[] = $upsell;
            }
        }
        foreach ($product->get_cross_sell_ids() as $crossSellId) {
            $crosssell = new WC_Product($crossSellId);
            if ($crosssell->is_in_stock()) {
                $productsToAdvertise[] = $crosssell;
            }
        }

        $productsToAdvertise = array_slice($productsToAdvertise, 0, 2);

        $response = array(
            
            "cart-count" => WooCommerce::instance()->cart->get_cart_contents_count(),
            "product" => array(
                "id" => $product->get_id(),
                "name" => $product->get_name(),
                "price" => $product->get_price(),
                "quantity" => $quantity
            )
        );

        wp_send_json($response);
    }

}

add_action('wp_ajax_tkAddToCart', 'tkAddToCart');
add_action('wp_ajax_nopriv_tkAddToCart', 'tkAddToCart');