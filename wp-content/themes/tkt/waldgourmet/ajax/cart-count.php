<?php

function tkCartCount()
{
    wp_send_json(array(
        "cart-count" => WooCommerce::instance()->cart->get_cart_contents_count()
    ));
}

add_action('wp_ajax_tkCartCount', 'tkCartCount');
add_action('wp_ajax_nopriv_tkCartCount', 'tkCartCount');