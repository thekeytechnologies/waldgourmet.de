<?php

add_shortcode("tk-product-slider", function ($atts) {
    $productIds = array_map("intval", array_map("trim", explode(",", $atts["ids"])));

    global $tkProductElementContext;
    $tkProductElementContext = "div";

    $html = "<div class='tk-product-slider'>";
    foreach ($productIds as $productId) {
        global $product;
        $product = WooCommerce::instance()->product_factory->get_product($productId);
        $html .= wc_get_template_html('content-product.php', array(), "woocommerce");
    }
    $html .= "</div>";
    return $html;
});