<?php

add_shortcode("tk-large-post-box", function ($atts) {
    $id = $atts["id"];
    $post = get_post($id);
    $imageSize = isset($atts["image_size"]) ? $atts["image_size"] : null;
    $imageSize_tablet = isset($atts["image_size_tablet"]) ? $atts["image_size_tablet"] : null;
    $imageSize_mobile = isset($atts["image_size_mobile"]) ? $atts["image_size_mobile"] : null;
    $imageSize_mobile_landscape = isset($atts["imageSize_mobile_landscape"]) ? $atts["imageSize_mobile_landscape"] : null;

    global $tkTwig;
    return $tkTwig->renderTemplate("tk-large-post-box.twig", array(
            "post" => $post,
            "imageSize" => $imageSize,
            "imageSize_tablet" => $imageSize_tablet,
            "imageSize_mobile" => $imageSize_mobile,
            "imageSize_mobile_landscape" => $imageSize_mobile_landscape,
            )
    );
});