<?php

add_shortcode("tk-post-box", function ($atts) {
    $id = $atts["id"];
    $post = get_post($id);
    $linkAtBottom = isset($atts["link_at_bottom"]) ? $atts["link_at_bottom"] : false;
    $normalTitle = isset($atts["normal_title"]) ? $atts["normal_title"] : false;
    $linkTitle = isset($atts["link_title"]) ? $atts["link_title"] : null;
    $imageSize = isset($atts["image_size"]) ? $atts["image_size"] : null;
    $imageSize_tablet = isset($atts["image_size_tablet"]) ? $atts["image_size_tablet"] : null;
    $imageSize_mobile = isset($atts["image_size_mobile"]) ? $atts["image_size_mobile"] : null;
    $imageSize_mobile_landscape = isset($atts["imageSize_mobile_landscape"]) ? $atts["imageSize_mobile_landscape"] : null;
    global $tkTwig;
    return $tkTwig->renderTemplate("tk-post-box.twig", array(
            "post" => $post,
            "linkAtBottom" => $linkAtBottom,
            "normalTitle" => $normalTitle,
            "linkTitle" => $linkTitle,
            "imageSize" => $imageSize,
            "imageSize_tablet" => $imageSize_tablet,
            "imageSize_mobile" => $imageSize_mobile,
            "imageSize_mobile_landscape" => $imageSize_mobile_landscape,
        )
    );
});