<?php

add_shortcode("tk-archive-excerpt", function () {
    if (is_shop()) {
        $value = get_option("tk-wg-settings_tk-shop-excerpt");
    } else if (is_archive() && get_post_type() == "tk-recipe") {
        $queriedObject = get_queried_object();
        if ($queriedObject instanceof WP_Post_Type) {
            $value = get_option("tk-wg-settings_tk-recipe-archive-excerpt");
        } else if ($queriedObject instanceof WP_Term) {
            $value = get_term_meta(get_queried_object_id(), "tk-excerpt", true);
        }
    } else {
        $value = get_term_meta(get_queried_object_id(), "tk-excerpt", true);
    }

    return "<p class='tk-archive-excerpt'>" . $value . "</p>";
});

add_shortcode("tk-archive-title", function () {
    $queriedObject = get_queried_object();
    if ($queriedObject instanceof WP_Post_Type) {
        if ($queriedObject->name == "tk-recipe") {
            return get_option("tk-wg-settings_tk-recipe-archive-title");
        } else {
            return $queriedObject->label;
        }
    } else if ($queriedObject instanceof WP_Term) {
        return $queriedObject->name;
    }
    return get_the_title();
});