<?php

function tkApplyCustomDiscount($price, $product) {
    $user = wp_get_current_user();
    $discount = tkWpMeta($user, 'tk-custom-discount');

    if ($discount) {
        if ($price == "") {
            $price = $product->get_regular_price();
        }

        $price = $price * (1 - ($discount / 100));
    }

    return $price;
}


/* Only do this on checkout, cart and gastronomentabelle */

add_action('wp', function () {

    if (is_checkout() || is_cart() || tkIsGastronomentabelle() ||(isset($_REQUEST['wc-ajax']) && ($_REQUEST['wc-ajax'] == 'get_refreshed_fragments' || $_REQUEST['wc-ajax'] == 'update_order_review' || $_REQUEST['wc-ajax'] == 'checkout'))) {
        add_filter('woocommerce_product_get_sale_price', 'tkApplyCustomDiscount', 11, 2);
        add_filter('woocommerce_product_get_price', 'tkApplyCustomDiscount', 11, 2);
        add_filter('woocommerce_product_variation_get_sale_price', 'tkApplyCustomDiscount', 11, 2);
        add_filter('woocommerce_product_variation_get_price', 'tkApplyCustomDiscount', 11, 2);
    }
});
