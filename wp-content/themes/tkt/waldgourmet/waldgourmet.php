<?php

require_once ("image-sizes.php");
require_once ("ajax/product-source-ajax.php");
require_once ("ajax/archive-filter.php");
require_once ("ajax/add-to-cart.php");
require_once ("ajax/cart-count.php");
require_once ("ajax/bulk-order.php");
require_once ("custom-discounts/_custom-discounts.php");
require_once ("shortcodes/_shortcodes.php");
require_once ("frischezuschlag.php");
require_once ("breadcrumbs.php");
require_once ("filter.php");
require_once ("twig/recipe.php");
require_once ("twig/filters.php");
require_once ("elementor/responsive-background.php");
require_once ("elementor/_extension.php");
require_once ("description-titles.php");
require_once ("hooks/_hooks.php");
require_once ("util/_util.php");
require_once ("subscriptions/_subscriptions.php");

tkRegisterQueryBinding("product", "pa_wildart", "wildart");
tkRegisterQueryBinding("product", "pa_saison", "saison");
tkRegisterQueryBinding("product", "pa_anlass", "anlass");

tkRegisterQueryBinding("tk-recipe", "tk-recipe-category", "kategorie");
tkRegisterQueryBinding("tk-recipe", "tk-recipe-seasons", "saison");
tkRegisterQueryBinding("tk-recipe", "tk-recipe-occassion", "anlass");

register_nav_menu("main-menu", "Hauptmenü");
register_nav_menu("tk-second-footer", "Zweiter footer");

add_filter("woocommerce_gzd_shipping_costs_text", function ($value) {
    return "zzgl. Versandkosten <i class=\"far fa-question-circle\"></i>";
});
