<?php
global $recipe;
?>

<li <?php post_class(array("tk-recipe"), $recipe); ?>>
    <div class="scale-with-grid product-loop-thumb">
        <div class="image_wrapper">
            <a href="<?php
            echo apply_filters('the_permalink', get_permalink($recipe)) ?>">
                <?php
                echo get_the_post_thumbnail($recipe, 'tk-shop-archive', array('class' => 'scale-with-grid'));
                ?>
            </a>
        </div>
    </div>

    <div class="desc">
        <div class="tk-header">
            <div class="tk-title">
                <h3>
                    <a href="<?php the_permalink($recipe); ?>"><?php echo get_post_meta($recipe->ID, "tk-display-title", true); ?></a>
                </h3>
                <?php
                $subtitle = get_post_meta($recipe->ID, "tk-subtitle", true);
                if ($subtitle) {
                    echo "<div class=\"tk-subtitle\">$subtitle</div>";
                }
                ?>
            </div>
        </div>
        <div class="tk-footer">
            <?php
            global $tkTwig;
            echo $tkTwig->renderTemplate("recipe-metas.twig", array("recipe" => $recipe))
            ?>
        </div>
    </div>
</li>
