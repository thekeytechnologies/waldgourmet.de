<?php

function tk_get_product_atc_icon($product) {
    $buttonText = $product->add_to_cart_text();

    switch ($buttonText) {
        case "Zum Produkt":
            $icon = get_option('tk-wg-settings_tk-atc-icon-sign-up')[0];
            break;
        case "Ausführung wählen":
            $icon = get_option('tk-wg-settings_tk-atc-icon-goto-page')[0];
            break;
        case "In den Warenkorb":
            $icon = get_option('tk-wg-settings_tk-atc-icon-add-to-cart')[0];
            break;
        default:
            $icon = get_option('tk-wg-settings_tk-atc-icon-add-to-cart')[0];
            break;
    }

    return $icon;
}