<?php


require_once('tk_product_util.php');
require_once ("tk-multisite-util.php");


/**
 * @param $item Object | Integer - Te
 * @param string $type "post" | "user" - terms would require to enter a taxonomy, not supported for now, but a term object will still be correctly returned
 * @return string
 */
function tkWpObject($item, $type = 'post') {

    $id = false;
    if (is_numeric($item)) {

        $id = $item;

    } elseif (is_array($item)) {
        if (isset($item["ID"])) {
            $id = $item['ID'];
        }
    }

    if ($id) {
        if ($type == 'post') {
            get_post('id');
        } elseif ($type == 'user') {
            get_user_by('id', $id);
        }
    }

    if (empty($item)) {
        return "";
    } else if ($item instanceof WP_Term || $item instanceof WP_Post || $item instanceof WP_User) {
        return $item;
    }

    return "";
}
