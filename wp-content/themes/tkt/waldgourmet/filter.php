<?php

class TkAttributeBinding
{
    private $taxonomy;
    private $queryParam;

    /**
     * @param string $taxonomy
     * @param string $queryParam
     */
    public function __construct(string $taxonomy, string $queryParam)
    {
        $this->taxonomy = $taxonomy;
        $this->queryParam = $queryParam;
    }

    /**
     * @return string
     */
    public function getTaxonomy()
    {
        return $this->taxonomy;
    }

    /**
     * @return string
     */
    public function getQueryParam()
    {
        return $this->queryParam;
    }
}

class TkActiveBinding
{
    private $binding;
    private $value;

    public function __construct(TkAttributeBinding $binding, string $value)
    {
        $this->binding = $binding;
        $this->value = $value;
    }

    /**
     * @return TkAttributeBinding
     */
    public function getBinding(): TkAttributeBinding
    {
        return $this->binding;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

}

/**
 * @var $tkTaxQueryBindings [string]AttributeBinding
 */
global $tkTaxQueryBindings;
$tkTaxQueryBindings = array();


/**
 * @var $tkActiveQueryBindings TkActiveBinding[]
 */
global $tkActiveQueryBindings;
$tkActiveQueryBindings = array();

function tkRegisterQueryBinding(string $postType, string $taxonomy, string $queryParam)
{
    global $tkTaxQueryBindings;
    $postTypeBindings = isset($tkTaxQueryBindings[$postType]) ? $tkTaxQueryBindings[$postType] : array();
    $postTypeBindings[] = new TkAttributeBinding($taxonomy, $queryParam);
    $tkTaxQueryBindings[$postType] = $postTypeBindings;
}


function tkGetActiveFilters()
{
    global $tkActiveQueryBindings;

    $filters = array();
    foreach ($tkActiveQueryBindings as $activeBinding) {
        $taxonomy = $activeBinding->getBinding()->getTaxonomy();
        $term = get_term_by("slug", $activeBinding->getValue(), $taxonomy);
        $filterValue = array("slug" => $activeBinding->getValue(), "name" => $term->name);
        if (isset($filters[$taxonomy])) {
            $filters[$taxonomy][] = $filterValue;
        } else {
            $filters[$taxonomy] = array($filterValue);
        }
    }
    return $filters;
}


function tk_add_filters_to_query(WP_Query $query) {
    if ($query->is_main_query() and empty($query->get("page"))) {
        global $tkTaxQueryBindings;
        global $tkActiveQueryBindings;

        $bindings = isset($tkTaxQueryBindings[$query->get("post_type")]) ? $tkTaxQueryBindings[$query->get("post_type")] : array();

        $taxquery = array(
            "relation" => "AND"
        );

        $bindingsFound = false;
        foreach ($bindings as $taxQueryBinding) {
            if (isset($_GET[$taxQueryBinding->getQueryParam()])) {
                $bindingsFound = true;
                $values = $_GET[$taxQueryBinding->getQueryParam()];
                $values = array_map("trim", explode(",", $values));

                foreach ($values as $value) {
                    $tkActiveQueryBindings[] = new TkActiveBinding($taxQueryBinding, $value);
                }

                $taxquery[] = array(
                    'taxonomy' => $taxQueryBinding->getTaxonomy(),
                    'field' => 'slug',
                    'terms' => $values,
                    'operator' => 'IN'
                );
            }
        }
        if ($bindingsFound) {
            $query->set('tax_query', $taxquery);
        }

        /* Add tk-in-stock filter */
        if (isset($_POST['inStock']) && $_POST['inStock'] == 1) {
            $metaquery = array(
                array(
                    'key' => '_stock_status',
                    'value' => 'instock'
                ),
                array(
                    'key' => '_backorders',
                    'value' => 'no'
                ),
            );
            $query->set('meta_query', $metaquery);
        }
    }
}


add_action("pre_get_posts", "tk_add_filters_to_query", 10, 1);