<?php

class TkBlock
{
    public $header;
    public $lines = array();
}

class CombinedBlock
{
    public $header;
    public $ingredientLines = array();
    public $stepLines = array();
}

function tkParseBlock($rawText): array
{
    $rawBlocks = preg_split("#\n\s*\n#Uis", $rawText);

    $blocks = array();
    foreach ($rawBlocks as $block) {
        $rawLines = explode("\n", $block);
        $block = new TkBlock();

        foreach ($rawLines as $rawLine) {
            if (startsWith($rawLine, "--")) {
                $block->header = trim(str_replace("--", "", $rawLine));
            } else if (strlen($rawLine) > 0) {
                $block->lines[] = trim($rawLine);
            }
        }
        $blocks[] = $block;
    }
    return $blocks;
}

function tkRecipeBlocks($recipe)
{
    $ingredientBlocks = tkParseBlock(tkWpMeta($recipe, "tk-ingredients"));
    //$stepBlocks = tkParseRecipeSteps(tkWpMeta($recipe, "tk-steps"));

    $blocks = [];
    foreach ($ingredientBlocks as $ingredientBlock) {
        /*$correspondingStepBlock = false;
        foreach ($stepBlocks as $stepBlock) {
            if ($stepBlock->header == $ingredientBlock->header) {
                $correspondingStepBlock = $stepBlock;
                break;
            }
        }*/
        //if ($correspondingStepBlock) {
            $combinedBlock = new CombinedBlock();
            $combinedBlock->header = $ingredientBlock->header;
            $combinedBlock->ingredientLines = $ingredientBlock->lines;
            //$combinedBlock->stepLines = $correspondingStepBlock->lines;
            $blocks[] = $combinedBlock;
        //}
    }
    return $blocks;
}

function tkRecipeStructuredData(WP_Post $recipe)
{
    $structuredData = array(
        "@context" => "http://schema.org/",
        "@type" => "Recipe",
        "name" => tkWpTitle($recipe),
        "author" => array(
            "@type" => "Organization",
            "name" => "Waldgourmet Digital GmbH"
        ),
        "datePublished" => date("Y-m-d", strtotime($recipe->post_date)),
        "description" => strip_tags($recipe->post_excerpt),
        "prepTime" => "PT" . tkWpMeta($recipe, "tk-cooking-time") . "M0S",
        "totalTime" => "PT" . tkWpMeta($recipe, "tk-overall-time") . "M0S",
        "recipeYield" => tkWpMeta($recipe, "tk-feeds") . " Portionen",
        "recipeCategory" => tkWpMeta($recipe, "tk-recipe-type")
    );

    if (has_post_thumbnail($recipe)) {
        $structuredData["image"] = get_the_post_thumbnail_url($recipe, "full");
    }

    $nutrition = tkWpMeta($recipe, "tk-nutrition");
    $structuredData["nutrition"] = array(
        "@type" => "NutritionInformation",
        "calories" => $nutrition . " kcal"
    );

    $ingredientBlocks = tkParseBlock(tkWpMeta($recipe, "tk-ingredients"));
    $ingredients = array();
    foreach ($ingredientBlocks as $stepBlock) {
        foreach ($stepBlock->lines as $line) {
            $line = str_replace("|", " ", $line);
            $ingredients[] = trim(str_replace("-", "", strip_tags($line)));
        }
    }
    $structuredData["recipeIngredient"] = $ingredients;


    $stepBlocks = tkParseBlock(tkWpMeta($recipe, "tk-steps"));
    $steps = array();
    foreach ($stepBlocks as $stepBlock) {
        foreach ($stepBlock->lines as $line) {
            $steps[] = trim(str_replace("-", "", strip_tags($line)));
        }
    }
    $structuredData["recipeInstructions"] = $steps;

    return "<script type=\"application/ld+json\">" . json_encode($structuredData) . "</script>";
}


global $tkTwig;
if ($tkTwig) {
    $tkTwig->addFilter("wgRecipeBlocks", "tkRecipeBlocks");
    $tkTwig->addFilter("wgParseBlock", "tkParseBlock");
    $tkTwig->addFilter("wgRecipeStructuredData", "tkRecipeStructuredData");

}
