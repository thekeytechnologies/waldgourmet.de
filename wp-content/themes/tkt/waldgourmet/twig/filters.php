<?php

global $tkTwig;

if ($tkTwig) {
    $tkTwig->addFunction("woocommerce_catalog_ordering", function () {
        woocommerce_catalog_ordering();
    });

    $tkTwig->addFunction("get_tax_query_bindings", function ($postType) {
        global $tkTaxQueryBindings;
        return $tkTaxQueryBindings[$postType];
    });

    $tkTwig->addFunction("get_filter_slugs", function ($filters, $taxonomyName) {
        $activeFilters = isset($filters[$taxonomyName]) ? $filters[$taxonomyName] : array();
        return array_map(function ($filter) {
            return $filter["slug"];
        }, $activeFilters);
    });


    $tkTwig->addFilter("tkCategoryIcon", function ($product) {
        $product = tkWpObject($product);

        $terms = get_the_terms($product->get_id(), "product_cat");
        $mainCategory = $terms[0];
        if ($mainCategory) {
            $icon = get_term_meta($mainCategory->term_id, "tk-category-icon", true);
            if ($icon) {
                return $icon;
            }
        }
    });

    $tkTwig->addFilter('tkWpPostType', function($post) {
        tkWpType($post);
    });
}
