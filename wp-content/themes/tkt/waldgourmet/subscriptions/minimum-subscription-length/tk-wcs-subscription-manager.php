<?php


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class TK_WCS_Subscription_Status_Manager {
    public $user;
    public $subscription;
    public $initSuccessful = false;
    public $userSubscriptionMeta = array();
    const TK_SUBSCRIPTION_META_NAME = 'tk_wcs_subscription_meta';

    public function __construct($user, $subscription) {

        if (!$user instanceof WP_User) {
            if (is_numeric($user)) {
                $user = get_user_by('id', $user);
            } elseif(is_array($user)) {
                if (isset($user['ID'])) {
                    $user = get_user_by('id', $user['ID']);
                }
            }
        }

        if (!$subscription instanceof WC_Subscription) {
            if (is_numeric($user)) {
                $subscription = wcs_get_subscription($subscription);
            }
        }

        if ($user instanceof WP_User && $subscription instanceof WC_Subscription) {
            $this->user = $user;
            $this->subscription = $subscription;
            $this->userSubscriptionMeta = tkWpMeta($this->user,self::TK_SUBSCRIPTION_META_NAME, false);
            $this->initSuccessful = true;
        }
    }

    public function getEndDate() {
        if (!$this->initSuccessful) {
            return;
        }

        $meta = $this->getMeta();
        if (isset($meta['enddate'])) {
            return $meta['enddate'];
        } else {
            $period = $this->getPeriod();
            return strtotime($meta['startdate'] . ' + ' . $period . ' days');
        }
    }

    public function setEndDate($newEndDate) {
        if (!$this->initSuccessful) {
            return;
        }

        $meta = $this->getMeta();
        $meta['endDate'] = $newEndDate;
        $this->setMeta($meta);
    }

    public function cancelAtNextEndDate() {
        if (!$this->initSuccessful) {
            return;
        }

        $this->getMeta();
        $meta['toCancel'] = true;
        $this->setMeta($meta);
    }

    public function getStartDate() {
        $subscription = $this->subscription;
        return $subscription->get_date_created()->date('m.d.y H:i:s');
    }

    public function updateStatus() {
        if (!$this->initSuccessful) {
            return;
        }

        $now = time();
        $endDate = $this->getEndDate();

        if ($endDate < $now) {
            $meta = $this->getMeta();
            if (isset($meta['toCancel']) && $meta['toCancel']) {

            } else {
                $this->setEndDate($this->getExtendedEndDate());
            }
        }
    }

    private function getExtendedEndDate() {
        $endDate = $this->getEndDate();
        $period = $this->getPeriod();
        return strtotime($endDate . ' + ' . $period . ' days');
    }

    public function getPeriod() {
        if (!$this->initSuccessful) {
            return;
        }
        return strtotime('3 Months');
    }

    private function getMeta() {
        if (key_exists($this->subscription->get_id(), $this->userSubscriptionMeta)) {
            return $this->userSubscriptionMeta[$this->subscription->get_id()];
        }
    }

    private function setMeta($meta) {
        $this->userSubscriptionMeta[$this->subscription->get_id()] = $meta;
        update_user_meta($this->user->ID, self::TK_SUBSCRIPTION_META_NAME, $this->userSubscriptionMeta);
    }

    public static function updateSubscriptionStatus($users) {

        foreach ($users as $user) {
            $subscriptions = wcs_get_users_subscriptions($user->ID);
            foreach ($subscriptions as $subscription) {
                $manager = new TK_WCS_Subscription_Status_Manager($user, $subscription);
                $manager->updateStatus();
            }
        }
    }

}