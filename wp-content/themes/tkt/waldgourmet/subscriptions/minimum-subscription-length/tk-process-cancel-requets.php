<?php

function tkCancelSubscription($userId, $subscription) {
    $user = get_user_by('id', $userId);
    $earliestCancelDate = tkGetEarliestCancelDate($subscription);

    $today = date('Y-m-d');
    if ($today > $earliestCancelDate) {
        WCS_User_Change_Status_Handler::change_users_subscription($subscription,'cancelled');
    } else {
        tkSetCancelDate($subscription, $earliestCancelDate);
        tkSendCancelConfirmationMail($user, $subscription);
    }
}

function tkGetEarliestCancelDate($subscription) {

    $startDate = $subscription->get_date('date_created');

    $minimumDuration = get_option('tk-wg-settings_tk-minimum-subscription-duration');
    $earliestEndDate = strtotime($startDate . ' + ' . $minimumDuration . ' Months');

    return date('Y-m-d', $earliestEndDate);
}

function tkSetCancelDate($subscription, $earliestCancelDate) {
    update_post_meta($subscription->get_id(), 'tk-cancel-date', $earliestCancelDate);
}

function tkSendCancelConfirmationMail($user, $subscription) {

    set_query_var('tk-cancel-confirmation-mail-subscription', $subscription);
    $to = $user->user_email;
    $subject = do_shortcode(get_option('tk-wg-settings_tk-subscription-cancel-subject'));
    $msg = do_shortcode(get_option('tk-wg-settings_tk-subscription-cancel-message'));
    tkSendMail($to, $subject, $msg, 'kontakt@waldgourmet.de');

    $infoTo = 'shop@waldgourmet.de';
    $infoSubject = "Abonement gekündigt ($user->firstname $user->lastname)";
    $infoMsg = do_shortcode(get_option('tk-wg-settings_tk-subscription-cancel-info-message'));
    tkSendMail($infoTo, $infoSubject, $infoMsg, $user->user_email);
}

function tkSubscriptionToBeCancelled($subscription) {
    $cancelDate = tkWpMeta($subscription->get_id(), 'tk-cancel-date');
    if ($cancelDate) {
        return true;
    }
    return false;
}

function tkHandleSubscriptionCancelRequest() {
    if ( isset( $_GET['tk_cancel_subscription'] ) && isset( $_GET['subscription_id'] ) && isset( $_GET['_wpnonce'] )  ) {

        $userId      = get_current_user_id();
        $subscription = wcs_get_subscription( $_GET['subscription_id'] );
        $new_status   = 'cancelled';

        /* Check if the user can cancel their subscription */
        if ( WCS_User_Change_Status_Handler::validate_request( $userId, $subscription, $new_status, $_GET['_wpnonce'] ) ) {
            tkCancelSubscription($userId, $subscription);
        }
    } elseif (isset( $_GET['tk_uncancel_subscription'] ) && isset( $_GET['subscription_id'] ) && isset( $_GET['_wpnonce'] )  ) {
        $userId      = get_current_user_id();
        $subscription = wcs_get_subscription( $_GET['subscription_id'] );
        delete_post_meta($subscription->get_id(), 'tk-cancel-date');
    }
}

add_action('wp_loaded', 'tkHandleSubscriptionCancelRequest');