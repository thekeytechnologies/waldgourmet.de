<?php

function tkChangeUserActionsForSubscriptions($actions, $subscription) {

    if (isset($actions['cancel'])) {

        if (tkSubscriptionToBeCancelled($subscription)) {
            $actions['cancel']['url'] = str_replace('change_subscription_to=cancelled', 'tk_uncancel_subscription=1', $actions['cancel']['url']);
            $actions['cancel']['name'] = 'Kündigung widerrufen';
        } else {
            $actions['cancel']['url'] = str_replace('change_subscription_to=cancelled', 'tk_cancel_subscription=1', $actions['cancel']['url']);
        }

    }

    return $actions;
}

add_filter('tk_wcs_user_actions_for_subscriptions', 'tkChangeUserActionsForSubscriptions', 10, 2);