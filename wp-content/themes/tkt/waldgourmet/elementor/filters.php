<?php

class TkFiltersWidget extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-filters';
    }

    public function get_title()
    {
        return __('(TK) Filters', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        global $tkTwig;
        global $wp_query;

        if (!is_shop()) {
            $taxonomy = get_queried_object()->taxonomy;
            $term = get_queried_object()->name;
        } else {
            $taxonomy = null;
            $term = null;
        }

        if (is_shop()) {
            $count_posts = wp_count_posts('product');
            $count = $count_posts->publish;
        } else {
            $count = $wp_query->found_posts;
        }

        if (get_post_type() == "tk-recipe") {
            $name = "Rezept";
            $postType = "tk-recipe";
        } else if (get_post_type("product") or is_woocommerce()) {
            $name = "Produkt";
            $postType = "product";
        } else {
            $name = "UNBEKANNT";
            $postType = "tk-horst";
        }

        if ($_POST['inStock'] == 1) {
            $inStock = true;
        } else {
            $inStock = false;
        }


        if ($count > 0)  {
            echo $tkTwig->renderTemplate("filters.twig", array(
                "name" => $name,
                "postType" => $postType,
                "filters" => tkGetActiveFilters(),
                "resultCount" => $count,
                "taxonomy" => $taxonomy,
                "term" => $term,
                "isShop" => is_shop(),
                "inStock" => $inStock
            ));
        }
    }
}
