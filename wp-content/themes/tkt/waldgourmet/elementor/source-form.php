<?php

class TkSourceForm extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-source-form';
    }

    public function get_title()
    {
        return __('(TK) Source Form', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        global $tkTwig;
        echo $tkTwig->renderTemplate("product-source-form.twig");
    }
}
