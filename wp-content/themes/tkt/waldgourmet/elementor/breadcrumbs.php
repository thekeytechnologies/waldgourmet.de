<?php


class TkBreadcrumbsWidget extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-breadcrumbs';
    }

    public function get_title()
    {
        return __('(TK) Breadcrumbs', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        echo wgBreadcrumbs(get_queried_object());
    }
}
