<?php

class TkSourceMap extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-source-map';
    }

    public function get_title()
    {
        return __('(TK) Source Map', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        echo "<script async defer
  src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyD0B60JX-8itWIQdw8F8bjWi8Zr27nY62w&callback=tkInitMap\">
</script>";
        echo "<div id='tk-source-map'></div>";
    }
}
