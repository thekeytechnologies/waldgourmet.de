<?php

class TkPostBoxWidget extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-post-box-widget';
    }

    public function get_title()
    {
        return __('(TK) Post Box', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Content', 'tkt'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'widget_title',
            [
                'label' => __('Widget Title', 'tkt'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',
            ]
        );

        $this->add_control(
            'post_id',
            [
                'label' => __('Post Id', 'tkt'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'input_type' => 'number',
            ]
        );

        $this->add_control(
            'link_text',
            [
                'label' => __('Link Text', 'tkt'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        global $tkTwig;

        $settings = $this->get_settings_for_display();

        echo $tkTwig->renderTemplate("tk-post-box-widget.twig", array(
            "widgetTitle" => $settings["widget_title"],
            "linkAtBottom" => true,
            "normalTitle" => true,
            "imageSize" => "tk-post-box-large",
            "linkTitle" => $settings["link_text"],
            "post" => get_post($settings["post_id"])));
    }
}
