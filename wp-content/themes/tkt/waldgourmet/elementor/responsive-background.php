<?php

add_action('elementor/element/before_section_start', function ($element, $section_id, $args) {
    /** @var \Elementor\Element_Base $element */
    if ('section' === $element->get_name() && 'section_background' === $section_id) {

        $element->start_controls_section(
            'custom_section',
            [
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                'label' => __('Responsive Background', 'elementor'),
            ]
        );

        $element->add_responsive_control(
            'responsive_background_control',
            [
                'label' => __('Display', 'elementor'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => '',
                'options' => [
                    '' => _x('Default', 'Background Control', 'elementor'),
                    'none' => _x('None', 'Background Control', 'elementor'),
                ],
                'selectors' => [
                    '{{WRAPPER}}' => 'background-image: {{VALUE}};',
                ],

            ]
        );

        $element->end_controls_section();
    }
}, 10, 3);