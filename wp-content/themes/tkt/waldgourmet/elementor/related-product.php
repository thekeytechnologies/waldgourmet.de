<?php

class TkRelatedProduct extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-related-product';
    }

    public function get_title()
    {
        return __('(TK) Related Product', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['woocommerce-elements'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        global $tkTwig;

        $recipe = get_post_meta(get_queried_object_id(), "tk-product-ref", true);
        if ($recipe) {
            echo $tkTwig->renderTemplate("tk-post-box-widget.twig", array(
                "widgetTitle" => "Ein alternatives Rezept",
                "linkAtBottom" => true,
                "normalTitle" => true,
                "imageSize" => "tk-post-box-large",
                "linkTitle" => get_post_meta(get_queried_object_id(), "tk-display-title", true),
                "post" => $recipe));
        }
    }
}
