<?php

class TkRecipeSteps extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-recipe-steps';
    }

    public function get_title()
    {
        return __('(TK) Recipe Steps', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        tkRenderRecipeSteps();
    }
}

function tkRenderRecipeSteps() {
    global $tkTwig;

    $recipe = get_queried_object();
    if ($recipe) {
        echo $tkTwig->renderTemplate("recipe-steps.twig", array(
            "recipe" => $recipe));
    }
}
add_shortcode('tk-recipe-steps', 'tkRenderRecipeSteps');
