<?php

require_once("bulk-order-form/bulk-order-form-util.php");

function tk_elementor_widgets()
{
    require_once "review-widget.php";
    $widgetManager = \Elementor\Plugin::instance()->widgets_manager;
    $widgetManager->register_widget_type(new TkReviewWidget());

    require_once "breadcrumbs.php";
    $widgetManager->register_widget_type(new TkBreadcrumbsWidget());

    require_once "add-to-cart/add-to-cart-widget.php";
    $widgetManager->register_widget_type(new TkAddToCartWidget());

    require_once "filters.php";
    $widgetManager->register_widget_type(new TkFiltersWidget());

    require_once "related-recipe.php";
    $widgetManager->register_widget_type(new TkRelatedRecipeWidget());

    require_once "cross-sells.php";
    $widgetManager->register_widget_type(new TkCrosssellsWidget());

    require_once "post-box-widget.php";
    $widgetManager->register_widget_type(new TkPostBoxWidget());

    require_once "recipe-archive.php";
    $widgetManager->register_widget_type(new TkRecipeArchiveWidget());

    require_once "recipe-metas.php";
    $widgetManager->register_widget_type(new TkRecipeMetas());

    require_once "print-button.php";
    $widgetManager->register_widget_type(new TkPrintButton());

    require_once "product-for-recipe-button.php";
    $widgetManager->register_widget_type(new TkProductForRecipeButton());

    require_once "recipe-content.php";
    $widgetManager->register_widget_type(new TkRecipeContent());

    require_once "recipe-ingredients.php";
    $widgetManager->register_widget_type(new TkRecipeIngredients());

    require_once "recipe-steps.php";
    $widgetManager->register_widget_type(new TkRecipeSteps());

    require_once "related-product.php";
    $widgetManager->register_widget_type(new TkRelatedProduct());

    require_once "source-form.php";
    $widgetManager->register_widget_type(new TkSourceForm());

    require_once "source-map.php";
    $widgetManager->register_widget_type(new TkSourceMap());

    require_once "cart-widget.php";
    $widgetManager->register_widget_type(new TkCartWidget());

    require_once "trusted-shops-reviews.php";
    $widgetManager->register_widget_type(new TkTrustedShopsReviews());

    require_once "sidebar-boxes/_sidebar-boxes.php";
    $widgetManager->register_widget_type(new TKSingleProductsSidebarBoxes());
    $widgetManager->register_widget_type(new TKSingleRecipeSidebarBoxes());

    require_once "bulk-order-form/bulk-order-form.php";
    $widgetManager->register_widget_type(new TkBulkOrderForm());

    require_once "menu-box.php";
    $widgetManager->register_widget_type(new tkMenuBox());

    wp_register_script("tk-mega-menu", get_theme_root_uri() . '/tkt/waldgourmet/elementor/menu/menu.js', ["jquery"]);
}

add_action('elementor/widgets/widgets_registered', 'tk_elementor_widgets');


function tk_elementor_widget_category($elements_manager)
{

    $elements_manager->add_category(
        'first-category',
        [
            'title' => __('TKT Custon Widgets', 'tkt'),
            'icon' => 'fas fa-star',
        ]
    );

}

add_action('elementor/elements/categories_registered', 'tk_elementor_widget_category');
