<?php

class TkRecipeArchiveWidget extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-recipe-archive';
    }

    public function get_title()
    {
        return __('(TK) Recipe Archive', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        global $tkTwig;
        $recipes = array();
        $structuredData = array(
            "@context" => "http://schema.org",
            "@type" => "ItemList",
            "itemListElement" => array()
        );
        ?>
        <div class="tk-recipes-wrapper tk-ajax-overlay-target">
            <ul class="tk-recipes tk-filter-target">
                <?php
                $position = 1;
                while (have_posts()) {
                    the_post();

                    $structuredData["itemListElement"][] = array(
                        "@type" => "ListItem",
                        "position" => $position,
                        "url" => get_the_permalink()
                    );

                    $recipe = get_post(get_the_ID());
                    $recipes[] = $recipe;
                    echo $tkTwig->renderTemplate("recipe-archive-item.twig", array("recipe" => $recipe));
                    $position++;
                }
                ?>
            </ul>
        </div>
        <?php

        echo "<script type=\"application/ld+json\">" . json_encode($structuredData) . "</script>";

    }
}
