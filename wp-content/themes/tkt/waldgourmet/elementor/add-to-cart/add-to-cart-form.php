<?php

$product = WooCommerce::instance()->product_factory->get_product(get_queried_object_id());

if (!$product) {
    return;
}

if ($product instanceof WC_Product_Variable) {
    $variableProduct = true;
    $variations = $product->get_available_variations();
    $variations = array_filter($variations, function ($v) {
        return $v["is_in_stock"];
    });
    $productsAvailable = !empty($variations);
    $attributes = $product->get_attributes();
    $isSubscription = $product instanceof WC_Product_Variable_Subscription;
} else {
    $variableProduct = false;
    $productsAvailable = $product->is_in_stock();
}

$singleVariation = false;
if ($isSubscription) {
    $singleVariationProduct = tkWpMeta($product->get_id(), 'tk-wc-single-varation');
    if ($singleVariationProduct) {
        $singleVariationProduct = wc_get_product($singleVariationProduct['ID']);
        if ($singleVariationProduct instanceof WC_Product) {
            $singleVariation = $singleVariationProduct;
        }
    }
}


?>

<?php if ($productsAvailable): ?>
    <form class="tk-add-to-cart tk-ajax-overlay-target"
          action="<?php echo esc_url(apply_filters('woocommerce_add_to_cart_form_action', $product->get_permalink())); ?>"
          method="post" enctype='multipart/form-data'
          data-product_id="<?php echo absint($product->get_id()); ?>"
          data-simple_product_max_quantity="<?php echo $product->get_max_purchase_quantity() ?>"
          data-simple_product_price="<?php echo $product->get_price() ?>"
          data-product_variations="<?php echo htmlspecialchars(wp_json_encode($variations)); // WPCS: XSS ok. ?>">
        <div class="tk-product-configuration">
            <?php if ($variableProduct): ?>
                <?php foreach ($attributes as $attributeKey => $options) :
                    $data = $options->get_data();
                    if (!$data["variation"]) {
                        continue;
                    }

                    $name = $data["name"];
                    ?>
                    <div class="label">
                        <label
                                for="<?php echo esc_attr(sanitize_title($name)); ?>"><?php echo wc_attribute_label($name); ?></label>
                    </div>
                    <div class="value">
                        <?php
                        wc_dropdown_variation_attribute_options(array(
                            'options' => $data["options"],
                            'attribute' => $name,
                            'product' => $product,
                        ));
                        ?>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <div class="label">
                <label>Verfügbarkeit</label>
            </div>
            <div class="value tk-availability">
                <?php
                if ($variableProduct) {
                    echo "Bitte erst Variante auswählen";
                } else {
                    echo "Lieferbar innerhalb von 3-8 Werktagen. Warum so lange? <i class=\"far fa-question-circle\"></i>";
                }
                ?>
            </div>

            <?php
            $minQty = (int)get_post_meta($product->get_id(), "min_quantity", true);
            $maxQty = (int)get_post_meta($product->get_id(), "max_quantity", true);
            if (!$minQty) {
                $minQty = 0;
            }
            if (!$maxQty) {
                $maxQty = $product->get_stock_quantity();
            }

            $singleSell = $maxQty <= 1;
            ?>

            <?php if (!$isSubscription): ?>
                <?php if (!$singleSell): ?>
                    <div class="label">
                        <label for="quantity">Anzahl</label>
                    </div>
                <?php endif; ?>
                <div class="value">
                    <?php
                    do_action('woocommerce_before_add_to_cart_quantity');

                    woocommerce_quantity_input(array(
                        'min_value' => $minQty,
                        'max_value' => $maxQty,
                        'input_value' => isset($_POST['quantity']) ? wc_stock_amount(wp_unslash($_POST['quantity'])) : $minQty, // WPCS: CSRF ok, input var ok.
                    ));

                    do_action('woocommerce_after_add_to_cart_quantity');
                    ?>
                </div>
            <?php else: ?>

                <input type="hidden" name="quantity" value="1">


            <?php endif ?>
        </div>
        <div class="tk-add-to-cart-footer">
            <div class="tk-price-wrapper">
                <?php
                woocommerce_template_single_price();
                echo "<div class='tk-additional-price-infos'>";
                woocommerce_gzd_template_single_price_unit();
                woocommerce_gzd_template_single_legal_info();
                echo "</div>";
                ?>
            </div>
            <div class="tk-add-to-cart-wrapper">
                <div class="woocommerce-variation-add-to-cart variations_button">
                    <?php do_action('woocommerce_before_add_to_cart_button'); ?>

                    <?php if ($variableProduct): ?>
                        <button type="submit"
                                class="tk-button tk-button-primary"
                                value="<?php echo esc_attr($product->get_id()); ?>"
                                disabled><?php echo "Bitte Variante wählen" ?></button>
                    <?php else: ?>
                        <button type="submit"
                                name="add-to-cart"
                                value="<?php echo esc_attr($product->get_id()); ?>"
                                class="tk-button tk-button-primary"><?php echo esc_html($product->single_add_to_cart_text()); ?></button>
                    <?php endif; ?>
                    <?php do_action('woocommerce_after_add_to_cart_button'); ?>

                    <?php if ($variableProduct): ?>
                        <input type="hidden" name="add-to-cart"
                               value="<?php echo absint($product->get_id()); ?>"/>
                        <input type="hidden" name="product_id"
                               value="<?php echo absint($product->get_id()); ?>"/>
                        <input type="hidden" name="variation_id" class="variation_id"
                               value="0"/>
                    <?php endif; ?>
                </div>
                <?php if ($singleVariation): ?>
                    <input type="hidden" name="tk-single-variation" value="<?php echo $singleVariation->get_id() ?>">
                    <a class="tk-add-single-variation-link">
                        Einzeln bestellen
                    </a>
                <?php endif ?>
                <a href="/warenkorb/" style="display: none" class="added_to_cart" title="Warenkorb anzeigen">Warenkorb
                    anzeigen</a>
            </div>
        </div>
    </form>

<?php else: ?>
    <?php
    $alternativeProducts = get_post_meta($product->get_id(), "tk-alternative-products");
    $productForAlternative = null;

    $alternatives = [];
    foreach ($alternativeProducts as $alternativeProduct) {
        $alternativeProduct = wc_get_product($alternativeProduct["ID"]);
        if ($alternativeProduct) {
            if ($alternativeProduct->is_in_stock()) {
                $alternatives[] = $alternativeProduct;
            }
        }
    }

    if (!$productForAlternative) {
        $naturbox = wc_get_product(9495);
        if ($naturbox->is_in_stock()) {
            $alternatives[] = $naturbox;
        }
    }

    ?>
    <div class="tk-product-configuration">
        <div class="label">
            <label>Verfügbarkeit</label>
        </div>
        <div class="value tk-availability">
            <?php
            echo sizeof($alternatives) ? "Das Produkt ist leider zur Zeit nicht verfügbar. Schauen Sie doch mal in die Alternativprodukte." : "Das Produkt ist leider zur Zeit nicht verfügbar."
            ?>
        </div>
    </div>

    <?php if (sizeof($alternatives)): ?>
        <h3 class="tk-waiting-list-heading">
            <?php echo sizeof($alternatives) === 1 ? "Alternativprodukt" : "Alternativprodukte" ?>
        </h3>
        <div class="tk-alternative-products">
            <?php
            global $product;
            $oldProduct = $product;

            foreach ($alternatives as $alternative): ?>
                <?php
                $product = $alternative;
                ?>
                <a href="<?php echo get_permalink($alternative->get_id()) ?>">
                    <div class="tk-product">
                        <div class="tk-thumbnail">
                            <?php echo get_the_post_thumbnail($alternative->get_id(), "tk-shop-thumbnail") ?>
                        </div>
                        <div class="tk-product-data">
                            <h4><?php echo $alternative->get_title() ?></h4>
                            <?php wc_get_template_part("single-product/price"); ?>
                        </div>
                    </div>
                </a>

            <?php endforeach;
            $product = $oldProduct;
            ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
