<?php


class TkAddToCartWidget extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-add-to-cart';
    }

    public function get_title()
    {
        return __('(TK) Add to cart', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['woocommerce-elements'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        require_once("add-to-cart-form.php");
    }
}
