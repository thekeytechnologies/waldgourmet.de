<?php


class TkReviewWidget extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-review';
    }

    public function get_title()
    {
        return __('(TK) Review', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Content', 'tkt'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'text',
            [
                'label' => __('Text', 'tkt'),
                'type' => \Elementor\Controls_Manager::TEXTAREA,
                'input_type' => 'text',
                'placeholder' => __('..ich war super zufrieden...', 'tkt'),
            ]
        );

        $this->add_control(
            'author',
            [
                'label' => __('Author', 'tkt'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',
                'placeholder' => __('Max Mustermann', 'tkt'),
            ]
        );

        $this->add_control(
            'date',
            [
                'label' => __('Date', 'tkt'),
                'type' => \Elementor\Controls_Manager::DATE_TIME,
                'input_type' => 'text',
                'placeholder' => __('10.10.2018', 'tkt'),
            ]
        );

        $this->add_control(
            'stars',
            [
                'label' => __('Stars', 'tkt'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'input_type' => 'number',
                'placeholder' => __('1-5', 'tkt'),
            ]
        );

        $this->add_control(
            'url',
            [
                'label' => __('URL', 'tkt'),
                'type' => \Elementor\Controls_Manager::URL,
                'input_type' => 'url',
                'placeholder' => __('...trustedshops.com...', 'tkt'),
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        global $tkTwig;

        echo $tkTwig->renderTemplate("review.twig", $settings);
    }
}
