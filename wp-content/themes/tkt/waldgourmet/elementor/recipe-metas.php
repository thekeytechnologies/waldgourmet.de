<?php

class TkRecipeMetas extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-recipe-metas';
    }

    public function get_title()
    {
        return __('(TK) Recipe Metas', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        global $tkTwig;

        $recipe = get_queried_object();
        if ($recipe) {
            echo "<div class=\"tk-recipe-metas\">";
            echo $tkTwig->renderTemplate("recipe-metas.twig", array(
                "recipe" => $recipe));
            echo "</div>";
        }
    }
}
