<?php

class TkPrintButton extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-print-button';
    }

    public function get_title()
    {
        return __('(TK) Print button', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        echo "<button class=\"tk-button tk-button-outline tk-print-recipe-button\" onclick=\"window.print()\">Rezept drucken</button>";
    }
}
