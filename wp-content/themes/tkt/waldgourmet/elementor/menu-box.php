<?php

class TkMenuBox extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-menu-box';
    }

    public function get_title()
    {
        return __('(TK) Menu Box', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-box';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Content', 'plugin-name'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'boxHref',
            [
                'label' => __('Link', 'TKT'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'url',
                'placeholder' => __('https://your-link.com', 'TKT'),
            ]
        );

        $this->add_control(
            'boxImage',
            [
                'label' => __('Bild', 'TKT'),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $this->add_control(
            'boxTitle',
            [
                'label' => __('Titel', 'TKT'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __('Title', 'TKT'),
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $output = '
            <a class="p-0 tk-no-border" href="' . $settings['boxHref'] . '">
                <div class="tk-post-box">
                    <div class="tk-header tk-link-at-bottom">
                        <span class="tk-title tk-normal-title">' . $settings['boxTitle'] . '</span>
                        <span class="tk-arrow"><i class="fas fa-arrow-circle-right" aria-hidden="true"></i></span>
                    </div>
                    <img 
                         src="' . $settings['boxImage']['url'] . '"
                         class="attachment-tk-post-box-large size-tk-post-box-large wp-post-image lazyloaded tk-box-image"
                        >
                </div>
            </a>
            ';
     echo $output;
    }

}
