<?php

class TkRecipeIngredients extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-recipe-ingredients';
    }

    public function get_title()
    {
        return __('(TK) Recipe Ingredients', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        tkRenderRecipeIngredients();
    }
}

function tkRenderRecipeIngredients() {
    global $tkTwig;

    $recipe = get_queried_object();
    if ($recipe) {
        echo $tkTwig->renderTemplate("recipe-ingredients.twig", array(
            "recipe" => $recipe));
    }
}
add_shortcode('tk-recipe-ingredients', 'tkRenderRecipeIngredients');
