<?php


class TkTrustedShopsReviews extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-trusted-shops-reviews';
    }

    public function get_title()
    {
        return __('(TK) Trusted Shops Reviews', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {

    }

    protected function render()
    {
        $product = wc_get_product(get_queried_object_id());

        $productIds = [];
        if ($product instanceof WC_Product) {
            $productIds = [$product->get_sku()];
        }
        ?>
        <div id="tk-ts-reviews"></div>
        <script type="text/javascript">
            _tsProductReviewsConfig = {
                tsid: 'XE38B90314DDB83C7833F1C1293861722',
                sku: <?php echo json_encode($productIds) ?>,
                variant: 'productreviews',
                /* optional */
                borderColor: '#0DBEDC',
                backgroundColor: '#ffffff',
                locale: 'de_DE',
                starColor: '#FFDC0F',
                commentBorderColor: '#dad9d5',
                commentHideArrow: 'false',
                richSnippets: 'on',
                starSize: '15px',
                ratingSummary: 'false',
                maxHeight: '1200px',
                hideEmptySticker: 'false',
                filter: 'true',
                introtext: 'Produktbewertungen:'

            };
            var scripts = document.getElementsByTagName('SCRIPT'),
                me = scripts[scripts.length - 1];
            var _ts = document.createElement('SCRIPT');
            _ts.type = 'text/javascript';
            _ts.async = true;
            _ts.charset = 'utf-8';
            _ts.src = '//widgets.trustedshops.com/reviews/tsSticker/tsProductSticker.js';
            document.getElementById("tk-ts-reviews").appendChild(_ts);
            _tsProductReviewsConfig.script = _ts;
        </script>
        <?php
    }
}
