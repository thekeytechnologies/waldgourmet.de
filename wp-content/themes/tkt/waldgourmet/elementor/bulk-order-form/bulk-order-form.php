<?php

use Elementor\Widget_Base;

class TkBulkOrderForm extends Widget_Base
{
    public function get_name()
    {
        return 'tk-bulk-order-form-widget';
    }

    public function get_title()
    {
        return __('(TK) Bulk Order Form', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        add_filter('woocommerce_product_get_price', 'tkKilopreis', 10, 2);
        add_filter('woocommerce_product_get_regular_price', 'tkKilopreis', 10, 2);
        add_filter('woocommerce_product_get_sale_price', 'tkKilopreis', 10, 2);
        add_filter('woocommerce_product_variation_get_price', 'tkKilopreis', 10, 2);
        add_filter('woocommerce_product_variation_get_regular_price', 'tkKilopreis', 10, 2);

        echo $this->renderBulkOrderForm();
    }

    function renderBulkOrderForm() {

        $html = "";

        $categories = apply_filters('tk-bulk-order-form-categories', get_terms(array(
            'taxonomy' => 'product_cat',
            'hide_empty' => true,
            'orderby' => 'name',
            'order' => 'asc',
            'name' => array('rehfleisch', 'wildschweinfleisch', 'hirschfleisch', 'damhirschfleisch')
        )));

        $html .= "<form class='tk-bulk-order-form' method='post'>";
            $html .= "<div class='tk-header-row'>
                <div class='tk-title-col'>Produkt</div>
                <div class='tk-price-col'>Einzelpreis</div>
                <div class='tk-available-col'>Verfügbar</div>
                <div class='tk-amount-col'>Menge</div>
                <div class='tk-total-col'>Gesamtpreis</div>
            </div>";

            foreach ($categories as $category) {
                $html .= $this->renderCategory($category);
            }
            $html .= "<button type='submit' class='tk-button tk-button-primary'>Hinzufügen & Weiter</button>";

        $html .= "</form>";

        return $html;
    }
    /**
     * @param WP_Term $category
     * @return string
     */
    function renderCategory($category) {
        $html = "";

        $products = wc_get_products(array(
            "numberposts" => -1,
            'category' => $category,
            'orderby' => 'title',
            'order' => 'asc',
        ));

        $html .= "<div class='tk-category'>";
            $html .= "<div class='tk-category-header'>";
                $html .= $category->name;
            $html .= "</div>";
            $html .= "<div class='tk-products'>";
                foreach ($products as $product) {
                    $html .= $this->renderProduct($product);
                }
            $html .= "</div>"; /* tk-products */
        $html .= "</div>"; /* tk-category */

        return $html;
    }

    /**
     * @param WC_Product $product
     * @return string
     */
    function renderProduct($product) {
        $html = "";

        if ($product instanceof WC_Product_Variable) {
            $html .= $this->renderVariableProduct($product);
        } else {
            $html .= $this->renderSimpleProduct($product);
        }

        return $html;
    }

    /**
     * @param WC_Product $product
     * @return string
     */
    function renderVariableProduct($product) {
        $html = "";
        $variations = $product instanceof WC_Product_Variable ? $product->get_available_variations() : false;

        $html .= "<div class='tk-product-wrapper tk-variable-product'>";
            $html .= $this->renderProductRow($product, array(
                'addable' => 'sum',
                'price' => false,
                'class' => 'tk-variable-product-parent-row'
            ));
            $html .= "<div class='tk-variations'>";
                foreach($variations as $variation) {
                    $html .= $this->renderProductRow($product, array(
                        'name' =>  $variation["attributes"]["attribute_variante"],
                        'url' => false,
                        'img' => '',
                        'price' => $variation["display_price"],
                        'variationId' => $variation["variation_id"],
                        'class' => 'tk-variation'
                    ));
                }
            $html .= "</div>"; /* tk-variations */
        $html .= "</div>"; /* tk-variable-product */

        return $html;
    }

    /**
     * @param WC_Product $product
     * @return string
     */
    function renderSimpleProduct($product) {
        $html = "";

        $html .= "<div class='tk-product-wrapper tk-simple-product'>";
            $html .= $this->renderProductRow($product);
        $html .= "</div>"; /* tk-simple-product */
        return $html;
    }

    /**
     * @param WC_Product $product
     * @param array $args
     *      ['img']             string  - html of the thumbnail                             default product thumbnail
     *      ['name']            string  - name for product title                            default product name
     *      ['descriptions']    string  - description for product title                     default product description
     *      ['url']             string  - href for product title                            default product permalink
     *      ['price']           float   - price, set to false to display no price           default product price
     *      ['addable']         bool    - whether or not the product can be bought          default true if the product is in stock
     *      ['variationId']     string  - variationid                                       default ""
     *      ['class']           string  - classes to be added to the row                    default ""
     * @return string
     */
    function renderProductRow($product, $args = array('')) {

        $stockQuantity = $product->get_stock_quantity();
        $inStock = $stockQuantity > 0;
        $unit = get_post_meta($product->get_id(), "_unit_product", true);
        $step = $unit / 1000;
        $availableKg = $stockQuantity * $unit / 1000;

        $img = isset($args['img']) ? $args['img'] : $product->get_image('thumbnail');
        $name = isset($args['name']) ? $args['name'] : $product->get_name();
        $description = isset($args['description']) ? $args['description'] : $product->get_short_description();
        $url = isset($args['url']) ? $args['url'] : get_permalink($product->get_id());
        $price = isset($args['price']) ? $args['price'] : $product->get_price();
        $addable = isset($args['addable']) ? $args['addable'] : $inStock;
        $variationId = isset($args['variationId']) ? $args['variationId'] : "";
        $class = isset($args['class']) ? $args['class'] : "";

        $class .= $inStock ? ' tk-in-stock' : ' tk-not-in-stock';

        $html = "<div class='tk-product-row $class'>";

            /* Produktbezeichnung */
            $html .= "<div class='tk-title-col'>";
                $html .= sprintf("<div class='tk-product-image'>%s</div>",
                    $img
                );

                $html .= "<div class='tk-product-desc'>";
                    if ($url) {
                        $html .=  sprintf("<a href='%s' target='_blank'>",
                            $url
                        );
                    }

                        $html .= sprintf("<div class='tk-product-title'>%s</div>",
                            $name
                        );

                    if ($url) {
                        $html .= "</a>";
                    }

                    /*$html .=  sprintf("<div class='tk-product-short-desc'>%s</div>",
                        $description
                    );*/

                $html .= "</div>"; /* /tk-product-desc */
            $html .= "</div>"; /* tk-title-col */

            /* Preis */
            if ($price) {
                $html .= sprintf("<div class='tk-price-col'><div><span class='tk-price-container'>%s</span> € <span class='tk-light-font'>pro Kg</span></div></div>",
                    number_format($price, 2, ',', '.')
                );
            } else {
                $html .= sprintf("<div class='tk-price-col'></div>");
            }


            /* Verfügbar */
            $html .= sprintf("<div class='tk-available-col'><span class='tk-available-amount'>%s</span> kg</div>",
                number_format($availableKg, 1, ',', '.')
            );

            /* Mengenauswahl */
            if ($addable == 'addable' && $inStock) {
                $html .= sprintf("<div class='tk-amount-col tk-addable'><label><input type='number' step='%s' min='0' max='%s' placeholder='0,0' data-tk-portion-size='%s' data-tk-product-id='%s' data-tk-variation-id='%s'> kg</label></div>",
                    $step,
                    $availableKg,
                    $step,
                    $product->get_id(),
                    $variationId
                );
            } elseif ($addable == 'sum' && $inStock) {
                $html .= sprintf("<div class='tk-amount-col tk-sum'><label><input readonly type='number' max='%s' placeholder='0,0' data-tk-product-id='%s'> kg</label></div>",
                    $availableKg,
                    $product->get_id()
                );
            } else {
                $html .= sprintf("<div class='tk-amount-col tk-not-addable'><span class='tk-not-in-stock-message'>Nicht Verfügbar</></div>");
            }

            /* Gesamtpreis */
            $html .= sprintf("<div class='tk-total-col'><div class='tk-total-top'><span class='tk-price-container'>%s</span> €</div><div class='tk-total-addendum tk-light-font'>zzgl. Versand</div></div>",
                number_format(0, 2, ',', '.')
            );

        $html .= "</div>"; /* tk-product-row */

        return $html;
    }
}
