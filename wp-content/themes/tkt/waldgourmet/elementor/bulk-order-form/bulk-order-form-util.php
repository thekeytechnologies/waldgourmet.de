<?php

function tkIsGastronomentabelle() {
    $currentPage = get_queried_object_id();
    if ($currentPage == 4715) {
        return true;
    }

    return false;
}

function tkKilopreis($price, $product)
{
    if ($price == "") {
        return $price;
    } else {
        return $price * 5;
    }

}