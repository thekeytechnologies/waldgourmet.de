<?php

class TkProductForRecipeButton extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-product-for-recipe-button';
    }

    public function get_title()
    {
        return __('(TK) Product for recipe button', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        $relatedProduct = tkWpMeta(get_queried_object(), "tk-product-ref");

        if ($relatedProduct) {
            $url = tkWpUrl($relatedProduct);
            $title = tkWpTitle($relatedProduct);

            echo "<a href=\"$url\" target=\"_blank\">";
            echo "<button class=\"tk-button tk-related-product-button\">$title kaufen</button>";
            echo "</a>";
        }
    }
}
