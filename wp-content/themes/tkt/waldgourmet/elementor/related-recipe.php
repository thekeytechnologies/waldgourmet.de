<?php

class TkRelatedRecipeWidget extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-related-recipe';
    }

    public function get_title()
    {
        return __('(TK) Related Recipe', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['woocommerce-elements'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        global $tkTwig;

        $recipe = get_post_meta(get_queried_object_id(), "tk-recipe-ref", true);
        if ($recipe) {
            echo $tkTwig->renderTemplate("tk-post-box-widget.twig", array(
                "widgetTitle" => "Das passende Rezept",
                "linkAtBottom" => true,
                "normalTitle" => true,
                "imageSize" => "tk-post-box-large",
                "linkTitle" => get_post_meta(get_queried_object_id(), "tk-recipe-title", true),
                "post" => $recipe));
        }
    }
}
