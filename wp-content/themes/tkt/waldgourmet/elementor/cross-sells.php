<?php

class TkCrosssellsWidget extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-cross-sells';
    }

    public function get_title()
    {
        return __('(TK) Crosssells', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['woocommerce-elements'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        global $tkTwig;

        $product = WooCommerce::instance()->product_factory->get_product(get_queried_object_id());
        if ($product) {
            $crossSells = $product->get_cross_sell_ids();

            foreach ($crossSells as $crossSell) {
                echo $tkTwig->renderTemplate("tk-post-box-widget.twig", array(
                    "widgetTitle" => "Alternatives Produkt",
                    "linkAtBottom" => true,
                    "normalTitle" => true,
                    "imageSize" => "tk-post-box-large",
                    "post" => get_post($crossSell)));
            }
        }
    }
}
