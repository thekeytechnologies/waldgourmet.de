<?php


class TKSingleRecipeSidebarBoxes extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-single-recipe-sidebar-boxes';
    }

    public function get_title()
    {
        return __('(TK) Recpie Sidebar Boxes', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['woocommerce-elements'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {

        $boxes = array();



        /* First Box - Recipe */
        $product = tkWpMeta(get_queried_object_id(), 'tk-product-ref');
        if ($product) {
            $boxes[] = array(
                'id' => $product['ID'],
                'supertitle' => "Das Produkt zum Rezept",
                'class' => 'tk-product-box'
            );
        }


        /* Second Box - Naturbox */
        $globalProduct = get_option("tk-wg-settings_tk-product-sidebar-global-product-box");
        if ($globalProduct) {
            $boxes[] = array(
                'id' => $globalProduct[0],
                'supertitle' => "Jeden Monat eine Freude",
                'class' => 'tk-naturbox-box'
            );
        }


        /* Third Box - Gastblogpost */
        $guestPost = get_option("tk-wg-settings_tk-recipe-sidebar-guest-box");
        if ($guestPost) {
            $boxes[] = array(
                'id' => $guestPost[0],
                'supertitle' => "Bei unseren Freunden essen",
                'class' => 'tk-guest-box'
            );
        }


        global $tkTwig;
        echo $tkTwig->renderTemplate("sidebar-boxes.twig", array(
            'boxes' => $boxes
        ));
    }
}
