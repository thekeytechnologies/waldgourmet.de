<?php


class TKSingleProductsSidebarBoxes extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-single-product-sidebar-boxes';
    }

    public function get_title()
    {
        return __('(TK) Product Sidebar Boxes', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['woocommerce-elements'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {

        $boxes = array();



        /* First Box - Recipe */
        $recipe = tkWpMeta(get_queried_object_id(), 'tk-recipe-ref');
        if ($recipe) {
            $boxes[] = array(
                'id' => $recipe['ID'],
                'supertitle' => "Das Rezept zum Produkt",
                'class' => 'tk-recipe-box'
            );
        }


        /* Second Box - Naturbox */
        $globalProduct = get_option("tk-wg-settings_tk-product-sidebar-global-product-box");
        if ($globalProduct) {
            $boxes[] = array(
                'id' => $globalProduct[0],
                'supertitle' => "Jeden Monat eine Freude",
                'class' => 'tk-naturbox-box'
            );
        }


        /* Third Box - Gastprodukt */
        $guestProduct = get_option("tk-wg-settings_tk-product-sidebar-guest-box");
        if ($guestProduct) {
            $boxes[] = array(
                'id' => $guestProduct[0],
                'supertitle' => "Produkte unserer Freunde",
                'class' => 'tk-guest-box'
            );
        }


        global $tkTwig;
        echo $tkTwig->renderTemplate("sidebar-boxes.twig", array(
            'boxes' => $boxes
        ));
    }
}
