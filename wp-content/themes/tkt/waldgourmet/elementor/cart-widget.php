<?php

class TkCartWidget extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-cart';
    }

    public function get_title()
    {
        return __('(TK) Cart', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['woocommerce-elements'];
    }

    protected function _register_controls()
    {
    }

    public function render()
    {
        $cartCount = WooCommerce::instance()->cart->get_cart_contents_count();

        echo "<div class=\"tk-icon-link tk-cart-link\">
                    <a href=\"/warenkorb/\">
                        <div class=\"tk-cart-count\">$cartCount</div>
                        <img src=\"/wp-content/themes/tkt/assets/img/einkaufskorb.png\">
                    </a>
                </div>";
    }
}
