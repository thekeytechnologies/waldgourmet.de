<?php

class TkRecipeContent extends \Elementor\Widget_Base
{
    public function get_name()
    {
        return 'tk-recipe-content';
    }

    public function get_title()
    {
        return __('(TK) Recipe Content', 'tkt');
    }

    public function get_icon()
    {
        return 'fas fa-star';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
    }

    protected function render()
    {
        tkRenderRecipeContent();
    }
}

function tkRenderRecipeContent() {
    global $tkTwig;

    $recipe = get_queried_object();
    if ($recipe) {
        echo $tkTwig->renderTemplate("recipe-content.twig", array(
            "recipe" => $recipe));
    }
}
add_shortcode('tk-recipe-content', 'tkRenderRecipeContent');
