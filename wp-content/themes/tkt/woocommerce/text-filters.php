<?php

add_filter("woocommerce_product_add_to_cart_text", "tk_add_signup_text_to_atc_button", 10, 2);
function tk_add_signup_text_to_atc_button ($text, $product) {

    /* Should only happen when not in stock */
    if ($product->is_in_stock() && $product->is_purchasable()) {
        return $text;
    } else {
        return "Zum Produkt";
    }
};

add_filter("woocommerce_get_price_html", "tkPriceRangeToLowestPrice");
//add_filter("woo_discount_rules_price_strikeout_discount_price_html", "tkPriceRangeToLowestPrice");

function tkPriceRangeToLowestPrice($html) {
    if (strpos($html, "&ndash;")) {
        $firstPart = explode("&ndash;", $html);
        return "ab " . $firstPart[0];
    }

    return $html;
}
