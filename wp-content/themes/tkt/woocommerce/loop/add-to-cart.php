<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

/* default stuff (basically the same as the wc original, only more readable */
$url = esc_url( $product->add_to_cart_url() );
$quantity = esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 );
$class = esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' );
$attributes = isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '';
$content = esc_html( $product->add_to_cart_text() );

/* tkt stuff */
$icon = tk_get_product_atc_icon($product);
if ($icon) {
    $icon = "<div class='tk-atc-icon'>" . wp_get_attachment_image($icon, 'tk-category-icon') . "</div>";
}

$link = "<a href='$url' data-quantity='$quantity' class='$class' $attributes>$content $icon</a>";


$link = apply_filters( 'woocommerce_loop_add_to_cart_link', $link, $product, $args);// WPCS: XSS ok.

echo $link;
