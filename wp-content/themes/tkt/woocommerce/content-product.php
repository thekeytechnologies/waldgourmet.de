<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}

add_filter('single_product_archive_thumbnail_size', function () {
    return "tk-shop-archive";
});

remove_action("woocommerce_before_shop_loop_item", "woocommerce_template_loop_product_link_open");
remove_action("woocommerce_after_shop_loop_item_title", "woocommerce_template_loop_product_link_close");
remove_action("woocommerce_after_shop_loop_item", "woocommerce_gzd_template_single_tax_info", wc_gzd_get_hook_priority('loop_tax_info'));
remove_action("woocommerce_after_shop_loop_item", "woocommerce_gzd_template_single_shipping_costs_info", wc_gzd_get_hook_priority('loop_shipping_costs_info'));
remove_action("woocommerce_after_shop_loop_item_title", "woocommerce_gzd_template_single_price_unit", wc_gzd_get_hook_priority('loop_price_unit'));

add_action("tk-woocommerce-loop-legal", "woocommerce_gzd_template_single_tax_info", wc_gzd_get_hook_priority('loop_tax_info'));
add_action("tk-woocommerce-loop-legal", "woocommerce_gzd_template_single_shipping_costs_info", wc_gzd_get_hook_priority('loop_shipping_costs_info'));


?>
<li <?php wc_product_class("tk-product", $product); ?>>

    <?php
    /**
     * Hook: woocommerce_before_shop_loop_item.
     *
     * //     * @hooked woocommerce_template_loop_product_link_open - 10
     */
    do_action('woocommerce_before_shop_loop_item');

    /**
     * Hook: woocommerce_before_shop_loop_item_title.
     *
     * @hooked woocommerce_show_product_loop_sale_flash - 10
     * @hooked woocommerce_template_loop_product_thumbnail - 10
     * @hooked tk_product_thumbnail_icons - 11
     */
    remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash');
    woocommerce_template_loop_product_link_open();
    do_action('woocommerce_before_shop_loop_item_title');
    woocommerce_template_loop_product_link_close();


    ?>
    <div class="tk-description">
        <div class="tk-header">
            <div class="tk-title">
                <?php

                /**
                 * Hook: woocommerce_shop_loop_item_title.
                 *
                 * @hooked woocommerce_template_loop_product_title - 10
                 */
                woocommerce_template_loop_product_link_open();
                do_action('woocommerce_shop_loop_item_title');
                woocommerce_template_loop_product_link_close();

                ?>
                <div class="tk-subtitle"><?php echo get_post_meta($product->get_id(), "tk-short-title", true) ?></div>
            </div>
            <div class="tk-category-icon">
                <?php
                $terms = get_the_terms($product->get_id(), "product_cat");
                $mainCategory = $terms[0];
                if ($mainCategory) {
                    $icon = get_term_meta($mainCategory->term_id, "tk-category-icon", true);
                    if ($icon) {
                        echo wp_get_attachment_image(tkWpId($icon), "tk-category-icon");
                    }
                }
                ?>
            </div>
        </div>
        <hr>
        <div class="tk-footer">

            <div class="tk-price-and-legal-wrapper">
                <div class="tk-price-wrapper">
                    <?php

                    /**
                     * Hook: woocommerce_after_shop_loop_item_title.
                     *
                     * @hooked woocommerce_template_loop_rating - 5
                     * @hooked woocommerce_template_loop_price - 10
                     */
                    do_action('woocommerce_after_shop_loop_item_title');

                    ?>
                    <span class="tk-price-unit">
                        <?php echo get_post_meta($product->get_id(), "tk-base-price-unit-title", true) ?>
                    </span>
                </div>
                <div class="tk-legal-wrapper">
                    <?php do_action("tk-woocommerce-loop-legal") ?>
                </div>
            </div>

            <div class="tk-add-to-cart-wrapper">
                <?php

                /**
                 * Hook: woocommerce_after_shop_loop_item.
                 *
                 * //         * @hooked woocommerce_template_loop_product_link_close - 5
                 * @hooked woocommerce_template_loop_add_to_cart - 10
                 */
                do_action('woocommerce_after_shop_loop_item');
                ?>
            </div>
        </div>
    </div>
</li>
