<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product;
$price = $product->get_price_html();
?>
<div class="tk-price">
    <div class="price">
        <?php echo $price; ?>
    </div>
    <div class="tk-price-unit"
         data-tk-base-price-value="<?php echo get_post_meta($product->get_id(), "_unit_product", true); ?>"
         data-tk-base-price-unit="<?php echo get_post_meta($product->get_id(), '_unit', true) ?>"
    >
        <?php echo get_post_meta($product->get_id(), "tk-base-price-unit-title", true) ?>
    </div>

</div>
