<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */

get_header();
while (have_posts()) {
    the_post();                            // Post Loop
}

get_footer();

// Omit Closing PHP Tags