<?php
/**
 * Class RuleConditions
 *
 * @package WPDesk\FSPro\TableRate\Rule
 */

namespace WPDesk\FSPro\TableRate\Rule;

use FSProVendor\WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\FS\TableRate\Rule\Condition\AbstractCondition;
use WPDesk\FSPro\TableRate\Rule\Condition\CartLineItem;
use WPDesk\FSPro\TableRate\Rule\Condition\Item;
use WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory\CategoriesOptions;
use WPDesk\FSPro\TableRate\Rule\Condition\ShippingClass;

/**
 * Can provide rule conditions.
 */
class RuleConditions implements Hookable {

	/**
	 * @var CategoriesOptions
	 */
	private $categories_options;

	/**
	 * RuleConditions constructor.
	 *
	 * @param CategoriesOptions $categories_options .
	 */
	public function __construct( CategoriesOptions $categories_options ) {
		$this->categories_options = $categories_options;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_filter( 'flexible_shipping_rule_conditions', array( $this, 'add_rule_conditions' ) );
	}

	/**
	 * @param AbstractCondition[] $conditions .
	 *
	 * @return AbstractCondition[]
	 */
	public function add_rule_conditions( $conditions ) {
		$item           = new Item();
		$cart_line_item = new CartLineItem();
		$shipping_class = new ShippingClass( $this->get_shipping_classes() );

		$conditions[ $item->get_condition_id() ]           = $item;
		$conditions[ $cart_line_item->get_condition_id() ] = $cart_line_item;
		$conditions[ $shipping_class->get_condition_id() ] = $shipping_class;

		return $conditions;
	}

	/**
	 * @return array
	 */
	private function get_shipping_classes() {
		$shipping_classes_terms = WC()->shipping()->get_shipping_classes();
		$shipping_classes       = array();
		foreach ( $shipping_classes_terms as $shipping_class ) {
			$shipping_classes[ $shipping_class->term_id ] = $shipping_class->name;
		}

		return $shipping_classes;
	}
}
