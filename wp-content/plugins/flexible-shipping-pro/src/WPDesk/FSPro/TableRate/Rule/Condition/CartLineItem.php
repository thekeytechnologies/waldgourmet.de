<?php
/**
 * Class CartLineItem
 *
 * @package WPDesk\FSPro\TableRate\Rule\Condition
 */

namespace WPDesk\FSPro\TableRate\Rule\Condition;

use Psr\Log\LoggerInterface;
use WPDesk\FS\TableRate\Rule\Condition\AbstractCondition;
use WPDesk\FS\TableRate\Rule\ShippingContents\ShippingContents;
use FSVendor\WPDesk\Forms\Field;

/**
 * Cart line item condition.
 */
class CartLineItem extends AbstractCondition {

	const MIN          = 'min';
	const MAX          = 'max';
	const CONDITION_ID = 'cart_line_item';

	/**
	 * None constructor.
	 */
	public function __construct() {
		$this->condition_id = self::CONDITION_ID;
		$this->name         = __( 'Cart line item', 'flexible-shipping-pro' );
	}

	/**
	 * @param array            $condition_settings .
	 * @param ShippingContents $contents           .
	 * @param LoggerInterface  $logger             .
	 *
	 * @return bool
	 */
	public function is_condition_matched( array $condition_settings, ShippingContents $contents, LoggerInterface $logger ) {
		$min = (float) ( $condition_settings[ self::MIN ] ? $condition_settings[ self::MIN ] : 0 );
		$max = (float) ( $condition_settings[ self::MAX ] ? $condition_settings[ self::MAX ] : INF );

		$contents_lines = count( $contents->get_contents() );

		$condition_matched = $contents_lines >= $min && $contents_lines <= $max;

		$logger->debug( $this->format_for_log( $condition_settings, $condition_matched, $contents_lines ) );

		return $condition_matched;
	}

	/**
	 * @return Field[]
	 */
	public function get_fields() {
		return array(
			( new Field\InputNumberField() )
				->set_name( self::MIN )
				->add_class( 'wc_input_decimal' )
				->add_class( 'hs-beacon-search' )
				->add_class( 'parameter_min' )
				->add_data( 'beacon_search', __( 'cart lines is from', 'flexible-shipping-pro' ) )
				->set_placeholder( __( 'min', 'flexible-shipping-pro' ) )
				->set_label( __( 'is from', 'flexible-shipping-pro' ) )
				->add_data( 'suffix', __( 'lines', 'flexible-shipping-pro' ) ),
			( new Field\InputNumberField() )
				->set_name( self::MAX )
				->add_class( 'wc_input_decimal' )
				->add_class( 'hs-beacon-search' )
				->add_class( 'parameter_max' )
				->add_data( 'beacon_search', __( 'lines to', 'flexible-shipping-pro' ) )
				->set_placeholder( __( 'max', 'flexible-shipping-pro' ) )
				->set_label( __( 'to', 'flexible-shipping-pro' ) )
				->add_data( 'suffix', __( 'lines', 'flexible-shipping-pro' ) ),
		);
	}

}
