<?php
/**
 * Class ShippingClassContentsFilter
 *
 * @package WPDesk\FSPro\TableRate\Rule
 */

namespace WPDesk\FSPro\TableRate\Rule\Condition\ShippingClass;

use WPDesk\FS\TableRate\Rule\ContentsFilter;
use WPDesk\FSPro\TableRate\Rule\Condition\ShippingClass;

/**
 * Can filter shipping contents against shipping classes.
 */
class ShippingClassContentsFilter implements ContentsFilter {

	/**
	 * @var array
	 */
	private $shipping_classes;

	/**
	 * @var array
	 */
	private $present_shipping_classes = array();

	/**
	 * ShippingClassContentsFilter constructor.
	 *
	 * @param array $shipping_classes .
	 */
	public function __construct( array $shipping_classes ) {
		$this->shipping_classes = array_map( 'strval', $shipping_classes );
	}


	/**
	 * Returns filtered contents.
	 *
	 * @param array $contents .
	 *
	 * @return array
	 */
	public function get_filtered_contents( array $contents ) {
		foreach ( $contents as $key => $item ) {
			if ( $this->should_be_item_removed( $this->get_product_from_item( $item ) ) ) {
				unset( $contents[ $key ] );
			}
		}
		foreach ( $this->shipping_classes as $shipping_class ) {
			if ( ! isset( $this->present_shipping_classes[ (string) $shipping_class ] ) ) {
				$contents = array();
			}
		}

		return $contents;
	}

	/**
	 * @param array $item .
	 *
	 * @return \WC_Product
	 */
	private function get_product_from_item( array $item ) {
		return $item['data'];
	}

	/**
	 * @param \WC_Product $product .
	 *
	 * @return bool
	 */
	private function should_be_item_removed( $product ) {
		if ( in_array( ShippingClass::ALL_PRODUCTS, $this->shipping_classes, true ) ) {
			$this->present_shipping_classes[ ShippingClass::ALL_PRODUCTS ] = 1;
			return false;
		}

		$shipping_class_id = (string) $this->convert_shipping_class_id_for_wpml( $product->get_shipping_class_id() );
		if ( $shipping_class_id ) {

			if ( in_array( ShippingClass::NONE, $this->shipping_classes, true ) ) {
				return true;
			}

			if ( in_array( ShippingClass::ANY_CLASS, $this->shipping_classes, true ) || in_array( $shipping_class_id, $this->shipping_classes, true ) ) {
				$this->present_shipping_classes[ ShippingClass::ANY_CLASS ] = 1;
				$this->present_shipping_classes[ $shipping_class_id ]       = 1;

				return false;
			}
		} else {
			if ( in_array( ShippingClass::NONE, $this->shipping_classes, true ) ) {
				$this->present_shipping_classes[ ShippingClass::NONE ] = 1;

				return false;
			}
		}

		return true;
	}

	/**
	 * Maybe convert shipping class id (WPML).
	 *
	 * @param string $shipping_class_id .
	 *
	 * @return mixed
	 */
	private function convert_shipping_class_id_for_wpml( $shipping_class_id ) {
		if ( $shipping_class_id && function_exists( 'icl_object_id' ) ) {
			global $sitepress;
			if ( ! empty( $sitepress ) ) {
				$default_language  = $sitepress->get_default_language();
				$shipping_class_id = icl_object_id( $shipping_class_id, 'product_shipping_class', false, $default_language );
			}
		}

		return $shipping_class_id;
	}


}
