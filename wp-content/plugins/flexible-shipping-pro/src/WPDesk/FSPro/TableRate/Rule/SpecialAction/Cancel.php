<?php
/**
 * Class Cancel
 *
 * @package WPDesk\FSPro\TableRate\Rule\SpecialAction
 */

namespace WPDesk\FSPro\TableRate\Rule\SpecialAction;

use WPDesk\FS\TableRate\Rule\SpecialAction\AbstractSpecialAction;

/**
 * Cancel action.
 */
class Cancel extends AbstractSpecialAction {

	/**
	 * Cancel constructor.
	 */
	public function __construct() {
		parent::__construct( 'cancel', __( 'Cancel', 'flexible-shipping-pro' ) );
	}

	/**
	 * @return bool
	 */
	public function is_cancel() {
		return true;
	}

	/**
	 * @return bool
	 */
	public function is_stop() {
		return false;
	}

}
