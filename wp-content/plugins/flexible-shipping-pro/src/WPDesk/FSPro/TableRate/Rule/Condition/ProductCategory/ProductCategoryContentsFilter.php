<?php
/**
 * Class ProductCategoryContentsFilter
 *
 * @package WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory
 */

namespace WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory;

use WPDesk\FS\TableRate\Rule\ContentsFilter;
use WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory;
use WPDesk\FSPro\TableRate\Rule\Condition\ShippingClass;

/**
 * Can filter shipping contents against shipping classes.
 */
class ProductCategoryContentsFilter implements ContentsFilter {

	/**
	 * @var array
	 */
	private $product_categories;

	/**
	 * @var array
	 */
	private $present_product_categories = array();

	/**
	 * ProductCategoryContentsFilter constructor.
	 *
	 * @param array $product_categories .
	 */
	public function __construct( array $product_categories ) {
		$this->product_categories = array_map( 'strval', $product_categories );
	}

	/**
	 * Returns filtered contents.
	 *
	 * @param array $contents .
	 *
	 * @return array
	 */
	public function get_filtered_contents( array $contents ) {
		foreach ( $contents as $key => $item ) {
			if ( $this->should_be_item_removed( $this->get_product_from_item( $item ) ) ) {
				unset( $contents[ $key ] );
			}
		}
		foreach ( $this->product_categories as $product_category ) {
			if ( ! isset( $this->present_product_categories[ (string) $product_category ] ) ) {
				$contents = array();
			}
		}

		return $contents;
	}

	/**
	 * @param array $item .
	 *
	 * @return \WC_Product
	 */
	private function get_product_from_item( array $item ) {
		return $item['data'];
	}

	/**
	 * @param \WC_Product $product .
	 *
	 * @return bool
	 */
	private function should_be_item_removed( $product ) {
		$product_categories = $product->get_category_ids();
		if ( 0 === count( $product_categories ) ) {
			if ( in_array( ProductCategory::NONE, $this->product_categories, true ) ) {
				$this->present_product_categories[ ProductCategory::NONE ] = 1;
				return false;
			}
		} else {
			$this->present_product_categories[ ProductCategory::ANY_CATEGORY ] = 1;
			foreach ( $product_categories as $product_category ) {
				if ( in_array( (string) $product_category, $this->product_categories, true ) ) {
					$this->present_product_categories[ (string) $product_category ] = 1;
					return false;
				}
			}
		}

		return true;
	}

}
