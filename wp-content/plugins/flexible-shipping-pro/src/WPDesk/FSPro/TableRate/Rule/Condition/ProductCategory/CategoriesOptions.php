<?php
/**
 * Class CategoriesOptions
 *
 * @package WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory
 */

namespace WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory;

use WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory;

/**
 * Can provide product categories options.
 */
class CategoriesOptions {

	/**
	 * @var array
	 */
	private $predefined_values;

	/**
	 * CategoriesOptions constructor.
	 */
	public function __construct() {
		$this->prepare_predefined_values();
	}

	/**
	 * @param string $search_text .
	 *
	 * @return array
	 */
	public function search_categories( $search_text ) {
		$found_categories = array();
		$args             = array(
			'taxonomy'   => array( 'product_cat' ),
			'orderby'    => 'id',
			'order'      => 'ASC',
			'hide_empty' => true,
			'fields'     => 'all',
			'name__like' => $search_text,
		);

		$terms = get_terms( $args );

		if ( $terms ) {
			foreach ( $terms as $term ) {
				$term->formatted_name = $this->get_term_formatted_name( $term ) . $term->name;
				$found_categories[]   = $this->prepare_option( $term->term_id, $term->formatted_name );
			}
		}

		$found_predefined = $this->search_predefined( $search_text );

		return array_merge( $found_predefined, $found_categories );
	}

	/**
	 * @param string $search_text .
	 *
	 * @return array
	 */
	private function search_predefined( $search_text ) {
		$found_categories = array();
		foreach ( $this->predefined_values as $key => $value ) {
			$callback = function_exists( 'mb_strpos' ) ? 'mb_strpos' : 'strpos';
			if ( false !== $callback( $value, $search_text ) ) {
				$found_categories[] = $this->prepare_option( $key, $value );
			}
		}
		return $found_categories;
	}

	/**
	 * @param \WP_Term $term .
	 *
	 * @return string
	 */
	private function get_term_formatted_name( \WP_Term $term ) {
		$formatted_name = '';
		if ( $term->parent ) {
			$ancestors = array_reverse( get_ancestors( $term->term_id, 'product_cat' ) );
			foreach ( $ancestors as $ancestor ) {
				$ancestor_term = get_term( $ancestor, 'product_cat' );
				if ( $ancestor_term ) {
					$formatted_name .= $ancestor_term->name . ' > ';
				}
			}
		}

		return $formatted_name;
	}

	/**
	 * @param string $category_id .
	 *
	 * @return array|null
	 */
	public function get_category_option( $category_id ) {
		if ( in_array( $category_id, $this->predefined_values, true ) ) {
			$this->prepare_option( $category_id, $this->predefined_values[ $category_id ] );
		}

		$option = null;
		$term   = get_term( $category_id, 'product_cat' );

		if ( $term ) {
			$term->formatted_name = $this->get_term_formatted_name( $term ) . $term->name;
			$option               = $this->prepare_option( $term->term_id, $term->formatted_name );
		}

		return $option;
	}

	/**
	 * @param string $value .
	 * @param string $label .
	 *
	 * @return array
	 */
	private function prepare_option( $value, $label ) {
		return array(
			'value' => $value,
			'label' => $label,
		);
	}

	/**
	 * .
	 */
	private function prepare_predefined_values() {
		$this->predefined_values                                  = array();
		$this->predefined_values[ ProductCategory::ANY_CATEGORY ] = __( 'Any category (must be set)', 'flexible-shipping-pro' );
		$this->predefined_values[ ProductCategory::NONE ]         = __( 'None (category not set)', 'flexible-shipping-pro' );
	}

}
