<?php
/**
 * Class ProductCategory
 *
 * @package WPDesk\FSPro\TableRate\Rule\Condition
 */

namespace WPDesk\FSPro\TableRate\Rule\Condition;

use Psr\Log\LoggerInterface;
use WPDesk\FS\TableRate\Rule\Condition\AbstractCondition;
use FSVendor\WPDesk\Forms\Field;
use WPDesk\FS\TableRate\Rule\ShippingContents\ShippingContents;
use WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory\AjaxHandler;
use WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory\CategoriesOptions;
use WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory\ProductCategoryContentsFilter;

/**
 * Product Category Condition.
 */
class ProductCategory extends AbstractCondition {

	const ANY_CATEGORY = 'any';
	const NONE         = 'none';

	const PREDEFINED_VALUES = array(
		self::ANY_CATEGORY,
		self::NONE,
	);

	const CONDITION_ID = 'product_category';

	/**
	 * @var CategoriesOptions
	 */
	private $categories;

	/**
	 * ProductCategory constructor.
	 *
	 * @param CategoriesOptions $categories .
	 */
	public function __construct( CategoriesOptions $categories ) {
		$this->condition_id = self::CONDITION_ID;
		$this->name         = __( 'Product category', 'flexible-shipping-pro' );
		$this->categories   = $categories;
	}

	/**
	 * @param array            $condition_settings .
	 * @param ShippingContents $contents           .
	 * @param LoggerInterface  $logger             .
	 *
	 * @return bool
	 */
	public function is_condition_matched( array $condition_settings, ShippingContents $contents, LoggerInterface $logger ) {
		$condition_matched = 0 !== count( $contents->get_contents() );

		$logger->debug( $this->format_for_log( $condition_settings, $condition_matched, $this->format_input_data_for_logger( $contents->get_contents() ) ) );

		return $condition_matched;
	}

	/**
	 * @param array $contents .
	 *
	 * @return string
	 */
	private function format_input_data_for_logger( array $contents ) {
		$format_input_data = '';

		foreach ( $this->get_product_categories_from_contents( $contents ) as $product_category ) {
			$format_input_data .= $product_category . ', ';
		}

		return trim( $format_input_data, ', ' );
	}

	/**
	 * @param array $condition_settings .
	 *
	 * @return array
	 */
	public function prepare_settings( $condition_settings ) {
		$categories = isset( $condition_settings[ self::CONDITION_ID ] ) ? $condition_settings[ self::CONDITION_ID ] : '';
		if ( ! is_array( $categories ) ) {
			$categories                               = array();
			$condition_settings[ self::CONDITION_ID ] = array();
		}
		$categories_options = array();
		foreach ( $categories as $single_category ) {
			$option = $this->categories->get_category_option( $single_category );
			if ( $option ) {
				$categories_options[] = $option;
			}
		}
		$condition_settings['select_options'] = $categories_options;

		return $condition_settings;
	}


	/**
	 * @param array $contents .
	 *
	 * @return array
	 */
	private function get_product_categories_from_contents( array $contents ) {
		$contents_product_categories = array();
		foreach ( $contents as $contents_item ) {
			$product            = $this->get_product_from_contents_item( $contents_item );
			$product_categories = $product->get_category_ids();
			if ( 0 === count( $product_categories ) ) {
				$contents_product_categories[ self::NONE ] = __( 'None (category not set)', 'flexible-shipping-pro' );
			} else {
				foreach ( $product_categories as $product_category ) {
					$contents_product_categories[ $product_category ] = $this->categories->get_category_option( $product_category )['label'];
				}
			}
		}

		return $contents_product_categories;
	}

	/**
	 * @param array $contents_item .
	 *
	 * @return \WC_Product
	 */
	private function get_product_from_contents_item( array $contents_item ) {
		return $contents_item['data'];
	}

	/**
	 * @param array  $condition_settings .
	 * @param bool   $condition_matched .
	 * @param string $input_data .
	 *
	 * @return string
	 */
	protected function format_for_log( array $condition_settings, $condition_matched, $input_data ) {
		// Translators: condition name.
		$formatted_for_log = '   ' . sprintf( __( 'Condition: %1$s;', 'flexible-shipping-pro' ), $this->get_name() );

		$product_categories = isset( $condition_settings[ self::CONDITION_ID ] ) ? $condition_settings[ self::CONDITION_ID ] : array();
		$product_categories = is_array( $product_categories ) ? $product_categories : array( $product_categories );

		$formatted_product_categories = '';
		foreach ( $product_categories as $product_category ) {
			if ( in_array( $product_category, self::PREDEFINED_VALUES, true ) ) {
				$formatted_product_categories .= $product_category . ', ';
			} else {
				$formatted_product_categories .= $this->categories->get_category_option( $product_category )['label'] . ', ';
			}
		}
		$formatted_product_categories = trim( $formatted_product_categories, ', ' );

		// Translators: input data.
		$formatted_for_log .= sprintf( __( ' product categories: %1$s;', 'flexible-shipping-pro' ), $formatted_product_categories );
		// Translators: input data.
		$formatted_for_log .= sprintf( __( ' input data: %1$s;', 'flexible-shipping-pro' ), $input_data );
		// Translators: matched condition.
		$formatted_for_log .= sprintf( __( ' matched: %1$s', 'flexible-shipping-pro' ), $condition_matched ? __( 'yes', 'flexible-shipping-pro' ) : __( 'no', 'flexible-shipping-pro' ) );

		return $formatted_for_log;
	}

	/**
	 * @return Field[]
	 */
	public function get_fields() {
		return array(
			( new Field\WooSelect() )
				->set_name( self::CONDITION_ID )
				->set_multiple()
				->add_class( 'product-category' )
				->set_placeholder( __( 'search product category', 'flexible-shipping-pro' ) )
				->set_label( __( 'is', 'flexible-shipping-pro' ) )
				->add_data( 'ajax-url', $this->create_ajax_url() )
				->add_data( 'async', true )
				->add_data( 'autoload', true ),
		);
	}

	/**
	 * @return string
	 */
	private function create_ajax_url() {
		return admin_url( 'admin-ajax.php?action=' . AjaxHandler::AJAX_ACTION . '&security=' . wp_create_nonce( AjaxHandler::NONCE_ACTION ) );
	}

	/**
	 * @param ShippingContents $shipping_contents .
	 * @param array            $condition_settings .
	 *
	 * @return ShippingContents
	 */
	public function process_shipping_contents( ShippingContents $shipping_contents, array $condition_settings ) {
		if ( isset( $condition_settings[ self::CONDITION_ID ] ) ) {
			$product_categories = is_array( $condition_settings[ self::CONDITION_ID ] ) ? $condition_settings[ self::CONDITION_ID ] : array( $condition_settings[ self::CONDITION_ID ] );
			if ( ! empty( $product_categories ) ) {
				$shipping_contents->filter_contents( new ProductCategoryContentsFilter( $product_categories ) );
			}
		}

		return $shipping_contents;
	}

}
