<?php
/**
 * Plugin Name: Flexible Shipping PRO
 * Plugin URI: https://flexibleshipping.com/table-rate/
 * Description: Extends the free version of Flexible Shipping by adding advanced pro features.
 * Version: 2.0.3
 * Author: WP Desk
 * Author URI: https://www.wpdesk.net/
 * Text Domain: flexible-shipping-pro
 * Domain Path: /lang/
 * Requires at least: 4.9
 * Tested up to: 5.6
 * WC requires at least: 4.5
 * WC tested up to: 4.9
 * Requires PHP: 5.6
 *
 * Copyright 2017 WP Desk Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package Flexible Shipping Pro
 */

/* THIS VARIABLE CAN BE CHANGED AUTOMATICALLY */
$plugin_version = '2.0.3';

$plugin_name        = 'WooCommerce Flexible Shipping PRO';
$plugin_class_name  = 'WPDesk_Flexible_Shipping_Pro_Plugin';
$plugin_text_domain = 'flexible-shipping-pro';
$product_id         = 'WooCommerce Flexible Shipping PRO';
$plugin_file        = __FILE__;
$plugin_dir         = dirname( __FILE__ );

define( 'FLEXIBLE_SHIPPING_PRO_VERSION', $plugin_version );
define( $plugin_class_name, $plugin_version );

$requirements = array(
	'php'          => '5.6',
	'wp'           => '4.5',
	'repo_plugins' => array(
		array(
			'name'      => 'woocommerce/woocommerce.php',
			'nice_name' => 'WooCommerce',
			'version'   => '3.8',
		),
		array(
			'name'      => 'flexible-shipping/flexible-shipping.php',
			'nice_name' => 'Flexible Shipping',
			'version'   => '2.1',
		),
	),
);

require __DIR__ . '/vendor_prefixed/wpdesk/wp-plugin-flow/src/plugin-init-php52.php';
