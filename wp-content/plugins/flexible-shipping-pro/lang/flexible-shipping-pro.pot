msgid ""
msgstr ""
"Project-Id-Version: Flexible Shipping PRO\n"
"Report-Msgid-Bugs-To: \n"
"Last-Translator: Grzegorz Rola <grola@wpdesk.pl>\n"
"Language-Team: Maciej Swoboda <maciej.swoboda@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2020-12-30T12:22:07+00:00\n"
"PO-Revision-Date: 2020-12-30T12:22:07+00:00\n"
"Language: \n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-WPHeader: flexible-shipping-pro.php\n"

#: /builds/wpdesk/flexible-shipping-pro/classes/woocommerce-form-field.php:39
msgid "required"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-helper/src/Integration/LogsIntegration.php:60
#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-helper/src/Integration/TrackerIntegration.php:71
msgid "Enable"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-helper/src/Page/SettingsPage.php:58
msgid "WP Desk Helper Settings"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-license/src/ApiManager/class-wc-api-manager-menu.php:309
msgid "Deactivates an API Key so it can be used on another blog."
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-license/src/Page/License/views/license-actions.php:14
msgid "Key:"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-license/src/Page/License/views/license-actions.php:25
msgid "Email:"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-license/src/Page/License/views/license-actions.php:41
msgid "Activate"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-license/src/Page/License/views/license-actions.php:47
msgid "Deactivate"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-license/src/Page/License/views/licenses.php:27
msgid "WP Desk Subscriptions"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:18
msgid "Please help us improve our plugins! If you opt-in, we will collect some non-sensitive data and usage information anonymously. If you skip this, that's okay! All plugins will work just fine."
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:27
msgid "Allow & Continue &rarr;"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:32
msgid "Skip"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:39
msgid "What permissions are being granted?"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:48
msgid "Your Site Overview"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:51
msgid "WP version, PHP info"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:59
msgid "Plugin Usage"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:62
msgid "Current settings and usage information of WP Desk plugins"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:70
msgid "Your Store Overview"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:73
msgid "Anonymized and non-sensitive store usage information"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-connect.php:83
#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-notice.php:27
msgid "Find out more &raquo;"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:19
msgid " If you have a moment, please let us know why you are deactivating plugin (anonymous feedback):"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:28
msgid "The plugin suddenly stopped working"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:38
msgid "The plugin broke my site"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:48
msgid "I found a better plugin"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:53
msgid "What's the plugin's name?"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:63
msgid "I only needed the plugin for a short period"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:73
msgid "I no longer need the plugin"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:83
msgid "It's a temporary deactivation. I'm just debugging an issue."
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:93
msgid "Other"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:98
msgid "Kindly tell us the reason so we can improve"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:107
msgid "Cancel"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:110
msgid "Skip &amp; Deactivate"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-deactivate.php:126
msgid "Submit &amp; Deactivate"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-notice.php:22
msgid "We need your help to improve <strong>WP Desk plugins</strong>, so they are more useful for you and the rest of <strong>30,000+ users</strong>. By collecting data on how you use our plugins, you will help us a lot. We will not collect any sensitive data, so you can feel safe."
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-notice.php:41
msgid "Allow"
msgstr ""

#: /builds/wpdesk/flexible-shipping-pro/vendor_prefixed/wpdesk/wp-wpdesk-tracker/src/views/tracker-opt-out-notice.php:11
msgid "You successfully opted out of collecting usage data by WP Desk. If you change your mind, you can always opt in later in the plugin's quick links."
msgstr ""
