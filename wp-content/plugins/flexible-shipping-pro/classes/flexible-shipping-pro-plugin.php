<?php
/**
 * Plugin.
 *
 * @package Flexible Shipping PRO
 */

use FSProVendor\WPDesk\FS\Compatibility\PluginCompatibility;
use FSProVendor\WPDesk\Logger\WPDeskLoggerFactory;
use FSProVendor\WPDesk\PluginBuilder\Plugin\AbstractPlugin;
use FSProVendor\WPDesk\PluginBuilder\Plugin\HookableCollection;
use FSProVendor\WPDesk\PluginBuilder\Plugin\HookableParent;
use FSProVendor\WPDesk\PluginBuilder\Plugin\TemplateLoad;
use FSProVendor\WPDesk\Beacon\BeaconPro;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use WPDesk\FSPro\TableRate\FreeShipping\FreeShippingNoticeAllowed;
use WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory\AjaxHandler;
use WPDesk\FSPro\TableRate\Rule\Condition\ProductCategory\CategoriesOptions;
use WPDesk\FSPro\TableRate\Rule\RuleConditions;
use WPDesk\FSPro\TableRate\Rule\SpecialActions;
use WPDesk\FSPro\TableRate\RuleCost\RuleAdditionalCostHooks;
use WPDesk\FSPro\TableRate\RuleSettingsConverter;
use WPDesk\FSPro\TableRate\RulesTableSettings;
use WPDesk\FSPro\TableRate\ShippingMethod\CalculatedCost;
use WPDesk\FSPro\TableRate\ShippingMethod\CalculationFunction;
use WPDesk\FSPro\TableRate\ShippingMethod\FreeShippingCalculatorCallback;
use WPDesk\FSPro\TableRate\ShippingMethod\ShippingContentsFilter;

/**
 * Plugin.
 */
class WPDesk_Flexible_Shipping_Pro_Plugin extends AbstractPlugin implements HookableCollection {

	use HookableParent;
	use TemplateLoad;

	const HOOK_PRIORITY_AFTER_DEFAULT = 11;

	/**
	 * Logger.
	 *
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * WPDesk_Flexible_Shipping_Pro_Plugin constructor.
	 *
	 * @param FSProVendor\WPDesk_Plugin_Info $plugin_info Plugin info.
	 */
	public function __construct( FSProVendor\WPDesk_Plugin_Info $plugin_info ) {
		$this->plugin_info = $plugin_info;
		parent::__construct( $this->plugin_info );
		$this->init_logger();
	}

	/**
	 * Initialize $this->logger
	 */
	private function init_logger() {
		if ( class_exists( 'WPDesk_Flexible_Shipping_Logger_Settings' ) ) {
			$logger_settings = new WPDesk_Flexible_Shipping_Logger_Settings();
			if ( $logger_settings->is_enabled() ) {
				$this->logger = ( new WPDeskLoggerFactory() )->createWPDeskLogger( $logger_settings->get_logger_channel_name() );
			}
		}

		$this->logger = new NullLogger();
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		parent::hooks();

		$this->add_hookable( new PluginCompatibility() );

		add_action( 'plugins_loaded', array( $this, 'init_flexible_shipping' ), self::HOOK_PRIORITY_AFTER_DEFAULT );
		$this->hooks_on_hookable_objects();
	}

	/**
	 * Init base variables for plugin
	 */
	public function init_base_variables() {
		$this->plugin_url = $this->plugin_info->get_plugin_url();

		$this->plugin_path   = $this->plugin_info->get_plugin_dir();
		$this->template_path = $this->plugin_info->get_text_domain();

		$this->plugin_text_domain = $this->plugin_info->get_text_domain();
		$this->plugin_namespace   = $this->plugin_info->get_text_domain();

	}

	/**
	 * .
	 */
	public function init_flexible_shipping() {
		$fs = new WPDesk_Flexible_Shipping_Pro_FS_Hooks();

		$woocommerce_form_fields = new WPDesk_Flexible_Shipping_Pro_Woocommerce_Form_Field();
		$woocommerce_form_fields->hooks();

		if ( 'pl_PL' !== get_locale() ) {
			$beacon = new BeaconPro(
				'2321116f-e474-45a7-b04d-0950420ff894',
				new WPDesk_Flexible_Shipping_Pro_Plugin_Beacon_Should_Show_Strategy(),
				$this->get_plugin_url() . 'vendor_prefixed/wpdesk/wp-helpscout-beacon/assets/'
			);
			$beacon->hooks();
		}

		$rule_hooks = new RuleAdditionalCostHooks();
		$rule_hooks->hooks();

		if ( class_exists( 'WPDesk\FS\TableRate\Rule\Condition\AbstractCondition' ) ) {
			$categories_options = new CategoriesOptions();

			$rule_conditions = new RuleConditions( $categories_options );
			$rule_conditions->hooks();

			$rule_conditions = new AjaxHandler( $categories_options );
			$rule_conditions->hooks();
		}

		$special_actions = new SpecialActions();
		$special_actions->hooks();

		$free_shipping_allowed = new FreeShippingNoticeAllowed();
		$free_shipping_allowed->hooks();

		$rule_settings_hooks = new RuleSettingsConverter();
		$rule_settings_hooks->hooks();

		$default_rule_settings = new \WPDesk\FSPro\TableRate\DefaultRulesSettings();
		$default_rule_settings->hooks();

		$rules_table_settings = new RulesTableSettings();
		$rules_table_settings->hooks();

		$shipping_contents = new ShippingContentsFilter();
		$shipping_contents->hooks();

		$calculation_function = new CalculationFunction();
		$calculation_function->hooks();

		$calculated_cost = new CalculatedCost();
		$calculated_cost->hooks();

		$free_shipping_calculator = new FreeShippingCalculatorCallback();
		$free_shipping_calculator->hooks();
	}

	/**
	 * .
	 *
	 * @param mixed $links .
	 *
	 * @return array
	 */
	public function links_filter( $links ) {
		$docs_link    = get_locale() === 'pl_PL' ? 'https://www.wpdesk.pl/docs/flexible-shipping-pro-woocommerce-docs/' : 'https://docs.flexibleshipping.com/collection/20-fs-table-rate/';
		$support_link = get_locale() === 'pl_PL' ? 'https://www.wpdesk.pl/support/' : 'https://flexibleshipping.com/support/';

		$plugin_links = array(
			'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=shipping&section=flexible_shipping_info' ) . '">' . __( 'Settings', 'flexible-shipping-pro' ) . '</a>',
			'<a target="_blank" href="' . $docs_link . '">' . __( 'Docs', 'flexible-shipping-pro' ) . '</a>',
			'<a target="_blank" href="' . $support_link . '">' . __( 'Support', 'flexible-shipping-pro' ) . '</a>',
		);

		return array_merge( $plugin_links, $links );
	}
}
