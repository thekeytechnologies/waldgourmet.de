<?php

namespace FSProVendor\WPDesk\Composer\Codeception;

use FSProVendor\WPDesk\Composer\Codeception\Commands\CreateCodeceptionTests;
use FSProVendor\WPDesk\Composer\Codeception\Commands\RunCodeceptionTests;
/**
 * Links plugin commands handlers to composer.
 */
class CommandProvider implements \FSProVendor\Composer\Plugin\Capability\CommandProvider
{
    public function getCommands()
    {
        return [new \FSProVendor\WPDesk\Composer\Codeception\Commands\CreateCodeceptionTests(), new \FSProVendor\WPDesk\Composer\Codeception\Commands\RunCodeceptionTests()];
    }
}
