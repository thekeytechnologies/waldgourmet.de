=== Schema App WooCommerce JSON-LD ===
Contributors: vberkel
Plugin Name: Schema App WooCommerce
Tags: schema, structured data, schema.org, rich snippets, woocommerce, products, json-ld
Author URI: https://www.hunchmanifest.com
Author: Mark van Berkel (vberkel)
Requires at least: 3.7
Tested up to: 5.6
Stable tag: 1.5.11
License: GPL2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The [Schema App WooCommerce](http://www.schemaapp.com/product/schema-app-woocommerce/) plugin adds schema.org structured data to all WooCommerce products. Schema.org/Product Markup for all your Product data.
Supports Rich Snippets. Fixes and Extends WooCommerce markup. Includes 1 year upgrades & support.

== Description ==
Get fully detailed schema.org structured data markup for your products. 

== Installation ==

Installation is straightforward

1. Upload schema-app-woocommerce to the /wp-content/plugins/ directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.`

== Frequently Asked Questions ==
You\'ll find the [FAQ on SchemaApp.com] (http://www.schemaapp.com/product/schema-app-woocommerce/).

== Screenshots ==
1. Schema App Tools Admin

== Changelog ==

= 1.5.11 = 
- Improve, Optimize Category pages in WooCommerce

= 1.5.10 = 
- Improve, Additional GTIN support from "WooCommerce Product Feed Pro" plugin

= 1.5.9 = 
- Fix, Updated BreadcrumbList markup based on Google's schema
- Fix, Plugin adhere to parent WP Plugin's breadcrumb settings
- Fix, Updated all Schema.org context to https

= 1.5.8 = 
- Fix, isRelatedTo and isSimilarTo non existing product attribute error
- Improve, added setting to enable/disable isRelatedTo and isSimilarTo markup on product detail page

= 1.5.7 = 
- Improve, isRelatedTo and isSimilarTo product schema to include necessary markup like sku, gtin, offers, review, rating and brand

= 1.5.6 = 
- Feature, Add support for GTIN via "WooCommerce UPC, EAN, and ISBN" plugin

= 1.5.5 = 
- Improve, Migrate License Web Service

= 1.5.4 = 
- Improve, added setting to select short or full description for generating description markup

= 1.5.3 = 
- Fix, added URL to offer schema
- Fix, Change itemCondition to full URL

= 1.5.2 = 
- Fix, PHP error in previous release

= 1.5.1 = 
- Fix, Shortcode showing in Product description

= 1.5.0 = 
- Feature, add schema.org/BreadcrumbList to Products

= 1.4.0 = 
- Feature, Both Price & Sale Price now included
- Fix, Min Max Price Warnings when no variant price available

= 1.3.1 = 
- Fix, Add worstRating property for Aggregate for Search Console report

= 1.3.0 =
- Feature, OfferCatalog for Taxonomy Archive pages
- Improvement, move schema data to Footer (vs Header) to improve page rendering speed

= 1.2.0 = 
- Feature, OfferCatalog add Offer price or AggregateOffer for variables
- Feature, Product Offers get names, useful for multiple variants
- Feature, Product Brands, attribute mapped by name, added support for WooCommerce Brand plugin
- Fix, OfferCatalog ItemList, seller improvement, images reuse @id.
- Fix, conflict with Divi theme override of WC Shop page

= 1.1.1 =
- Fix, check version of WC but is not yet defined

= 1.1.0 = 
- Feature, OfferCatalog add aggregateRating
- Fix, SDTT error when Ratings were missing
- Fix, Output Seller markup once, repeat with @id
- Fix, OfferCatalog numberOfItems for all posts, not shown

= 1.0.2 = 
- Fix, error in PHP7
- Fix, license key activation

= 1.0.1 = 
- Fix, location of WooCommerce v3 Markup disabling hook

= 1.0.0 = 
- Feature, Add http://schema.org/OfferCatalog for Product Category pages
- Feature, Upgrade support of WooCommerce v3
- Feature, Add support for 'Testimonials by WooThemes'
- Feature, configuration option for itemCondition
- Feature, Add Product @id, seller
- Fix, improve plugin updater
- Fix, separate markup for Product multiple categories
- Documentation, slight improvements for clarification

= 0.3.0 = 
- Feature, Add filter option for customizing product markup

= 0.2.3 =
- Fix, Check license for updates
- Fix, Ipdate to woocommerce v2.6 templates
- Fix; itemCondition overrides
- Fix; Attribute values from taxonomy

= 0.2.2 = 
- Fix; Multi-variable product IDs

= 0.2.1 = 
- Fix; Error for product types that are not simple nor variable

= 0.2.0 =
- Documentation; Added a simple information page, Settings --> Schema App WC
- Feature; Add schema.org/offer for each WC product variation
- Feature; Add schema.org/itemCondition
- Fix; description strips out html tags
- Fix; image property

= 0.1.0 =
- First version 