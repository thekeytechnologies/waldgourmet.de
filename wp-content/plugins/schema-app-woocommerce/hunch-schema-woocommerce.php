<?php defined('ABSPATH') OR die('This script cannot be accessed directly.');
/*
 * Plugin Name: Schema App WooCommerce
 * Plugin URI: http://www.schemaapp.com
 * Description: This plugin adds http://schema.org structured data using JSON-LD to woocommerce products
 * Version: 1.5.11
 * Author: Hunch Manifest
 * Author URI: https://www.hunchmanifest.com
 * License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * WC requires at least: 2.4
 * WC tested up to: 4
 */

////
// Attribute class for assigning WooCommerce Attributes to Schema Properties
//
class Attribute
{
    private $_attributes; 
    private $_product; 
    
    public function __construct($product) {
        $this->_attributes = $product->get_attributes();
        $this->_product = $product;
    }
    
    // Find attribute for property
    public function assignProperty(&$schemaObj, $needle) {
        foreach ( $this->_attributes as $key => $attrib ) {
            if ( stristr($key, $needle)) {
                $schemaObj[$needle] = $this->_product->get_attribute( $key );
                unset($this->_attributes[$key]);
                return;
            }
        }
    }
    
    // Map remaining PropertyValue
    public function assignRemaining(&$schemaObj) {
        $prop = array();
        foreach ( $this->_attributes as $key => $attrib ) {
            if ( $attrib['is_visible'] === 1 && $attrib['is_variation'] === 0 ) {
                $prop[] = array(
                    '@type' => 'PropertyValue',
                    'name' => $key,
                    'value' => $this->_product->get_attribute( $key )
                );
            }
        }
        if ( count($prop) > 0 ) {
            $schemaObj['additionalProperty'] = $prop;
        }
    }
    
}

// Static information from v0.2
if (is_admin()) {

	$SchemaAppLicense = get_option( 'schema_option_name_license' );

    if ( $SchemaAppLicense['schema_license_wc_status'] == 'Active' )
    {
		add_filter( 'pre_set_site_transient_update_plugins', 'HunchSchemaWCSiteTransientUpdatePlugins' );
		add_filter( 'plugins_api', 'HunchSchemaWCPluginsApi', 10, 3 );
    }


	function HunchSchemaWCSiteTransientUpdatePlugins( $Transient )
	{
		if ( $Transient )
		{
			$PluginData = get_plugin_data( __FILE__ );

			$Response = wp_remote_retrieve_body( wp_remote_post( 'https://app.schemaapp.com/assets/scripts/schema-app-woocommerce-plugin-api.php', array( 'body' => array( 'Action' => 'Version', 'URL' => site_url() ) ) ) );
			$Response = $Response ? unserialize ( $Response ) : $Response;

			if ( is_object( $Response ) && version_compare( $PluginData['Version'], $Response->new_version, '<' ) )
			{
				$Transient->response[ plugin_basename( __FILE__ ) ] = $Response;
			}
		}

		return $Transient;
	}


	function HunchSchemaWCPluginsApi( $False, $Action, $Args )
	{
		if ( $Args->slug == basename( __FILE__, '.php' ) && $Action == 'plugin_information' )
		{
			$Response = wp_remote_retrieve_body( wp_remote_post( 'https://app.schemaapp.com/assets/scripts/schema-app-woocommerce-plugin-api.php', array( 'body' => array( 'Action' => 'Info', 'URL' => site_url() ) ) ) );
			$Response = $Response ? unserialize ( $Response ) : $Response;

			if ( is_object( $Response ) )
			{
				return $Response;
			}
			else
			{
				return new WP_Error( 'plugin_api_failed', 'An unexpected error occurred. Please <a href="#" onclick="document.location.reload(); return false;">try again</a>.' );
			}
		}

		return $False;
	}


	add_action( 'wp_scheduled_delete', 'HunchSchemaCronLicenseCheck', 100 );

	function HunchSchemaCronLicenseCheck()
	{
		global $SchemaAppLicense;

		if ( ! empty( $SchemaAppLicense['schema_license_wc'] ) )
		{
			$SchemaServer = new SchemaServer();

			$Response = $SchemaServer->activateLicense( array( 'license' => $SchemaAppLicense['schema_license_wc'], 'siteToAdd' => site_url() ) );

			if ( $Response[0] == true )
			{
				$SchemaAppLicense['schema_license_wc_status'] = 'Active';
			}
			else
			{
				$SchemaAppLicense['schema_license_wc_status'] = 'Inactive';
			}

			update_option( 'schema_option_name_license', $SchemaAppLicense );
		}
	}


	add_filter( 'site_transient_update_plugins', 'HunchSchemaSiteTransientUpdatePlugins' );

	function HunchSchemaSiteTransientUpdatePlugins( $Data )
	{
		global $SchemaAppLicense;

		if ( class_exists( 'WooCommerce' ) && $SchemaAppLicense['schema_license_wc_status'] != 'Active' && ! empty( $Data ) && isset( $Data->response[ plugin_basename( __FILE__ ) ] ) )
		{
			unset( $Data->response[ plugin_basename( __FILE__ ) ] );
		}

		return $Data;
	}


	add_action( 'admin_notices', 'HunchSchemaWCAdminNotices' );

	function HunchSchemaWCAdminNotices()
	{
		global $SchemaAppLicense;

		if ( class_exists( 'WooCommerce' ) )
		{
			if ( ! class_exists( 'SchemaSettings' ) )
			{
				print '<div class="error"><p><strong>Schema App WooCommerce:</strong> Please install the <a href="https://wordpress.org/plugins/schema-app-structured-data-for-schemaorg/">Schema App Structured Data</a> Plugin.</p></div>';
			}
			elseif ( $SchemaAppLicense['schema_license_wc_status'] != 'Active' )
			{
				print '<div class="error"><p><strong>Schema App WooCommerce:</strong> Your license is not active, please <a href="options-general.php?page=schema-app-setting">update settings</a>.</p></div>';
			}

			if ( version_compare( WC()->version, '2.4', '<' ) )
			{
				print '<div class="error"><p><strong>Schema App WooCommerce:</strong> We do not support legacy version of WooCommerce, please upgrade to version 2.4.0 or above.</p></div>';
			}
		}
	}


    // create custom plugin settings menu
    add_action('admin_menu', 'hunch_schema_wc_create_menu');

    function hunch_schema_wc_create_menu() {
        // This page will be under "Settings"
        add_options_page(
            'Schema App WooCommerce Settings', 
            'Schema App WC', 
            'manage_options', 
            'schema-app-wc-setting', 
            'hunch_schema_wc_settings_page'
        );
        // Register settings function
        add_action( 'admin_init', 'register_hunch_schema_wc_settings' );
    }


    function register_hunch_schema_wc_settings()
    {
		register_setting( 'schema_wc', 'schema_wc', 'HunchSchemaWCSettingsSanitize' );

		add_settings_section( 'wc-setting', '', 'HunchSchemaWCSettingsSectionRender', 'schema-app-wc-setting' );

		add_settings_field( 'itemCondition', 'itemCondition', 'HunchSchemaWCSettingsFielditemCondition', 'schema-app-wc-setting', 'wc-setting' );      
		add_settings_field( 'V3DefaultMarkup', 'Default Markup', 'HunchSchemaWCSettingsFieldV3DefaultMarkup', 'schema-app-wc-setting', 'wc-setting' );      
		add_settings_field( 'description', 'Generate description from', 'HunchSchemaWCSettingsFieldDescription', 'schema-app-wc-setting', 'wc-setting' );      
		add_settings_field( 'RelatedProductMarkup', 'Related/similar product markup', 'HunchSchemaWCSettingsFieldRelatedProductMarkup', 'schema-app-wc-setting', 'wc-setting' );      
		add_settings_field( 'CategorySKU', 'SKU on category pages', 'HunchSchemaWCSettingsFieldCategorySKU', 'schema-app-wc-setting', 'wc-setting' );      
    }

    function HunchSchemaWCSettingsSanitize( $input )
    {
        $new_input = array();
        
        if ( ! empty( $input['itemCondition'] ) )
        {
			$new_input['itemCondition'] = sanitize_text_field( $input['itemCondition'] );
		}

        if ( ! empty( $input['V3DefaultMarkup'] ) )
        {
			$new_input['V3DefaultMarkup'] = sanitize_text_field( $input['V3DefaultMarkup'] );
		}

        if ( ! empty( $input['description'] ) )
        {
			$new_input['description'] = sanitize_text_field( $input['description'] );
		}

        if ( ! empty( $input['RelatedProductMarkup'] ) )
        {
			$new_input['RelatedProductMarkup'] = sanitize_text_field( $input['RelatedProductMarkup'] );
		}

        if ( ! empty( $input['CategorySKU'] ) )
        {
			$new_input['CategorySKU'] = sanitize_text_field( $input['CategorySKU'] );
		}

        return $new_input;
    }

	function HunchSchemaWCSettingsSectionRender()
	{
		print '<h2>Settings</h2>';
	}

	function HunchSchemaWCSettingsFielditemCondition()
	{
		$Settings = get_option( 'schema_wc' );
		$Value = ! empty( $Settings['itemCondition'] ) ? $Settings['itemCondition'] : 'NewCondition';
		$Items = array( 'DamagedCondition', 'NewCondition', 'RefurbishedCondition', 'UsedCondition' );

		print '<select name="schema_wc[itemCondition]">';

		foreach ( $Items as $Item )
		{
			printf( '<option value="%s" %s>%s</option>', $Item, ( $Value == $Item ? 'selected' : '' ), $Item );
		}

		print '</select> <p class="description">Set default condition for products.</p>';
	}

	function HunchSchemaWCSettingsFieldV3DefaultMarkup()
	{
		$Settings = get_option( 'schema_wc' );
		$Value = isset( $Settings['V3DefaultMarkup'] ) ? $Settings['V3DefaultMarkup'] : '0';
		$Items = array( 1 => 'Enabled', 0 => 'Disabled' );

		print '<select name="schema_wc[V3DefaultMarkup]">';

		foreach ( $Items as $ItemId => $ItemLabel )
		{
			printf( '<option value="%s" %s>%s</option>', $ItemId, selected( $Value, $ItemId, false ), $ItemLabel );
		}

		print '</select> <p class="description">Default WooCommerce 3 JSON/LD structured data format.</p>';
	}

	function HunchSchemaWCSettingsFieldDescription()
	{
		$Settings = get_option( 'schema_wc' );
		$Value = isset( $Settings['description'] ) ? $Settings['description'] : 'short';
		$Items = array( 'short' => 'Short description', 'full' => 'Full description' );

		print '<select name="schema_wc[description]">';

		foreach ( $Items as $ItemId => $ItemLabel )
		{
			printf( '<option value="%s" %s>%s</option>', $ItemId, selected( $Value, $ItemId, false ), $ItemLabel );
		}

		print '</select> <p class="description">Select source for generating description.</p>';
	}

	function HunchSchemaWCSettingsFieldRelatedProductMarkup()
	{
		$Settings = get_option( 'schema_wc' );
		$Value = isset( $Settings['RelatedProductMarkup'] ) ? $Settings['RelatedProductMarkup'] : '0';
		$Items = array( 1 => 'Enabled', 0 => 'Disabled' );

		print '<select name="schema_wc[RelatedProductMarkup]">';

		foreach ( $Items as $ItemId => $ItemLabel )
		{
			printf( '<option value="%s" %s>%s</option>', $ItemId, selected( $Value, $ItemId, false ), $ItemLabel );
		}

		print '</select> <p class="description">On Product detail pages, add isRelatedTo and isSimilarTo products\' markup.</p>';
	}

	function HunchSchemaWCSettingsFieldCategorySKU()
	{
		$Settings = get_option( 'schema_wc' );
		$Value = isset( $Settings['CategorySKU'] ) ? $Settings['CategorySKU'] : '0';
		$Items = array( 1 => 'Enabled', 0 => 'Disabled' );

		print '<select name="schema_wc[CategorySKU]">';

		foreach ( $Items as $ItemId => $ItemLabel )
		{
			printf( '<option value="%s" %s>%s</option>', $ItemId, selected( $Value, $ItemId, false ), $ItemLabel );
		}

		print '</select> <p class="description">On Product detail pages, add isRelatedTo and isSimilarTo products\' markup.</p>';
	}

    function hunch_schema_wc_settings_page() {
    ?>
    <div class="wrap">
        <h2>Schema App WooCommerce</h2>
        <h2>Description</h2>
        <p>This plugin adds <a href='http://schema.org/Product'>Schema.org/Product</a> Markup for all your Product data. 
            It enables Product and Review Rich Snippets, while fixing then significantly extends the default WooCommerce markup. The 
            plugin also provides the necessary Schema Markup for Google Merchant Shopping Feed. For more information about the benefits see the <a href='https://www.schemaapp.com/product/schema-app-woocommerce/'>product 
            description page</a> and test a product page using the <a href='https://developers.google.com/structured-data/testing-tool/'>Google Structured Data Testing Tool</a></p>
        <h3>Setup & Support</h3>
        <p>If you're reading this, your markup is active and working. As part of your purchase we provide 12 months of upgrades and support. To get plugin upgrades you must activate the plugin with the license key.</p>
        <ol>
            <li>You will find license key on <a href="http://app.schemaapp.com/licenses" target="_blank">Schema App License</a> page.&nbsp;Login credentials to Schema App should have been received from notifications@hunchmanifest.com.</li>
            <li>With the key, you must activate with the sister plugin, Schema App, a free plugin in the WordPress repository. Add Schema App in a few steps:
                <ol>
                    <li>Login to your WP Admin, Go to ⇒ Plugins ⇒ Add New</li>
                    <li>Search for Schema App (by Hunch Manifest)</li>
                    <li>Install Schema App plugin</li>
                    <li>Activate plugin</li>
                    <li>Add your Account ID (received in the registration email from Schema App and in <a href='http://app.schemaapp.com/wordpress'>http://app.schemaapp.com/wordpress</a>)</li>
                </ol>
            <li>Enter the License in Wordpress Admin by navigating to Settings -> Schema App. The License tab allows you to Activate for the current website.</li>
        </ol>
        <p>If you have any questions about the plugin email <a href='mailto:support@hunchmanifest.com'>support@hunchmanifest.com</a></p>

		<form method="post" action="options.php">
		<?php

			settings_fields( 'schema_wc' );   
			do_settings_sections( 'schema-app-wc-setting' ); 
			submit_button(); 

		?>
		</form>

    </div>
    <?php }
    return;
    
}


add_action( 'plugins_loaded', 'hunch_schema_wc_hook_plugins_loaded' );

function hunch_schema_wc_hook_plugins_loaded()
{
	if ( class_exists( 'WooCommerce' ) && version_compare( WC()->version, '2.4', '>=' ) )
	{
		add_action( 'init', 'HunchSchemaWCInit' );
		// Add WooCommerce Schema.org JSON-LD
		add_action('wp_footer', 'hunch_schema_wc_add');
	}
}


function HunchSchemaWCInit()
{
	$Settings = get_option( 'schema_wc' );

	if ( version_compare( WC()->version, '3', '>=' ) && empty( $Settings['V3DefaultMarkup'] ) )
	{
		remove_action( 'wp_footer', array( WC()->structured_data, 'output_structured_data' ), 10 );
		remove_action( 'woocommerce_email_order_details', array( WC()->structured_data, 'output_email_structured_data' ), 30 );
	}
}


// WooCommerce Schema.org JSON-LD
function hunch_schema_wc_add()
{
	global $post;

	$QueriedObject = get_queried_object();

	if ( is_product() )
	{
		HunchSchemaWCMarkupProduct();
	}
	elseif ( is_product_category() || is_product_tag() || is_shop() || ( is_tax() && taxonomy_is_product_attribute( $QueriedObject->taxonomy ) ) )
	{
		HunchSchemaWCMarkupProductCategory();
	}
}


function HunchSchemaWCMarkupProduct()
{
    global $post, $woothemes_testimonials;

	$Settings = get_option( 'schema_wc' );
	$settings_schema_app = get_option( 'schema_option_name' );
    $product = wc_get_product(get_the_ID());
    $attributes = new Attribute($product);

    // Downloadable product schema handling
    $productClass = "Product";
    if ($product->is_downloadable()) {
        switch ($product->download_type) {
            case 'application' :
                $productClass = "SoftwareApplication";
                break;
            case 'music' :
                $productClass = "MusicAlbum";
                break;
            default :
                $productClass = "Product";
                break;
        }
    }

    // Compile the base product data
    $productSchema = array(
        '@context' => 'https://schema.org',
        '@type' => $productClass,
		'@id' => get_permalink() . '#' . $productClass,
        'name' => $product->get_title(),
		'description' => ( $Settings['description'] == 'full' ) ? wp_strip_all_tags( apply_filters( 'the_excerpt', $post->post_content ), true ) : wp_strip_all_tags( apply_filters( 'the_excerpt', $post->post_excerpt ), true ),
        'url' => get_permalink(),
    );

    // Product Stock Keeping Unit
    if ($product->get_sku()) {
        $productSchema['sku'] = $product->get_sku();
    }

    // Product Categories
	$categories = version_compare( WC()->version, '3', '>=' ) ? wc_get_product_category_list( get_the_ID() ) : $product->get_categories();
    if ($categories ) {
        $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
        if (preg_match_all("/$regexp/siU", $categories, $matches, PREG_SET_ORDER)) {
            foreach($matches as $match) {
				$productSchema['category'][] = $match[3];
            }
        }
    }

	if ( $Settings['RelatedProductMarkup'] ) {
		// Related Products http://schema.org/isRelatedTo
		$related_product_ids = version_compare( WC()->version, '3', '>=' ) ? wc_get_related_products( get_the_ID() ) : $product->get_related(5);
		$productSchema['isRelatedTo'] = hunch_schema_wc_schema_related_products( $related_product_ids );

		// http://schema.org/isSimilarTo
		$similar_product_ids = version_compare( WC()->version, '3', '>=' ) ? $product->get_upsell_ids() : $product->get_upsells();
		$productSchema['isSimilarTo'] = hunch_schema_wc_schema_related_products( $similar_product_ids );
	}
    
    // Product Dimensions
    if ($product->has_dimensions()) {
        // "INH" for inches and 
        switch (get_option('woocommerce_dimension_unit')) {
            case "in":
                $unitCode = "INH";
                break;
            case "m":
                $unitCode = "MTR";
                break;
            case "cm":
                $unitCode = "CMT";
                break;
            case "mm":
                $unitCode = "MMT";
                break;
            case "yd":
                $unitCode = "YRD";
                break;
            default:
                break;
        }        
        $productSchema['height'] = array(
            "@type" => "QuantitativeValue",
            'unitCode' => $unitCode,
            'value' => $product->get_height()
        );
        $productSchema['width'] = array(
            "@type" => "QuantitativeValue",
            'unitCode' => $unitCode,
            'value' => $product->get_width()
        );
        $productSchema['depth'] = array(
            "@type" => "QuantitativeValue",
            'unitCode' => $unitCode,
            'value' => $product->get_length()
        );
    }
    if ($product->has_weight()) {
        // KGM for kilogram, LBR for pound
        $unitCode = get_option('woocommerce_weight_unit') == "lbs" ? "LBR" : "KGM";
        switch (get_option('woocommerce_weight_unit')) {
            case "lbs":
                $unitCode = "LBR";
                break;
            case "kg":
                $unitCode = "KGM";
                break;
            case "g":
                $unitCode = "LBR";
                break;
            case "oz":
                $unitCode = "LBR";
                break;
            default:
                break;
        }
        $productSchema['weight'] = array(
            '@type' => 'QuantitativeValue',
            'unitCode' => $unitCode,
            'value' => $product->get_weight(),
        );
    }

    // GTIN numbers need product attributes
    $attributes->assignProperty($productSchema, 'gtin8');
    $attributes->assignProperty($productSchema, 'gtin12');
    $attributes->assignProperty($productSchema, 'gtin13');
    $attributes->assignProperty($productSchema, 'gtin14');    
    // Attribute Color
    $attributes->assignProperty($productSchema, 'color');    
    // Attribute Brand
    $attributes->assignProperty($productSchema, 'brand');


	// Add GTIN
	$productSchema = hunch_schema_wc_schema_gtin( $productSchema, $product );


    // Image (Primary image + Gallery)
    if ($product->get_image_id()) {
        $wpimage = wp_get_attachment_image_src($product->get_image_id(), 'single-post-thumbnail');
        $images[] = array(
            "@type" => "ImageObject",
            "url" => $wpimage[0],
            "height" => $wpimage[2],
            "width" => $wpimage[1]
        );
		$gallery = version_compare( WC()->version, '3', '>=' ) ? $product->get_gallery_image_ids() : $product->get_gallery_attachment_ids();
        foreach($gallery AS $index => $imageid) {
            $wpimage = wp_get_attachment_image_src($imageid, 'single-post-thumbnail');
            $images[] = array(
                "@type" => "ImageObject",
                "url" => $wpimage[0],
                "height" => $wpimage[2],
                "width" => $wpimage[1]
            );
        }
        if ( count($images) > 0 ) {
            $productSchema['image'] = count( $images ) == 1 ? $images[0] : $images;
        }
    }
        
    // Offers
	$productSchema['offers'] = hunch_schema_wc_schema_offer( $product, $attributes );

	// Ratings
	if ( $product->get_rating_count() > 0 ) {
		// Aggregate Rating
		$productSchema['aggregateRating'] = hunch_schema_wc_schema_rating( $product );

		// Reviews
		$productSchema['review'] = hunch_schema_wc_schema_review( $product );
	}
    
	// Testimonials
	if ( class_exists( 'Woothemes_Testimonials' ) )
	{
		$Testimonials = $woothemes_testimonials->get_testimonials( $args );

		if ( $Testimonials && count( $Testimonials ) )
		{
			foreach ( $Testimonials as $Testimonial )
			{
				$productSchema['review'][] = array
				(
					'@type' => 'Review',
					'@id' => $Testimonial->guid,
					'description' => wp_strip_all_tags( strip_shortcodes( $Testimonial->post_content ) ),
					'datePublished' => $Testimonial->post_date_gmt,
					'author' => array
					(
						'@type' => 'Person',
						'name' => $Testimonial->byline,
						'url' => $Testimonial->url,
					)
				);
			}
		}
	}

	// Brands
	if ( function_exists( 'get_brands' ) && taxonomy_exists( 'product_brand' ) ) {
		$productSchema['brand'] = hunch_schema_wc_schema_brand( $product );
	}

    // Map remaining attributes to additionalProperty PropertyValue
    $attributes->assignRemaining($productSchema);

    // Apply Filter for customized markup
    $productSchema = apply_filters('hunch_schema_woocommerce_productschema', $productSchema);
    
	printf( '<!-- Schema App WC --><script type="application/ld+json" data-schema="product-Default">%s</script><!-- Schema App WC -->' . "\n", json_encode( $productSchema ) );


	if ( ! empty( $settings_schema_app['SchemaBreadcrumb'] ) ) {
		$schema_breadcrumb_position = 1;
		$schema_breadcrumb = array();
		$schema_breadcrumb['@context'] = 'https://schema.org';
		$schema_breadcrumb['@type'] = 'BreadcrumbList';

		$product_categories = get_the_terms( get_the_ID(), 'product_cat' );

		if ( $product_categories && ! is_wp_error( $product_categories ) ) {
			foreach( $product_categories as $product_category ) {
				$schema_breadcrumb['itemListElement'][] = array(
					'@type' => 'ListItem',
					'position' => $schema_breadcrumb_position++,
					'name' => $product_category->name,
					'item' => get_term_link( $product_category, 'product_cat' ) . '#breadcrumbitem',
				);
			}
		}

		$schema_breadcrumb['itemListElement'][] = array(
			'@type' => 'ListItem',
			'position' => $schema_breadcrumb_position++,
			'name' => get_the_title(),
			'item' => get_permalink() . '#breadcrumbitem',
		);

		$schema_breadcrumb = apply_filters( 'hunch_schema_woocommerce_breadcrumb', $schema_breadcrumb );

		printf( '<!-- Schema App WC Breadcrumb --><script type="application/ld+json">%s</script><!-- Schema App WC Breadcrumb -->' . "\n", json_encode( $schema_breadcrumb ) );
	}
}


function HunchSchemaWCMarkupProductCategory()
{
	global $post, $wp_query;

	$Items = array();
	$Count = 0;
	$CategoryPermalink = '';
	$post_type = '';

	if ( is_product_category() )
	{
		$post_type = 'product_cat';
		$CategoryPermalink = get_term_link( get_query_var( 'product_cat' ), 'product_cat' );
	}
	elseif ( is_product_tag() )
	{
		$post_type = 'product_tag';
		$CategoryPermalink = get_term_link( get_query_var( 'product_tag' ), 'product_tag' );
	}
	elseif ( is_shop() )
	{
		$post_type = 'product_shop';
		$CategoryPermalink = version_compare( WC()->version, '3', '>=' ) ? get_permalink( wc_get_page_id( 'shop' ) ) : get_permalink( woocommerce_get_page_id( 'shop' ) ); 
	}


	while ( have_posts() )
	{
		the_post();

		$Product = wc_get_product( get_the_ID() );

		if ( ! $Product )
		{
			continue;
		}

		$Count++;

		$Settings = get_option( 'schema_wc' );
		$Attribute = new Attribute( $Product );
		$ProductSchema = array(
			'@type' => 'Product',
			'@id' => get_permalink(),
			'name' => get_the_title(),
			'url' => get_permalink(),
			'offers' => hunch_schema_wc_schema_offer( $Product, $Attribute, true, $Count ),
		);

		if ( $Product->get_image_id() )
		{
			$Image = wp_get_attachment_image_src( $Product->get_image_id(), 'single-post-thumbnail' );

			$ProductSchema['image'] = array
			(
				'@type' => 'ImageObject',
				'url' => $Image[0],
				'height' => $Image[2],
				'width' => $Image[1]
			);
		}

		if ( $Settings['CategorySKU'] && $Product->get_sku() ) {
			$ProductSchema['sku'] = $Product->get_sku();
		}

		if ( version_compare( WC()->version, '3', '>=' ) ) {
			$ProductSchema['aggregateRating'] = hunch_schema_wc_schema_rating( $Product );
		}

		// Brands
		if ( function_exists( 'get_brands' ) && taxonomy_exists( 'product_brand' ) ) {
			$ProductSchema['brand'] = hunch_schema_wc_schema_brand( $Product );
		} else {
			// Attribute Brand
			$Attribute->assignProperty( $ProductSchema, 'brand' );
		}

		$Items[] = array
		(
			'@type' => 'ListItem',
			'position' => $Count,
			'item' => $ProductSchema,
		);
	}


	$Schema = array
	(
		'@context' => 'https://schema.org',
		'@type' => 'OfferCatalog',
		'@id' => $CategoryPermalink,
		'name' => single_cat_title( '', false ),
		'description' => wp_strip_all_tags( strip_shortcodes( category_description() ) ),
		'url' => $CategoryPermalink,
		'itemListOrder' => 'ItemListOrderAscending',
		'numberOfItems' => $wp_query->found_posts,
		'itemListElement' => $Items,
	);


	$Schema = apply_filters( 'hunch_schema_woocommerce_productcategoryschema', $Schema );
	
	printf( '<!-- Schema App WC --><script type="application/ld+json" data-schema="%s-Default">%s</script><!-- Schema App WC -->' . "\n", $post_type, json_encode( $Schema ) );
}


function hunch_schema_wc_schema_offer( $Product, $Attribute, $InCategory = false, $Count = 0 )
{
	$Settings = get_option( 'schema_wc' );

	if ( $Product->is_type( 'variable' ) )
	{
		$variations = $Product->get_available_variations();

		if ( ! empty( $variations ) )
		{
			if ( $InCategory )
			{
				$VariationCount = 0;
				$VariationPrices = array();

				foreach( $variations as $variation )
				{
					// Offers are only for public products
					if ( $variation['variation_is_visible'] && $variation['variation_is_active'] )
					{
						$VariationCount++;

						$VariationPrices[] = $variation['display_price'];
					}
				}

				if ( count( $VariationPrices ) )
				{
					$offer = array
					(
						'@type' => 'AggregateOffer',
						'lowPrice' => min( $VariationPrices ),
						'highPrice' => max( $VariationPrices ),
						'priceCurrency' => get_woocommerce_currency(),
						'offerCount' => $VariationCount,
					);

					return $offer;
				}
				else
				{
					return;
				}
			}
			else
			{
				$offers = array();

				foreach( $variations as $variation )
				{
					// Offers are only for public products
					if ( $variation['variation_is_visible'] && $variation['variation_is_active'] )
					{
						$availablity = $variation['is_in_stock'] ? 'https://schema.org/InStock' : 'https://schema.org/OutOfStock';

						$offer = array
						(
							'@type' => 'Offer',
							'url' => $Product->get_permalink(),
							'name' => implode( ' - ', array_filter( array_map( 'trim', $variation['attributes'] ) ) ),
							'price' => $variation['display_price'],
							'priceCurrency' => get_woocommerce_currency(),
							'availability' => $availablity,
							'itemCondition' => 'https://schema.org/' . ( ! empty( $Settings['itemCondition'] ) ? $Settings['itemCondition'] : 'NewCondition' ), // Default
						);

						if ( hunch_schema_wc_schema_seller() )
						{
							if ( count( $offers ) )
							{
								$offer['seller']['@id'] = site_url();
							}
							else
							{
								$offer['seller'] = hunch_schema_wc_schema_seller();
							}
						}

						// Generate a variation ID
						if ( $variation['sku'] !== '' )
						{
							$offer['sku'] = $variation['sku'];
							$offer['@id'] = $Product->get_permalink() . '#' . $variation['sku'];
						}
						else
						{
							$attribute_values = array();

							foreach ( $variation['attributes'] as $key => $value )
							{
								if ( $value !== '' )
								{
									$attribute_values[] = substr( $key, 10 ) . '-' . filter_var( $value, FILTER_SANITIZE_URL );
								}
							}

							$offer['@id'] = $Product->get_permalink() . '#' . implode( '-', $attribute_values );
						}

						// Look for itemCondition override by variation
						foreach ( $variation['attributes'] as $key => $value )
						{
							if ( stristr( $key, 'itemCondition' ) )
							{
								$offer['itemCondition'] = $value;
							}
						}


						$product_variation = new WC_Product_Variation( $variation['variation_id'] );

						// Separate offers for sale and regular price
						if ( $product_variation->is_on_sale() ) {
							$offerregular = $offersale = $offer;


							$offerregular['@id'] .= '-regular-price';
							$offerregular['name'] .= ' Regular Price';
							$offerregular['price'] = version_compare( WC()->version, '3', '>=' ) ? wc_get_price_to_display( $Product, array( 'price' => $product_variation->get_regular_price() ) ) : $product_variation->get_regular_price();

							$offers[] = $offerregular;


							$offersale['@id'] .= '-sale-price';
							$offersale['name'] .= ' Sale Price';

							if ( $product_variation->get_date_on_sale_from() ) {
								$offersale['validFrom'] = date( 'Y-m-d', $product_variation->get_date_on_sale_from()->getTimestamp() );
							}

							if ( $product_variation->get_date_on_sale_to() ) {
								$offersale['validThrough'] = date( 'Y-m-d', $product_variation->get_date_on_sale_to()->getTimestamp() );
								$offersale['priceValidUntil'] = date( 'Y-m-d', $product_variation->get_date_on_sale_to()->getTimestamp() );
							}

							if ( hunch_schema_wc_schema_seller() )
							{
								unset( $offersale['seller'] );
								$offersale['seller']['@id'] = site_url();
							}

							$offers[] = $offersale;
						} else {
							$offers[] = $offer;
						}
					}
				}

				return $offers;
			}
		}
	}
	else
	{
		$availablity = $Product->is_in_stock() ? 'https://schema.org/InStock' : 'https://schema.org/OutOfStock';

		$offer = array
		(
			'@type' => 'Offer',
			'url' => $Product->get_permalink(),
			'price' => version_compare( WC()->version, '3', '>=' ) ? wc_get_price_to_display( $Product ) : $Product->get_display_price(),
			'priceCurrency' => get_woocommerce_currency(),
			'availability' => $availablity,
		);

		if ( hunch_schema_wc_schema_seller() )
		{
			if ( $InCategory && $Count > 1 )
			{
				$offer['seller']['@id'] = site_url();
			}
			else
			{
				$offer['seller'] = hunch_schema_wc_schema_seller();
			}
		}

		$offer['itemCondition'] = 'https://schema.org/' . ( ! empty( $Settings['itemCondition'] ) ? $Settings['itemCondition'] : 'NewCondition' ); // Default
		$Attribute->assignProperty($offer, 'itemCondition'); // Override


		// Separate offers for sale and regular price
		if ( $Product->is_on_sale() ) {
			$offers = array();
			$offerregular = $offersale = $offer;


			$offerregular['@id'] = $Product->get_permalink() . '#regular-price';
			$offerregular['name'] = 'Regular Price';
			$offerregular['price'] = version_compare( WC()->version, '3', '>=' ) ? wc_get_price_to_display( $Product, array( 'price' => $Product->get_regular_price() ) ) : $Product->get_regular_price();

			$offers[] = $offerregular;


			$offersale['@id'] = $Product->get_permalink() . '#sale-price';
			$offersale['name'] = 'Sale Price';

			if ( $Product->get_date_on_sale_from() ) {
				$offersale['validFrom'] = date( 'Y-m-d', $Product->get_date_on_sale_from()->getTimestamp() );
			}

			if ( $Product->get_date_on_sale_to() ) {
				$offersale['validThrough'] = date( 'Y-m-d', $Product->get_date_on_sale_to()->getTimestamp() );
				$offersale['priceValidUntil'] = date( 'Y-m-d', $Product->get_date_on_sale_to()->getTimestamp() );
			}

			if ( hunch_schema_wc_schema_seller() )
			{
				unset( $offersale['seller'] );
				$offersale['seller']['@id'] = site_url();
			}

			$offers[] = $offersale;


			return $offers;
		} else {
			return $offer;
		}
	}
}


function hunch_schema_wc_schema_rating( $product ) {
	if ( $product->get_rating_count() ) {
		return array(
			'@type' => 'AggregateRating',
			'ratingValue' => $product->get_average_rating(),
			'ratingCount' => (string) $product->get_rating_count(),
			'reviewCount' => $product->get_review_count(),
			'bestRating' => '5',
			'worstRating' => '1'
		);
	}
}


function hunch_schema_wc_schema_review( $product ) {
	$schema = array();

	$comments = get_comments( array(
		'post_type' => 'product',
		'post_id' => $product->get_id(),
		'status' => 'approve'
	));

	if ( $comments ) {
		foreach ( $comments as $comment ) {
			$ratingValue = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) );
			$reviewRating = $ratingValue ? array( '@type' => 'Rating', 'ratingValue' => $ratingValue ) : null;

			$schema[] = array(
				'@type' => 'Review',
				'@id' => $product->get_permalink() . '#li-comment-' . $comment->comment_ID,
				'description' => wp_strip_all_tags( strip_shortcodes( $comment->comment_content ) ),
				'datePublished' => $comment->comment_date,
				'reviewRating' => $reviewRating,
				'author' => array(
					'@type' => 'Person',
					'name' => $comment->comment_author,
					'url' => $comment->comment_author_url,
				)
			);
		}
	}

	return $schema;
}


function hunch_schema_wc_schema_brand( $product ) {
	$schema = array();

	if ( function_exists( 'get_brands' ) && taxonomy_exists( 'product_brand' ) ) {
		$brands = get_the_terms( $product->get_id(), 'product_brand' );

		if ( $brands ) {
			foreach ( $brands as $brand ) {
				$image = '';
				$image_id = get_woocommerce_term_meta( $brand->term_id, 'thumbnail_id', true );

				if ( $image_id ) {
					$image_src = wp_get_attachment_image_src( $image_id, 'thumbnail' );

					if ( $image_src ) {
						$image = $image_src[0];
					}
				}

				$schema[] = array(
					'@type' => 'Brand',
					'@id' => get_term_link( $brand ),
					'name' => $brand->name,
					'description' => wp_strip_all_tags( strip_shortcodes( $brand->description ) ),
					'url' => get_term_link( $brand ),
					'image' => $image,
				);
			}
		}
	}

	return $schema;
}


function hunch_schema_wc_schema_seller() {
	$schema = array();

	if ( class_exists( 'HunchSchema_Thing' ) && is_callable( array( 'HunchSchema_Thing', 'getPublisher' ) ) ) {
		$hunchschema_thing = new HunchSchema_Thing;

		$schema = $hunchschema_thing->getPublisher();

		$schema['url'] = site_url();
		$schema['@id'] = site_url();
	}

	return $schema;
}


function hunch_schema_wc_schema_gtin( $schema, $product ) {
	// Add GTIN from https://wordpress.org/support/plugin/woo-add-gtin/; https://wordpress.org/support/topic/add-gtin-to-schema-markup/#post-11765945
	$gtin = get_post_meta( $product->get_id(), 'hwp_product_gtin', true );

	// https://webappick.com/docs/woocommerce-product-feed/61-product-attributes/47-how-to-add-gtin-ean-mpn-upc-isbn-to-products-and-make-feed/
	if ( ! $gtin ) {
		$post_meta_name = $product->is_type( 'variation' ) ? 'woo_feed_gtin_var' : 'woo_feed_gtin';

		$gtin = get_post_meta( $product->get_id(), $post_meta_name, true );
	}

	if ( ! empty( $gtin ) ) {
		switch ( strlen( $gtin ) ) {
			case 8 :
				$schema['gtin8'] = $gtin;
				break;
			case 12 :
				$schema['gtin12'] = $gtin;
				break;
			case 13 :
				$schema['gtin13'] = $gtin;
				break;
			case 14 :
				$schema['gtin14'] = $gtin;
				break;
			default :
				$schema['gtin'] = $gtin;
		}
	}

	return $schema;
}


function hunch_schema_wc_schema_related_products( $product_ids ) {
	$schema = array();
	$settings = get_option( 'schema_wc' );

	if ( count( $product_ids ) ) {
		foreach( $product_ids as $product_id ) {
			$product = wc_get_product( $product_id );

			if ( ! $product ) {
				continue;
			}

			$attributes = new Attribute( $product );

			$product_schema = array(
				'@type' => "Product",
				'@id' => $product->get_permalink() . '#Product',
				'name' => $product->get_title(),
				'description' => ( $settings['description'] == 'full' ) ? wp_strip_all_tags( strip_shortcodes( $product->get_description() ), true ) : wp_strip_all_tags( strip_shortcodes( $product->get_short_description() ), true ),
				'url' => $product->get_permalink(),
			);

			if ( $product->get_sku() ) {
				$product_schema['sku'] = $product->get_sku();
			}

			$product_schema = hunch_schema_wc_schema_gtin( $product_schema, $product );

			if ( $product->get_image_id() ) {
				$product_image = wp_get_attachment_image_src( $product->get_image_id(), 'single-post-thumbnail' );

				if ( $product_image ) {
					$product_schema['image'] = array(
						'@type' => 'ImageObject',
						'url' => $product_image[0],
						'height' => $product_image[2],
						'width' => $product_image[1]
					);
				}
			}

			$product_schema['offers'] = hunch_schema_wc_schema_offer( $product, $attributes, true );

			if ( $product->get_rating_count() ) {
				// https://github.com/SchemaApp/SchemaApp/issues/2299#issuecomment-590880047
				//$product_schema['aggregateRating'] = hunch_schema_wc_schema_rating( $product );
				$product_schema['review'] = hunch_schema_wc_schema_review( $product );
			}

			if ( function_exists( 'get_brands' ) && taxonomy_exists( 'product_brand' ) ) {
				$product_schema['brand'] = hunch_schema_wc_schema_brand( $product );
			}

			$schema[] = $product_schema;
		}
	}

	return count( $schema ) == 1 ? reset( $schema ) : $schema;
}


// Override templates 
add_filter('wc_get_template', 'hunch_schema_woocommerce_locate_template', 10, 3);
add_filter('wc_get_template_part', 'hunch_schema_woocommerce_locate_template', 10, 3);
function hunch_schema_woocommerce_locate_template($template, $slug, $name) {

    if (strstr($template, "themes")) {
        return $template;
    }

	if ( version_compare( WC()->version, '2.6', '>=' ) )
	{
		$schemaTemplates = plugin_dir_path(__FILE__) . 'woocommerce-2.6/';
	}
	else
	{
		$schemaTemplates = plugin_dir_path(__FILE__) . 'woocommerce/';
	}

    $template_name = substr($template, strpos($template, '/templates/') + 11);
    $schemaTemplate = $schemaTemplates . $template_name;

    // Use the template from this plugin, if it exists
    if (file_exists($schemaTemplate)) {
        return $schemaTemplate;
    } else {
        return $template;
    }
}
