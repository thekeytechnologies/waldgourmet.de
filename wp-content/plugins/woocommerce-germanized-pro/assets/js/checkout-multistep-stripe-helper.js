jQuery( function( $ ) {

    /**
     * Object to handle Stripe payment forms.
     */
    var wc_gzdp_multistep_stripe_helper = {

        /**
         * Initialize e handlers and UI state.
         */
        init: function() {
            this.$form   = $( 'form.checkout' );
            this.methods = wc_gzdp_stripe_multistep_params.methods;

            this.update();

            $( document.body ).bind( 'updated_checkout', this.update );
            $( document.body ).bind( 'wc_gzdp_step_changed', this.update );

            // These events are used by Stripe to remove the hidden input
            $( document ).on( 'checkout_error', this.change );
            $( document ).on( 'change', 'form.checkout', this.change );
            $( document ).on( 'change keyup', 'form.checkout', this.change );

            $( 'form.woocommerce-checkout' ).on( 'checkout_place_order_stripe checkout_place_order_stripe_bancontact checkout_place_order_stripe_sofort checkout_place_order_stripe_giropay checkout_place_order_stripe_ideal checkout_place_order_stripe_alipay checkout_place_order_stripe_sepa', this.change );
        },

        isStripePaymentMethodSelected: function() {
            return $( 'input[id=^payment_method_stripe]' ).is( ':checked' );
        },

        isStripePaymentMethodWithHandlingSelected: function() {
            var is_selected = false;

            $.each( wc_gzdp_multistep_stripe_helper.methods, function( key, value ) {
                var id = wc_gzdp_multistep_stripe_helper.getPaymentMethodId( key );

                if ( value.needs_handling && $( id ).is( ':checked' ) ) {
                    is_selected = true;
                }
            });

            return is_selected;
        },

        isStripePaymentMethodWithoutHandlingSelected: function() {
            var is_selected = false;

            $.each( wc_gzdp_multistep_stripe_helper.methods, function( key, value ) {
                var id = wc_gzdp_multistep_stripe_helper.getPaymentMethodId( key );

                if ( ! value.needs_handling && $( id ).is( ':checked' ) ) {
                    is_selected = true;
                }
            });

            return is_selected;
        },

        getPaymentMethodId: function( method_name ) {
            return 'input#payment_method_stripe' + ( method_name != 'stripe' ? '_' + method_name : '' );
        },

        /**
         * Appends a hidden input if necessary to prevent Stripe from executing payment within a wrong step.
         */
        preventEarlyStripeExecution: function() {
            wc_gzdp_multistep_stripe_helper.$form = $( 'form.checkout' );

            if ( $( '#step-wrapper-order' ).hasClass( 'step-wrapper-active' ) ) {
                wc_gzdp_multistep_stripe_helper.$form.find( '.wc-gzdp-stripe-token-fix' ).remove();
            } else if( $( '#step-wrapper-payment' ).hasClass( 'step-wrapper-active' ) && wc_gzdp_multistep_stripe_helper.isStripePaymentMethodWithHandlingSelected() ) {
                wc_gzdp_multistep_stripe_helper.$form.find( '.wc-gzdp-stripe-token-fix' ).remove();
            } else if ( wc_gzdp_multistep_stripe_helper.isStripePaymentMethodWithoutHandlingSelected() || wc_gzdp_multistep_stripe_helper.isStripePaymentMethodWithHandlingSelected() ) {
                if ( wc_gzdp_multistep_stripe_helper.$form.find( '.wc-gzdp-stripe-token-fix' ).length == 0 ) {
                    wc_gzdp_multistep_stripe_helper.$form.append( '<input type="hidden" name="stripe_token" class="stripe_token stripe-source wc-gzdp-stripe-token-fix" />' );
                }
            }
        },

        change: function() {
            wc_gzdp_multistep_stripe_helper.preventEarlyStripeExecution();
        },

        update: function() {
            wc_gzdp_multistep_stripe_helper.preventEarlyStripeExecution();
        }
    };

    wc_gzdp_multistep_stripe_helper.init();
});