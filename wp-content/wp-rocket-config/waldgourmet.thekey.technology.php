<?php
defined( 'ABSPATH' ) || die( 'Cheatin&#8217; uh?' );

$rocket_cookie_hash = '349b493d5b8b0d6aa73648c1d2e6c583';
$rocket_logged_in_cookie = 'wordpress_logged_in_349b493d5b8b0d6aa73648c1d2e6c583';
$rocket_cache_mobile_files_tablet = 'desktop';
$rocket_cache_mobile = 1;
$rocket_cache_reject_uri = '/(.+/)?feed/?|/(?:.+/)?embed/|/kasse/(.*)|/warenkorb/|/mein-konto/(.*)|/wc-api/v(.*)|/(index\.php/)?wp\-json(/.*|$)';
$rocket_cache_reject_cookies = 'wordpress_logged_in_.+|wp-postpass_|wptouch_switch_toggle|comment_author_|comment_author_email_';
$rocket_cache_reject_ua = 'facebookexternalhit';
$rocket_cache_query_strings = array();
$rocket_secret_cache_key = '';
$rocket_cache_ssl = 1;
$rocket_do_caching_mobile_files = 0;
$rocket_cache_mandatory_cookies = '';
$rocket_cache_dynamic_cookies = array();
